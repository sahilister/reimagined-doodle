# sahilister's [Reimagined Doodle](https://blog.sahilister.in/)

Thoughts and ramblings at [blog.sahilister.in](https://blog.sahilister.in/).

![](/static/img/licensebutton.png) <br>
All content on the site licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license unless otherwise stated. 
Site powered by [Hugo](https://gohugo.io). Theme used [etch](https://github.com/Lukasjoswiak/etch) (under [MIT License](https://github.com/LukasJoswiak/etch/blob/master/LICENSE)) thanks to [Lukas Joswiak](https://lukasjoswiak.com/).

