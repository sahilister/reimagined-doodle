---
title: "About"
---

Hi, I'm Sahil Dhiman (he/him) going by the handle @sahilister almost everywhere on the internet. 

A GNU/Linux Network Systems Engineer by profession. A Debian Developer non-uploading. Otherwise into OpenStreetMap, sys-admin, networks, [Free Software](https://www.gnu.org/philosophy/free-sw.en.html) and now a blog writer ;)

Believer of liberalism. Always open to new ideas. Tries to separate identity from thoughts.
Thinks "mind is a collection of thoughts which can be debated and replaced".
Everyone should read _The Story of My Experiments with Truth_ by Mahatma Gandhi once in their lifetime.

**Present Mantra** -

When asked how he (ie [Terry Fox](https://en.wikipedia.org/wiki/Terry_Fox)) kept himself going as exhaustion set in and he had thousands of miles ahead of him, he answered,
>  I just keep running to the next telephone pole.<br>

**Feed** - [_blog.sahilister.in/index.xml_](https://blog.sahilister.in/index.xml) <br>
**Email** - _sahil AT sahilister.in_ <br>
**XMPP** - _sahil AT chat.sahilister.in_ <br>
**Mastodon** - [_sahil@toots.sahilister.in_](https://toots.sahilister.in/@sahil) <br>


_Site powered by [Hugo](https://gohugo.io). Theme used [etch](https://github.com/Lukasjoswiak/etch) (under [MIT License](https://github.com/LukasJoswiak/etch/blob/master/LICENSE)) thanks to [Lukas Joswiak](https://lukasjoswiak.com/)._
