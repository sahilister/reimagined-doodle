---
title: "Another Year Passed, 22"
date: 2021-09-29T21:26:56+05:30
tags: ["yearly", "life", "reflection", "100DaysToOffload"]
---

So 22 now.

During the midday cake cutting ceremony, I was surprised to find that I'm 22 now and not 21. 22 seems older, 21 feels young.

It so happened that my birthday coincided with my house shifting. We shifted place after  almost eight years at this place. Lots of usual troubles, getting stuff from here to there, which is in fact 200 meters from the present one. Internet, all the house stuff, utilities, everything needs to be moved. I wasn't feeling anything earlier, but the feelings have started pricking now. I was seeing this as a great shift refresh where all the useless stuff was moved out, thrown or cleaned. Sold 63.55 kg of stationers, textbooks and study materials; a boot full of stuff that was (now I mostly have fictions and other good stuff left ;)). It was/is a new start. The past one was rented, as does the present one. But the attachment remains. The transferrable nature of father's job had this that we moved places (many a times states) every three-four years, but then I was smaller, now a bit older. Eventually, with time, this one will start feeling like home.

This year also saw my foray into professional life. It's tiresome and long at times, it's a job after all. Now I do earn, more than I expected, but after the first one-two months, it has become just something that lands in the account on the last date of the month.

A significant new development was my new-found interests in servers and system administration (which may have played a big part in landing my Quality Assurance engineer job). I wasn't conscious, but a recent discussion about hosting my own XMPP server with [Ravi](https://ravidwivedi.in) and his mentioning "You are the one running almost every server now, except mails" made me aware that I do run quite a [bunch of services](https://blog.sahilister.in/2021/07/services-that-i-self-host-july-2021-edition/) now. It's like if I see a permanent need, I usually do start running a particular service now without second thoughts.

A bunch of stuff that happened in the past year wouldn't have been possible if I didn't have a reliable high speed internet connection. Even now, when I'm waiting for my internet connection migration to my new place, I can feel how important this is. Most of the stuff I did, I hadn't had to think twice about the internet connection fumbling; 4k video streams, long HD video calls, handling large video files or simply learning something, each of these were a breeze. This is a privilege which many people don't have, which I should bear in mind more. It's just that I'm in city areas where high speed fiber to home lines are laid and economical due to competition, albeit less, but affordable.

Lastly, partly due to seeing loads of travels vlogs and partially due to change in my liking, I do often travel to unexplored streets and corners of the city all alone. Covid has this that travelling outstation isn't doable as much, but going to this unexplored city areas give a different sort of kick. I just love them and want to continue going out to these places, mostly alone, to appreciate these and taking the time to slow down wherever I like.
