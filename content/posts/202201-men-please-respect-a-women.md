---
title: "Men, Please Respect Women!"
date: 2022-01-15T23:41:18+05:30
tags: ["life", "appeal", "100DaysToOffload"]
---
A humble appeal to men to please respect women and their consent. It pains me immensely to say that **EVERY SINGLE FEMALE FRIEND** I came in close contact with has multitudes of horrible stories to tell about happenings of men following, molesting, character assassinating, making unsolicited sexual advances and what not. These were terrible things to hear, and every one of these girls were traumatized beyond belief. On the surface, these female friends seem happy or fine, but the constant amount of these bad advances have them scared, mentally. Single acts go a long way and keep on affecting for years.

So please BACK-OFF or just go ask a female friend how horribly results these things can have.
