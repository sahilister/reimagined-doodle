---
title: "Life Goes On"
date: 2022-03-11T11:36:40+05:30
tags: ["life", "short", "thoughts"]
---
Whether it's life after someone's death or traffic on a highway, it keeps on going.

A small particle, person or writing is just a minute dent in the grand scheme of the universe. Big bang is big or probably the biggest event in comparison to just an exam or a deadline. Everything is relative.
A thing that's worrying can fade as soon as a different, "bigger" issue crops up. So, does anything matter, or we should say all is well and things will pan out fine in the end (or if they don't, a "bigger" issue is surely on the way).


