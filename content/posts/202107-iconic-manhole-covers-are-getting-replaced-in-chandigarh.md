---
title: "Iconic Manhole Covers are Getting Replaced in Chandigarh"
date: 2021-07-12T22:16:35+05:30
tags: ["chandigarh", "history", "manhole-cover", "100DaysToOffload"]
---

Recently, I came across the following news:

![Newspaper report of Chandigarh administration plan to replace the iconic manholes to prevent theft, smuggling](/img/chandigarh-manhole-cover-newspaper-report.jpg)

<div style="text-align: center">
    <i>
        Newspaper report of Chandigarh administration plan to replace the iconic manholes to prevent theft, smuggling. <a href="/img/chandigarh-manhole-cover-newspaper-report.jpg">Click to enlarge</a>
    </i>
</div>

Chandigarh administration is planning to replace the iconic manhole covers of Chandigarh. To give you some history lesson on Chandigarh, the city is a planned city with grid layout called sectors. It was developed post India's independence to serve as capital of Indian state of Punjab. The erstwhile Punjab got bifurcated into East Punjab (now Punjab, India) and West Punjab (now Punjab, Pakistan) with then capital, Lahore going to West Punjab. Existing cities like Amritsar, Jalandhar and Ludhiana were rejected for capital due to various reasons and the decision was made to develop a brand new capital from ground up in the foothills of Shivalik Range. It was to be India's first planned city, which was designed by Swiss-French architect Le Corbusier with help from his cousin and fellow architect Pierre Jeanneret and English couple Maxwell Fry and Jane Drew alongside a number of Indian planners and architect. It was completed in the 1960s. The manhole cover was part of the original plan and were designed by Le Corbusier himself.

<br>

![Chandigarh's iconic manhole cover image](/img/chandigarh-manhole-cover.jpg)
<div style="text-align: center"><i>
         <a href="https://www.flickr.com/photos/67987420@N00/277580106">Chandigarh Manhole cover</a> by
         <a href="https://www.flickr.com/photos/67987420@N00">Rossipaulo</a> under
         <a href="https://creativecommons.org/licenses/by/2.0/?atype=rich">CC BY 2.0</a> 
         </i>
</div>

These were cast iron manhole covers which had Chandigarh's map inscribed over them. All the grid roads, neatly dividing Chandigarh into sectors, Sukhna lake on the right and rivulet flowing in periphery of Chandigarh can be seen on the manhole covers. I remember seeing two variations of these, but have no means to confirm if there were indeed two versions made back then. The first time I came across this was probably in 2012-13, I observed the manhole cover with great surprise. I wasn't aware of the historical context to them then. The resemblance to actual map was incredible, and I spent quite sometime standing and literally observing the or a manhole cover. Eventually, I started finding them in random places, Inter State Bus terminal - 43, Manimajra market, city footpath amongst other places. I can't remember the number of times, I have stopped over these in various places and showed them to my mates and how these are actual maps of Chandigarh. These always to get me pumped. They along with the original grid roads, capitol complex and other structures have seen more than six decades of urbanization of Chandigarh. From a bunch of villages on the foothills of Shivalik range to one of the biggest cities in North India. 

Just like other historical artifacts of Chandigarh, these manhole covers were auctioned in international markets for as high as 10-11 lakhs INR. These and other factors have lead to multiple instances of theft and smuggling of these, leading to the ultimate decision of removal of them completely. Now they would be stowed away in some governmental warehouse and left to negligence.

_PS: [Jason](https://janusworx.com/) shared ['India’s Overlooked Concrete City of Oz'](https://www.messynessychic.com/2020/09/01/indias-overlooked-concrete-city-of-oz/) which shows the beautiful side of Chandigarh and some historical information. Do check it out._
