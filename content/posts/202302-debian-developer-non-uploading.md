---
title: "A Debian Developer non uploading now"
date: 2023-02-27T00:17:55+05:30
tags: ["debian", "debian-developer", "free-software"]
---

![Office buildings](/img/phase-3-metro-station.jpg)

I was standing on an elevated metro platform, looking at high rise office buildings. People were walking in the small park nearby, few were working in the office, unbeknown to the one event that I have been actively looking forward to. These high rises office spaces and people returning from work, again made me realize how small I am. The thought that I'm becoming a Debian Developer (non-uploading) and none of the folks I'm looking at would ever know, wouldn't care, was a humbling feeling.

Starting with series of broken Pop!_OS and Ubuntu OS installations to one month long hit and trials to get my Debian installation right, to DebConf20 happening online, to India winning the bid to host DebConf, meeting the Debian India community, to a bunch of other events finally lead to me becoming a DD.

Back in 2020, I had dreamed of becoming a Debian Developer before DebConf23 Kochi. That squarely gave me three years for becoming a DD. After almost three somewhat not so successful attempts at packaging, I had almost forgotten about that dream of becoming a DD and just went about helping in Debian conferences.

Initially, I wanted to grab _sahil@debian.org_ because I'm fascinated with emails, and this was one heck of a cool email address to give around. I was also excited about getting a LWN.net subscription as an added benefit to DDs. No one covers Linux and Free Software news better than LWN.net. Though on later discussions, I got to know that outgoing email delivery on @debian.org isn't good and LWN.net subscription benefit was trimmed down.

Looking back, I'm grateful to all the people who make Debian and the surrounding community happen. They have impacted many lives (including mine) greatly positive direction. If Debian wasn't there, I wouldn't have got the opportunity to meet so many interesting folks and my enthusiasm to attend all these events.  Debian is one of the few tech communities where I belonged :)

Going forward, now I would have a slightly bigger part to play in Debian (and with complete access to -private to keep me company ;)) I'll continue volunteering for various community and technical activities as usual, as becoming a DD changes nothing. Tomorrow again, the sun would rise. I'll wake up, get ready, go to work, see people and nothing would change. But there would be an inner joy that finally I have become a DD indeed :)

I would like to conclude with a few lines from Robert Frost's _Stopping by Woods on a Snowy Evening_
> _'..The woods are lovely, dark and deep,_ <br>
> _But I have promises to keep,_ <br>
> _And miles to go before I sleep,_ <br>
> _And miles to go before I sleep._'

_PS - Now you can mail me at sahil@debian.org_

_PPS - Just looked at db.debian.org today, and it seems I'm one of the fourteen DDs in India. Cool!_
