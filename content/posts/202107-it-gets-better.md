---
title: "It Get's Better"
date: 2021-07-25T21:43:22+05:30
tags: ["life","100DaysToOffload"]
---

It gets better, however life is now, it gets better. Whatever situation, hardship, existential thoughts/crisis one is facing now, things work out in the end. I highly encourage you to see 'Episode 17 - The Watchman - _चौकीदार_' of Doordarshan's _Malgudi Days_ on [YouTube](https://www.youtube.com/watch?v=5hdFB-7yGzc) (on [Invidious](https://yewtu.be/watch?v=5hdFB-7yGzc) or [Piped](https://piped.kavin.rocks/watch?v=5hdFB-7yGzc)) to see what I meant here.

On a personal level, my '[College Over](https://blog.sahilister.in/2021/05/college-over/)' post two months back wasn't an optimistic one. Things seem not so good with the ending of college and no particular path in mind. Now, as I'm writing this, I'm working with MakeMyTrip as Quality Assurance engineer (or SDET ;)) and will complete two months with them on the coming 10th. It happened all of a sudden.  Everything seems fine, colleagues are great and helpful, the package is great (more than I even thought of). I look back and see how things change, and mostly for the better. Looking further back, 11th-12th were grueling times, so much so that I didn't visit my native place for two years. Stressful times those were, but it turned out fine in the end.

In yester-month when _Battlegrounds Mobile India (BGMI)_ was launched, and I got it on my new phone, I was discussing it with Abhay in-game. Two-three years back, when _PUBG Mobile_ was a thing, me and Abhay had multiple issues playing it. My mobile would sweat and heat up while running it. I would sit outside in veranda  to even  get reasonable internet connection. Now both, me and Abhay have relatively capable mobile devices with fiber internet connections at home (and both have entered professional life) so playing it is easier and more fun now. Things are better now.

In the end, I would like to conclude with Shahrukh Khan's dialogue -
<br>_[...] अगर सब ठीक ना हो, तो दी एनड़ नहीं है दोस्तों; पीचर अभी बाकी हैं।_

Translation and [actual video](https://youtu.be/D6eJo5XdMhc) (on [Invidious](https://yewtu.be/watch?v=D6eJo5XdMhc) or [Piped](https://piped.kavin.rocks/watch?v=D6eJo5XdMhc)) of complete dialogue.
