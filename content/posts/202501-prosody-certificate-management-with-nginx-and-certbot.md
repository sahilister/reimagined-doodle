---
title: "Prosody Certificate Management With Nginx and Certbot"
date: 2025-01-12T10:20:21+05:30
tags: ["prosody", "xmpp", "certbot", "nginx"]
---

I have a self-hosted XMPP chat server through Prosody. Earlier, I struggled with certificate renewal and generation for Prosody because I have Nginx (and a bunch of other services) running on the same server which binds to Port 80. Due to this, Certbot wasn't able to auto-renew (through HTTP validation) for domains managed by Prosody. 

Now, I have cobbled together a solution to keep both Nginx and Prosody happy. This is how I did it:
- Expose `/.well-known/acme-challenge` through Nginx for Prosody domain. Nginx config looked like this: 
```
server {
      listen 80;
      listen [::]:80;
      server_name PROSODY.DOMAIN;
      root <ANY_NGINX_WRITABLE_LOCATION>;

      location ~ /.well-known/acme-challenge {
         allow all;
      }
}
```
- Run `certbot` to get certificates for <PROSODY.DOMAIN>. 
- To use those in Prosody, add a cron entry for `root` user:
```
0 0 * * * prosodyctl --root cert import /etc/letsencrypt/live/PROSODY.DOMAIN
```
Explanation from [Prosody docs](https://prosody.im/doc/certificates#automatic_certificate_import):
> Certificates and their keys are copied to /etc/prosody/certs (can be changed with the certificates option) and then it signals Prosody to reload itself. --root lets prosodyctl write to paths that may not be writable by the prosody user, as is common with /etc/prosody.
- Certbot now manages auto-renewal as well, and we're all set. 


