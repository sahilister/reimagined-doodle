---
title: "Significance of a Day"
date: 2021-12-31T20:02:34+05:30
tags: ["life", "100DaysToOffload"]
---

This is the first time in more than two decades when it's not holidays in Christmas-New Year season. Getting up, switching on the laptop to work while seeing the dates of new year is giving mixed feelings. It's almost the new year, and it should feel special, different or significant of a sort, but going through the day, working, it feels like just another day and right now, night. Colleagues are taking leaves to go out or just relax - another thing that tells it's the new year. 

During childhood, birthdays were happening days; not much anymore. It makes me wonder if only ones perception that makes the day special. It's what significance we give a day which make it special. Anyday can be special if one care or feel like it.

Not 2021 - in some time.
