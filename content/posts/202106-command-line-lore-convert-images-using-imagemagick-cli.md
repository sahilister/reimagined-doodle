---
title: "Command-line lore: Convert Images Using ImageMagick"
date: 2021-06-27T18:18:24+05:30
tags: ["command-line lore", "cli", "imagemagick", "100DaysToOffload"]
---
Another series, command-line lore. It will primarily document some quick command-line tricks. 

In this particular post, we'll discuss various quick stuff that can be done through [ImageMagick](https://imagemagick.org/). ImageMagick is used to create, edit, compose or convert digital images. It has both GUI and CLI applications.

On Debian/Ubuntu systems, just do the following to grab the package:
```bash
sudo apt install imagemagick
``` 

ImageMagick includes a [number of command-line utilities](https://legacy.imagemagick.org/script/command-line-tools.php), but we mainly require `convert` from the suite.

_Note - ImageMagick is moving towards v7 which doesn't have `convert` and other utilities available in v6 (legacy), so the following stuff won't work in v7._

## Table of Content

- [Append images together](#append-images-together)
- [Rotate image](#rotate-image)
- [Convert image to different format](#convert-image-to-different-format)
- [Crop or resize image](#crop-or-resize-image)


### Append images together

- Append images vertically together:
```bash
convert image1.png image2.png -append output.png
```

- Append images horizontally together:
```bash
convert image1.png image2.png +append output.png
```


### Rotate image

- Rotate by specifying the angle for rotation, moving towards right:
```bash
convert image.png -rotate {90|180|270} output.png
```


### Convert image to different format

- Convert image between image formats:
```bash
convert image.jpg output.png
```
- Convert an image to PDF format:
```bash
convert image.jpg output.pdf
```
_Note - ImageMagick started blocking image to PDF conversion due to a security issue. You can read more and how to enable it back on [_Stack OverFlow_](https://stackoverflow.com/questions/52998331/imagemagick-security-policy-pdf-blocking-conversion)._

- Combine multiple images and make a PDF:
```bash
convert image1.jpg image2.jpg output.pdf
```

### Crop or resize image

- Scale an image to a defined size:
```bash
convert image.jpg -resize 50% output.jpg
```

- Scale an image to a defined pixel size:
```bash
convert image.jpg -resize 32x32 output.jpg
```
