---
title: "Hosting a Mastodon Server"
date: 2021-12-21T09:35:08+05:30
tags: ["mastodon", "hosting", "100DaysToOffload", "social"]
---

_Never heard of Mastodon? [See this video](https://masto.sahilister.in/@sahil/107274773199794221) to know._

It's been more than a month since [_masto.sahilister.in_](https://masto.sahilister.in/about), the Mastodon instance I host is up and running. It has been surprisingly easy to maintain and hasn't given any real troubles yet. Part of it can be attributed to using docker-compose for setting it up due to non-successful attempts to get it up native method and partially due to my apprehension to not handle the DB (Postgres in this case).


It came to be when [Abbyck](https://abhinavkrishna.in/) mentioned Racknerd's 11.11 deal offering a 3 vCPU, 4 GB RAM rig with 75 GB storage for 50 USD/year, which I bought. Had some discussions with Ravi that as the rig is already paid for a year, hosting a Mastodon instance on it won't be of much trouble. I did setup-ed Pleroma a while back (on _social.sahilister.in_ namespace); it's lighter and more customizable. But Mastodon has its pull, it feels more mature, and I love the UI, so Mastodon was the natural choice. [Asked the fediverse](https://fosstodon.org/@sahilister/107233931955848475) on requirements which got diverse set of answers.  Experimented, things got stable and migrated my account using Mastodon's account migration tool, which migrated everything except for toots. 

The instance is now home to [me](https://masto.sahilister.in/@sahil) and [Ravi](https://masto.sahilister.in/@ravi). Federation works fine and interactions with fediverse are a breeze. Earlier, I wasn't keen on getting a personal instance as the local timeline on [_fosstodon.org_](https://fosstodon.org) was always full of technology, software based conversations. After migrating, my local timeline only houses, as of now (other than mine) Ravi's posts who I already follow. Due to the nature of the federated timeline, it wasn't much populated. I had read about relays. Relays federate post from bigger Mastodon/Pleroma/Fedi instances to one's personal/smaller instance to populate the federated timeline with their, diverse content. I got around subscribing to [_mas.to_](https://mas.to)'s [relay](https://mas.to/about/more#Relay). Now things are more happening on the timeline.

As for the maintenance part, I usually ssh into the machine and log into the docker container, to purge media every few days (which needs to be automated). Presently, Mastodon is using around 5 GBs of storage, 1.47 GBs of memory and less than 10% tops for CPU. Though due to latency, _Tusky_, my android client, while trying to play a video timeout saying "Can't play this video". Web/browser seems to play videos just fine. Probably due to smaller buffer/load time limits.

Essentially, hosting the instance gave insights on how federation works while giving the cool boosting factor of running it on own domain ;)

_PS: If you know me and want a home on the fediverse/on mastodon and okay-ish with occasional (albeit rare) downtimes, feel free to ping._


