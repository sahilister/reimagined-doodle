---
title: "I Like Writing Now!"
date: 2021-05-08T00:31:23+05:30
tags: ["writing", "100DaysToOffload"]
---

I have now grown to like the process of writing. The biggest joy it has given me is the appreciation of simplicity in text. Simple words are beautiful. Simple sentences are beautiful. Short posts are beautiful. Words in brackets are beautiful, holding sub-notes of main thought process (probably my favorite part). Hearing or reading usage of variety words in sentences gives me joy now. 

Writing is a snapshot of my mind at a period in time. Writing is a fine art created with loads rewrites to come with (almost) perfect words for a sentence. My writings resides in my happy corner ie my blog. I have grown to cherish this blog; a self written journey through life (to the moon :D).   



