---
title: "Write for Yourself"
date: 2022-01-04T20:35:17+05:30
tags: ["writing", "100DaysToOffload"]
---
[Akshay](https://asd.learnlearn.in/about/), during one of our mail correspondence, said - "The whole blogging thing is a giant ego boost".  Now I can't agree more. I spent the past half hour and countless nights before reading my life blogs and loved the feeling. Even though all of them were my experiences, written, edited and posted by me, still going through them was like reliving my past once again.

Many of my blogs feel worthless and not fit for posting immediately when in process of their creation, but I have come to this realization that they were/aren't for immediate me but for future me.

As I say, blog writing is like writing one's own history/autobiography. No one would appreciate them more than your future self. Reliving the moment is what they will help you. I encourage you to write, write for yourself.
