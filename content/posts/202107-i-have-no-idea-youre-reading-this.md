---
title: "I Have No Idea You're Reading This"
date: 2021-07-29T22:19:29+05:30
tags: ["no-analytics", "blog", "tracking", "100DaysToOffload"]
---

From the beginning, this site had no analytics of any kind. It's a static site which mostly loads three files, an HTML file, a stylesheet and CC license image at the bottom. It's hosted on GitLab pages, so no analytics from the platform as well. Having no analytics is good due to two main reasons. First it gives peace of mind, frankly because I'm a big fan of looking at tech graphs and numbers. No numbers from the blog takes away the option of compulsively looking at page views or traffic data, which helps me concentrate on just thinking and writing. Secondly, most analytics tools and services are just bad in terms of privacy.

I personally subscribe to blogs and other writings through RSS feeds in Thunderbird because it gives me bare writings, media and nothing else. No fancy styling, JavaScript or distracting themes. This blog is syndicated, in full, on [FSCI Planet](https://planet.fsci.in/) and on blog's [RSS feed](https://blog.sahilister.in/index.xml) [^1]. I encourage you to consume it through those if you don't feel like visiting the blog again and again.

Also, if you're reading this, feel free to say hi (hello works just as well ;)). Email in the [about section](https://blog.sahilister.in/about/). 

[^1]: If you're new to the concept of web/RSS feeds, read about it [here](https://ncase.me/rss/).
