---
title: "What Is Life?"
date: 2021-03-14T22:15:52+05:30
tags: ["life", "short", "100DaysToOffload"]
---
Life is a series of experiences.

Those experiences can be good or bad purely based on our thought process. We [stress out over small things](https://blog.sahilister.in/2020/08/dont-take-life-so-seriously/) and years later feel stupid and laugh over them. The following quote does encompass it nicely:

> Life is a tragedy when seen in close-up, but a comedy in long-shot. 

> — <cite>Charlie Chaplin</cite>

So go out in the field, take risks, do things that make you happy regardless of popular perception/trends and thoughts about what folks would think about you. After all, _YOLO!_ [^1]

[^1]: _YOLO is "you only live once"._

_PS: This what I think life is in March 2021. It stuck me while I was driving my two-wheeler today and was thinking about what life means for me. I believe my definition of life would change as life progress._
