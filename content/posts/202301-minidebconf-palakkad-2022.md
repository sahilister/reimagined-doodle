---
title: "MiniDebConf Palakkad 2022"
date: 2023-01-28T18:02:50+05:30
tags: ["debian", "minidebconf", "mdc", "experience", "kerala", "palakkad"]
---

It all started with a discussion that we'll meet IRL if we have a physical Debian event. A few texts exchanged - we got a venue, we had an organization team, and we were set for an event. This was how MiniDebConf Palakkad 2022 which happened on November 12 to November 13, in NSS College of Engineering Palakkad, Kerala came to be. 

To give you folks a bit of context, I have been active in Debian and Debian India since DebConf20, which was around the time when I started using Debian full time on my machine. Debian been such a welcoming community, and DebConf22 Kochi (which has since changed to DebConf23 Kochi) preparations ongoing, pulled me in for more participating and volunteering for more Debian events since. Due to Covid-19 pandemic, DebConf22 Kosovo was probably the first physical Debian event. As official host of next DebConf, the Debian India had to send a delegation to attend and receive Pollito. I wanted to attend this conference and meet folks I was working, interacting and gossiping with since I started on Debian, as we hadn't met IRL ever. But alas! Due to work, I had to skip attending the conf. Also, all the attendees kept saying, I should have attended it, it was so much fun. Now during once such conversation among the MDC Shoutout team (if you know, you know ;)), we were discussing this and that's when the idea of a Debian event came to be. A few texts by Abbyck to his alma mater club at FOSSNSS, and soon we had them onboard for venue and event. From there it all came to be. 

Now, coming up with a two day, two track Debian event is never easy. Call for proposals needs to be sent, website has to be done, registrations has to be taken care of, money needs to be raised, publicity has to be done and then on the venue, projection and sound system is to be setup, attendees need food and accommodation and all the nitty-gritty has to be planned, arranged and executed for the conference to happen. Discussions started around September and conference dates were fixed mid-November. Teams, consisting mostly of FOSSNSS and Debian India folks, started work under guidance from Anupa. (She's usually the one doing most stuff, never ever taking the credit for.) All the folks did loads of work, multiple meetings were conducted and on ground stuff was done. 

As Ravi too planned to join the conference, we booked the tickets to Coimbatore, from there we planned to travel by road to Palakkad. After landing in Coimbatore, rain was the last thing I was expecting in the month of November. Later on, I came to know it's always rainy here in South India for half the year. As I hadn't anticipated rain, I was wearing white shoes, and it felt real bad seeing them catch all the mud and dirt. Coming here was also a cultural shock for me. Since childhood, I had never travelled to south India or any place where Hindi wasn't a commonly understood or spoken language. Now, here I was in Tamil Nadu with no understanding of Tamil. It wasn't very hard per se, but me being in a bit of a cultural hocked state elevated the hardship. Being with Ravi, who has travelled to places, helped. He knew how to navigate unknown areas. Initially, the plan was to travel in cab via Uber, but that plan fell apart due to price differences. We finally travelled in KSRTC i.e. Kerala State Road Transport Corporation bus, much to the delight of Ravi. The accommodation for was arranged by local team and I got a hotel named KTDC Garden House near the scenic Malampuzha Dam. It was a scenic place, but with limited food options. Malampuzha Dam was beautiful but due to time constraint I couldn't explore it fully with rope way and activities near it. 

This was my first in-person Debian event, so was excited to attend it. On the day of conf, I meet many folks to which I was talking to since a very long time in-person. I also got to know many new folks. In total, I had proposed two Birds of Feathers (BoF), one short talk and a workshop, of which the workshop didn't happen as we (Abbyck and me) didn't get the time to sit together and prepare it. Coming to the BoFs, the [DebConf India BoF](https://in2022.mini.debconf.org/talks/9-debconf23-kochi-bof/) happened on Day 1. It was intended to introduce folks to DebConf23 and get them to join the team (though we weren't too successful on that part). Various teams and members were introduced, and work status was discussed. The second BoF was [Debian India](https://in2022.mini.debconf.org/talks/37-debian-india-bof/) BoF to discuss direction for Debian community in India. Various issues and suggestions were discussed. Moderating this BoF, was more difficult than I had anticipated, but that's how things are. They don't always turn out as we expect them to.

I had a short talk too on the topic, [Mobile OpenStreetMap mapping on the go](https://in2022.mini.debconf.org/talks/12-mobile-openstreetmap-contribution-on-the-go/). I had a few screenshots and outdoor pictures collected, which I planned to incorporate in my talk. I cobbled together all those pictures and finally completed my presentation just in nick of time for the talk. The idea was to present all the use cases and different mobile applications to map those in OpenStreetMap. (I plan to do a write-up for the talk, but let's see if it comes around). The presentation went well, though no-one ask any question in QnA. 

For final submission, the workshop on self-hosting with Abbyck, we decided to cancel due to time constraints.

As being the tradition with Debian events, GPG key signing was part of the event. Ravi gave an introduction on it and Pirate Praveen dived into details how it's done in his talk. It was a fun little activity where people came with their government IDs (some had their pictures from when they were teens) and key signature written or printed somewhere, and we cross verified it and then signed their key. I got my keys signed by Praveen and Anupa, both DDs to meet the criteria to start the DD process. Post that, I started my non uploading DD application. Nilesh, another DD had mentioned that he might be my Account Manager (AM) as his AM slot was empty, and by chance, he was chosen to be my AM, the role he has taken quite seriously since then. The AM process is complete now. I'm awaiting final step of Front desk or DAM approval, post which I'll officially become a Debian Developer, non uploading. 

I was meeting the whole DebConf23 Kochi organizing team for the first time (mostly I was the one coming from far, most of the team lives in and around Kerala). The whole organizing team was housed in KTDC to allow discussions in person to happen, which were held on the night of 12th. In person discussions were way more productive as we were able to discuss and deliberate on DC issues faster. 

![Pollito at Palakkad](/img/pollito.jpg)
<div style="text-align: center">
    <i>
    Pollito at Palakkad
    </i>
</div>

Finally, the event was also graced by the presence of DebConf mascot Pollito which is in India for DebConf23. Raju brought the Pollito on the last day of the conf, and it was an instant celebrity. Raju was mobbed by the crowd for getting a picture with it, and it was also given it's MDC ID as well. I, too, saw and held Pollito for the first time. 

In conf, everyone agreed that we needed more decentralized, in-person events in various parts of India; various nascent plans were made to conduct MDCs in Villupuram, Bengaluru, Hyderabad and Pune. For now, the Villupurm one is happening thanks to active local community VGLUG, titled MiniDebConf Tamil Nadu 2023. Another MDC might take place before DebConf23, Kochi. I hope starting with these MDC, at least the Palakkad and Villupuram MDCs become annual affairs. Fingers crossed on that.  If you want to organize an MiniDebConf in your city, get in contact, and we'll together figure something out.

![](/img/mdc-palakkad-volunteers.png)
<div style="text-align: center">
    <i>
    Volunteer team for the conference
    </i>
</div>

Finally, I would like to thanks all the folks who came together to make this conference a success, and also gave me the opportunity to meet everyone.

![MiniDebConf Palakkad group photo](/img/mdc-palakkad-group-photo.jpg)
<div style="text-align: center">
    <i>
    MiniDebconf Palakkad 2022 group photo. <a href="/img/mdc-palakkad-group-photo.jpg">Click to enlarge</a>
    </i>
</div>


PS - Finally, shoutout to Abbyck!
