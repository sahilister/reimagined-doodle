---
title: "What Is My Legacy?"
date: 2021-04-11T01:35:04+05:30
tags: ["life", "legacy", "100DaysToOffload"]
---

It seems pretty early in life to think about my legacy but some running thoughts in the night beg to differ. Nothing particularly significant is happening right now but remembering some particular praises gave much needed positive self affirmation. Now I'm not going to detail them here (why make reading back those moments here later cringe for myself). A placeholder for years later me, I'm thinking about people I affected (positively) while serving in various roles at Chitkara University Toastmasters Club. It feels nice that my actions helped, inspired, guided and morally supported someone. It affected them such that they remembered and mentioned it even when I wasn't present.

I'm pretty happy with it. These are (some positive) part of my legacy. I want to be remembered as someone who shared knowledge, supported folks and sometimes spread happiness ;)
