﻿---
title: "Confronting Thoughts with Writing"
date: 2021-03-25T22:33:09+05:30
tags: ["life", “thoughts”, “writing”, “100DaysToOffload”]
---
I fear boredom at times, which is apparent from my actions like listening to music while bathing, using mobile phone in the washroom, eating meals in front of television (which seems to become a compulsion when I’m home), filling every empty time with “pseudo-productivity” activities like listening and watching random videos. These can be in equal part attributed to fear of being alone with my thoughts; which run wild easily.

Writing for me is confronting these thoughts and in part, myself. Being truthful while jotting them down, collating them and analyzing them afterwards, is what I try to every time I sit to write. Writing makes thoughts and ideas clear, while helping in visualizing the issues or solution. 

Writing gives mental clarity and another bout with personal thoughts.


_PS: Thaasophobia is an abnormal fear or dislike of being idle [^1]._

_PSS: This started as a post on boredom and ended up more on writing. Actual thought collating and analyzing in play._

[^1]: [“Thaasophobia.”](https://www.thefreedictionary.com/thaasophobia) The Free Dictionary

