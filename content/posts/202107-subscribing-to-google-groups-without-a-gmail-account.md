---
title: "Subscribing to Google Groups Without a Gmail Account"
date: 2021-07-06T20:30:53+05:30
tags: ["google", "google-groups", "email", "100DaysToOffload"]
---

Recently, I wanted to move a Google Group hosted community subscription away from my erstwhile Gmail account to my custom domain mailbox. It's not inherent that a non-Gmail account can also be used to participate in discussions via mail, though it takes away the ability to join the discussion via the website. 

A search after, here's the trick, just append `+subscribe` to group's name in group's full email address, enter in "To" field and hit send from desired mailbox. For example, to subscribe to "Example" group with address _example@googlegroups.com_, email  _example+subscribe@googlegroups.com_. The content of the mail doesn't matter. It will revert with a confirmation mail, reply to it. If the group moderator/admin has enabled moderation queue (or something that Google Group offer), the request will land in moderation queue for them to approve first else an email notifying successful subscription will arrive. Similarly, to unsubscribe, just email to _example+unsubscribe@googlegroups.com_ .

_PS: While writing this post, I stumbled upon this [Stack Overflow thread](https://webapps.stackexchange.com/questions/13508/how-can-i-subscribe-to-a-google-mailing-list-with-a-non-google-e-mail-address), which chronicles some alternate methods of subscription. Also, some folks in the thread mention their Gmail address getting subscribed to the list instead of desirous email for various reason, so bear that in mind too._
