---
title: "Updates: Hosting Moved and Nginx Logs"
date: 2021-11-19T18:03:47+05:30
tags: ["updates", "hosting", "nginx", "100DaysToOffload", "logs"]
---

_Note - If you're facing troubles accessing the feed, please delete and re-add ie [https://blog.sahilister.in/index.xml](https://blog.sahilister.in/index.xml)._

The blog has moved home from GitLab Pages to own, Hetzner rig in Germany. Earlier, using my [mirror script](https://blog.sahilister.in/2021/04/script-to-automatically-mirror-blog-on-seperate-domain/index.html), I had the main blog hosted on GitLab Pages and mirrored/backup hosted on Hetzner, but a recent discussion prompted the reversing of the same.

During a discussion after a session in [Software Freedom camp](https://camp.fsci.in/), a learner/user from Iran mentioned that how people in Iran are denied access to many internet based services like Docker, GitHub, GitLab, GCP/AWS/Azure services, Cloudflare and hoards of US based services due to US sanctions on Iran. Many of these services host [Free Software](https://www.gnu.org/philosophy/free-sw.html) which essentially deny them the freedom to use them. Further, in the discussion, we also learned that GitLab pages sites are also not available to them without VPN or Tor. This essentially made this site unavailable to users due to no faults of their own. [Ravi](https://ravidwivedi.in/index.html), who was also present there, and I had this discussion in which I suggested moving own sites to non-US based hosting. I already had this mirroring setup, so decided to move the main site to Hetzner server in Germany, along with Ravi's. So now the setup is as follows - _blog.mirror.sahilister.in_ is served through GitLab pages. All the changes, git and Hugo build is done on GitLab and a cronjob runs every hour mirroring the content to _blog.sahilister.in_ on Hetzner. This was done to avoid introducing any change to my blogging flow. Though the [mirroring script](https://blog.sahilister.in/2021/04/script-to-automatically-mirror-blog-on-seperate-domain/index.html) handled everything else, the links in feed were pointing to blog.mirror.s.i instead of blog.s.i on main as well (thanks to [@minoru](https://functional.cafe/@minoru) for pointing it out :)). It was solved by adding the following command in mirroring script:
```bash
sed -i 's/blog.mirror.sahilister.in/blog.sahilister.in/g' index.xml
```
On a different note, as the main blog is served from my own box through nginx, I do have nginx access logs telling me if someone accessed/visited a particular section on the blog (and Ravi's blog too :D). Though, I'm thinking of using logrotate to purge them. So you can now say, I do have an idea that you're reading this. [^1]

[^1]: Earlier, due to my setup, I had [no idea that you were reading the blog](https://blog.sahilister.in/2021/07/i-have-no-idea-youre-reading-this/index.html).

