---
title: "Debutsav Kochi 2023"
date: 2023-06-25T15:37:40+05:30
tags: ["debutsav", "debian","debianindia", "conference", "experience","kochi"]
---

[DebUtsav](https://kochi.debutsav.in/) Kochi 2023 was held on June 17th, 2023, at Model Engineering College, Thrikkakara, Kochi. This was organized by the team at FOSSMEC with help from [Abraham Raji](https://abrahamraji.in/). 

I first heard about DebUtsav when, in our DebConf India team meeting, Abraham, mentioned that a college in Kochi wants to host a Debian event. As most folks were already engaged in DebConf related stuff, it was mostly he who helped and guided them for the event. Though, in the meeting, Abraham and Kiran joked around on the line that we might have someone coming from Gurugram (referring to me). Back then, I had no plans of attending the event; I didn't know how things would change eventually, and I, along with [Ravi](https://ravidwivedi.in/) were going to attend the event eventually and speak at it too. 

The concept of DebUtsav was conceived in the Debian India community on the premise that it's not possible to have all talks directly related to Debian and/or Free Software. The first of DebUtsav's was also held in Kochi in 2017. Later, DebUtsav Goa and Delhi also took place. You can check out DebUtsav website archives [here](https://gitlab.com/debutsav). Looking at this archive, it seems DebUtsav predates DebUtsav Kochi 2017. Maybe someone who attended those can shed some more light on the origin.

Coming back to DebUtsav Kochi 2023, seeing that the event is on a Saturday and remembering how fun Debian IRL events are, I eventually decided to attend the conference anyhow. I asked Ravi if he wanted to join, and he agreed. On learning about this later, Abraham called and asked if I could give a talk on the Debian community, as I actually came through the community route, doing community stuff to become a Debian Developer. I agreed without much thought (I usually agree to all speaking engagements at Debian events somehow; I speak too much sometimes :)), as I was in train then. Eventually, I had the (in my mind, brilliant) idea to name my talk "How I Became a Debian Developer, and you could too".

As usual, Ravi and I met directly at the Delhi airport, from where we had our Kochi flight. Due to busy days preceding the conference, I hadn't fully prepared my presentation uptil the day of the conference. We landed a day before in Kochi, but I was too tired to be up for completing the presentation, so I asked Ravi to wake me up at 6 in the morning, which he indeed did. I managed to cobble together the final presentation and structure in 1.3 hours. Then we walked to the venue, which was some 900 meters from our hotel, where we met my speaker POC, Arjun, who was a first-year and core team member of FOSSMEC there. He helped me with various things during the conference, for which I'm grateful. Then I got busy meeting old and new friends.

![Event standee](/img/standee-from-kurian.jpg)
<div style="text-align: center">
    <i>This was my debut on an event standee.</i>
    <br>
    <i>Image stolen from <a href="https://kurianbenoy.com/posts/2023/debutsav_talk/">Kurian's blog</a> :P </i>
</div>

The conference started with a lamp lighting ceremony, followed by a panel discussion on the topic "Introduction to Debian: Exploring the Foundations of Open Source." . Abraham, Abhijit, Bilal and I were on the panel with a moderator. In the first segment, an already prepared set of questions was posed to us, which turned out a bit formal. The team had arranged for the audience to pose their own questions, which were in turn displayed on screen. In the next segment, the moderator helped us go through the questions, which ranged from how stable releases are managed to opinions on Snap and AppImage to what desktop environment we run.  This turned out to be an interesting discussion. Following this, attendees moved to their respective talk venues. I had my talk in set 1, so I went there. My talk was preceded by Andrew Bastin's talk on "A journey in the world of sync systems". He presented the talk like a story, where he mentioned the name of the project first and walked us through the journey before concluding with revealing the full name, which directly correlated with the journey. Andrew did a great talk, filled with quirky anecdotes, humor, and technical facts that essentially held my attention throughout the time he was speaking at the dias. I really looked up to his delivery and the ease with which he gave the talk. It essentially made me nervous for my own talk. His talk gave me multiple pointers to work upon for improving my talks. Thanks, Andrew.

![Panel discussion](/img/debutsav-kochi-panel.jpg)
<div style="text-align: center">
    <i>Abraham and me during the panel discussion </i>
</div>

![My talk](/img/debutsav-kochi-talk.jpg)
<div style="text-align: center">
	<i>Me presenting my DD journey</i>
</div>

Next came my talk, which turned out better than I had anticipated, and I did manage to speak for 30 minutes straight without knowing as I didn't have any clock in sight. My talk, "How I became a Debian Developer, and _you could too_" chronicled my journey of becoming a Debian Developer, non uploading. When Abraham called me to present a talk on the Debian community, I thought about how the Debian community functions. But as I gave the topic some thought, I found that just telling the audience what and how things work on Debian would be uninteresting. Making a story about it would spice things up a little. Then the idea of telling my journey and connecting it with how things are done in the Debian community came to me, which I formulated into the talk. My talk was followed by a small Debian quiz and then [Kurian](https://kurianbenoy.com/)'s talk on ["AI with Malayalam Computing"](https://kurianbenoy.com/posts/2023/debutsav_talk/) which was dense with facts and AI, which isn't my area. 

The good folks at FOSSMEC sent some pictures as well, so I have more photos than usual to share here. 

![Group photo](/img/debutsav-kochi-group-photo.jpg)
<div style="text-align: center">
    <i> DebUtsav Kochi attendees group photo. 
    <a href="/img/debutsav-kochi-group-photo.jpg">Click to enlarge</a>
    </i>
</div>
<br>

![FOSSMEC volunteer team behind DebUtsav](/img/debutsav-kochi-fossmec-volunteer-team.jpg)
<div style="text-align: center">
    <i>FOSSMEC volunteer team behind DebUtsav </i>
</div>

This time around, we did manage to roam around the city for some sightseeing. We managed to cover Marine Drive, Kochi Water Metro, Fort Kochi and Jew Town, which were beautiful and colorful. Going around Kochi Water Metro, with large windows through the backwaters, was real fun. I had never thought a metro-like service could be implemented for daily commutes; it would be a real good use of the water bodies of a city built around water. 

![Jew Town Kochi](/img/kochi-jew-town-street.jpg)
<div style="text-align: center">
    <i> A Street in Jew Town, Kochi </i>
</div>

<br>

![Kochi Water Metro](/img/kochi-water-metro.jpg)
<div style="text-align: center">
    <i> Kochi Water Metro </i>
</div>

Overall, it was a fun and productive trip. I also got to know the geography of Kochi before coming back here for DebConf.  
