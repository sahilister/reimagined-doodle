---
title: "Experience Volunteering for MiniDebConf India 2021"
date: 2021-01-28T20:05:34+05:30
tags: ["mdc", "debian", "debianindia", "experience", "conference", "volunteering", "100DaysToOffload"]
---
This past weekend we had the [MiniDebConf India 2021](https://in2021.mini.debconf.org/) and I was fortunate enough to be in the orga(nizing) team. It did turn out to be a one big event in terms of numbers too.

Some quick stats:
- 2 days of online conference with 2 parallel tracks, Buzz and Rex.
- 6 languages - English, Hindi, Malayalam, Telugu, Kannada and Marathi.
- 48+ events in form of talks, BoFs, workshops and lightning talks.
- 100+ speakers.
- And probably the biggest MiniDebConf in terms of events \o/.

Reminiscing how things started almost 50 days ago in December, we started thinking we’re already late with starting the preparations. Frankly speaking, I had doubts if we be able to pull that of but as being part of some events have taught me to just go with the flow and things usually fall in place somehow. Initially the teams were defined, and I somehow landed in the publicity team. Also, as I had previous experience with [volunteering in DebConf20](/2020/09/experience-attending-debconf20/) and MDCO2 Gaming Edition with the video team, I also added my name to the video team.

The publicity team started sending out Call for Proposal (CfP) on 10th Dec, with last submission date being 10th Jan. It was publicized in the mailing lists, [Diaspora](https://poddery.com/people/0d4badb3092b7c5c) (the original MiniDebConf India social media platform, which in someway saved [poddery.com](https://poddery.com/) back in the days), [Fediverse](https://pleroma.debian.social/debianindia) (@debianindia on Debian's pleroma instance), IRC and Telegram channels. Fast forward almost a month, the CfP end date was nearing, and we had less than 10 submission and that was a worrying thing. The orga team was being told to make submissions in their languages so that we could atleast have a respectable number. On the submission deadline day, we had a meeting discussing the situation at about 8:00 pm. We had about 15-18 submissions at the start (a fairly okay-ish number, had 20 in mind initially). By the time the meeting ended we had about 24-25 submissions/ideas (the orga team went in to put a bunch of additional events, including one of mine "Introduction to Debian in Hindi"). And then the floodgates opened. The folks at [Free Software Movement of India](https://www.fsmi.in/#/) (FSMI) and [Swecha](https://swecha.org/) started submitting talks and events. In the span of 3-4 hours we went from 25 submissions to 50+ proposals. It came this suddenly that, by 12:30 am, Anupa was asking the website team to "please shut down the submission link". The submissions kept pouring in until the very end. Now a mixed feeling started arising. Happy on the part that we got these many proposals and tensed that we now have to tackle these many of them. After some discussion and consultations, the content team approved most of the proposals. After another round of discussions, the idea of having two parallel tracks was suggested which was duly confirmed with the #debconf-video team. It was probably the first time that any MiniDebConf had parallel tracks. The #debconf-video team came up the two track names, namely Buzz and Rex, probably from the first and second Debian named [releases](https://www.debian.org/doc/manuals/project-history/detailed.en.html#rel-1).

Now enough video volunteers (talkmeister and talk director) were to be trained, the speakers were also to be given the setup go through, recordings needed to be reviewed and uploaded on the server (speakers were encouraged to submit recordings of events prior so as to avoid any Internet related issues, QnA were live through Jitsi). Being in the video team, I got the responsibility and access to the video team server to review and upload recordings (as received from speakers). The original deadline for submission was 17th January, but we only received 10 submissions by then (me too didn't submit :D but we were expecting a two-day delay due to original delay in sending out confirmation mails). 

Then came the week of the conference. The video training was conducted, speakers introduction was handled and recordings were being uploaded daily as submissions poured in. Now I didn't mention that I had three events under my belt; A talk on ["Introduction to Debian in Hindi"](https://in2021.mini.debconf.org/talks/32-introduction-to-debian-hindi/), a BoF titled ["Digital privacy in Indian context and some Free software tools and services to the rescue in Hindi"](https://in2021.mini.debconf.org/talks/48-bof-digital-privacy-in-indian-context-and-some-free-software-tools-and-services-to-the-rescue-hindi/) and the [closing ceremony](https://in2021.mini.debconf.org/talks/55-closing-session/). These need their individual sections so will talk about them in a minute, after describing the whole conference.

The big day came. The start was at 11:00 am, and we were already short on video volunteers, but the team was ready to jump in wherever needed. On the recordings submission/upload front, we had about 15 recordings still to come and upload on the start of day one. And then my electricity went out. I literally couldn't do anything for the next hour. At about 12:00 pm, the electricity came, and I jumped back in action with the team. Every now and then between volunteering, a new recording submission would arrive, and I would drop, review and upload the submission. Then came the real comical/tense situation. Two talks scheduled for the same afternoon time slot (in different tracks) were still to recieve their recordings. Both speakers started uploading on their end at 30 minutes from the time slot. Received first talk 18 minutes before going live, which was uploaded without any review (which came back to haunt). Now came the second talk, which was received that via Telegram at about 5 minutes from the time slot so asked the talkmeister to delay the talk by 5 minutes. As soon as I started figuring out how to get the recording from Telegram to desktop to be uploaded on server, the first talk folks (which was already live on stream) raised an alarm that the recording wasn't the one for the talk. I checked and saw that the wrong video link was submitted which eventually went on live conference stream. I switched back, got the real link from the submitter and uploaded that quickly. That talk went ahead and started again with a 5 minutes delay. As for the second talk, we decided to postpone it latter in the day (which it eventually did, without any issue). The team had enough committed folks who took care of everything. The second day was relatively calm as compared to day one. Issues with the stream, volunteers and everything else was almost sorted by the end of day one.

You can view the talk recordings on [Peertube](https://peertube.debian.social/video-channels/mdcoin2021/videos?a-state=42) or on Debian's [video archive](https://meetings-archive.debian.net/pub/debian-meetings/2021/MiniDebConf-India/).

Now coming back to my talks and BoF. Here's a list and my takes: 

#### Introduction to Debian in Hindi

This was somewhat an impromptu suggestion. Rajudev was doing this talk in Marathi, so he floated this talk idea for Hindi and folks naturally turned to me for it (due to me knowing Hindi). I agreed as Rajudev gave me some good pointers to talk about plus it was only going to be a short talk of 15-20 minutes. The recording kept getting delayed due to my non-completion of the talk slides, which was probably the hardest part of all. Pirate Praveen helped by sharing his presentation on the same topic. Eventually I recorded it in one go on the 19th and uploaded (and submitted it to myself to be uploaded on the server by myself ;)) on the same day without any cuts.

Links: [Talk page](https://in2021.mini.debconf.org/talks/32-introduction-to-debian-hindi/), 
[Peertube](https://peertube.debian.social/videos/watch/5a4caf3c-94a4-46ad-81d1-82bc7ea60a41) and Debian [video archive](https://meetings-archive.debian.net/pub/debian-meetings/2021/MiniDebConf-India/32-introduction-to-debian-hindi.webm). 


#### BoF: Digital privacy in Indian context and some Free software tools and services to the rescue in Hindi

This was probably the most prepared amongst all three events I presented partly because there were more folks involved. Initially there were no proposal submissions in Hindi, so I and Ravi came up with the idea to discuss this topic on camera, as we usually do over some of our Jitsi calls. The initial set of people we expected for the BoF were busy, so initially we only had me and Ravi for it. As time passed; Rojin, Riya joined and on the day of the conference we asked Akshay (who was also director in our BoF) to join in the discussion. The discussion was good, we had prepared some pointers to discuss but most of them were skipped due to time constraints. Less talk on free software was done due to us focusing more on the privacy side of things but overall it a nice experience.

Links: [Talk page](https://in2021.mini.debconf.org/talks/48-bof-digital-privacy-in-indian-context-and-some-free-software-tools-and-services-to-the-rescue-hindi/), 
[Peertube](https://peertube.debian.social/videos/watch/417bd62c-cce1-494a-b96c-b0e39741cb09) and Debian [video archive](http://meetings-archive.debian.net/pub/debian-meetings/2021/MiniDebConf-India/48-bof-digital-privacy-in-indian-context-and-some-free-software-tools-and-services-to-the-rescue-hindi.webm). 

#### Closing Ceremony

In the final meeting, on CfP close date, we were discussing about opening and closing ceremony. I believe it was Pirate Praveen who said that Anupa should take one and me the other or something like that. I took the closing so Anupa took the opening ceremony (for which she prepared a collage of welcome videos and a nice speech). Honestly, I didn't have any grand ideas about it and wanted to give some views/experiences and to introduce the team. We were already in an upbeat mood when the closing neared. I went in and started sharing my experience and whatever came to my mind at the moment (did spoke quite a while back then) and eventually opened the Jitsi call for group photograph. You can see the photo on [pleroma](https://pleroma.debian.social/media/cda0f6e54d4d3fcd2afa4503e6ac7c057c557b25f7d5224f6d97d204e3a4b7e2.png), on [Debian wiki](https://wiki.debian.org/DebianIndia/MiniDebConfOnlineIndia2021?action=AttachFile&do=get&target=mdcinew.png) or on [Internet archive](https://web.archive.org/web/20210210124409if_/https://pleroma.debian.social/media/cda0f6e54d4d3fcd2afa4503e6ac7c057c557b25f7d5224f6d97d204e3a4b7e2.png). 

Links: [Talk page](https://in2021.mini.debconf.org/talks/55-closing-session/), 
[Peertube](https://peertube.debian.social/videos/watch/c0064cae-1ec6-47ba-80dc-9ff198cbc32e) and Debian [video archive](https://meetings-archive.debian.net/pub/debian-meetings/2021/MiniDebConf-India/55-closing-session.webm)    (this I can recommend watching, somewhat a video version of this post).


Overall it was an amazing experience, thanks to everyone involved and the community around Debian and Debian India. A special thanks to Sruthi for bringing MDC to India and Anupa for making the whole event possible by coordinating everything. 

So that's an end for this one, see you in another Debian or Free Software event o/ 

And and a special shoutout to Abbyck ;)

_PS: This was probably one of the long ones among my blogs. It took almost 4 days to write and edit. Too much time it took :sigh:_

_PSS: As video team member, also recieved talk recording shared via GitHub repositery, Telegram direct messeges amongst other places._
