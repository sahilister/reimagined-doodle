---
title: "Debian India Online Bookworm Release Party"
date: 2023-06-14T08:45:24+05:30
tags: ["debian", "bookworm", "release-party", "debianindia"]
---

As the tradition dictates, we at Debian India organised an online release party for the next stable release of Debian, codenamed bookworm on 10th June. The initial discussion started a while back, but we had a planning meeting just 3–4 days before the release party date. As most of us were busy with DebConf or DebUtsav Kochi stuff, we didn't have time to explore new ideas for events . The consensus was to have a technical overview of changes, a Debian trivia quiz and ending with SuperTuxKart gaming event. As we used to keep discussion improvements to Debian, we decided to hold a discussion on What Debian lacks/need to improve. This was later taken up by Akshay. A call for proposal was sent along with announcement on debian-dug-in mailing lists and Debian India channels, though no one took that offer. 

As Jitsi usually fumbles with large crowds with camera on. We choose to have the party on BigBlueButton. Like last year, Abhas generously supported us with a room on his BigBlueButton instance, and we created a redirect behind 12.debian.org.in to this room. 

On 10th, the release party started around 7:45 PM IST. As usual, the mood was light and fun. Around 16 people joined the party at it's highest. [Abraham](https://abrahamraji.in/) started with a technical overview covering changes in bookworm. A major highlight for me was the inclusion of non-free firmware on official ISOs, which would essentially ease one of the biggest pain point for new user on-boarding. Now, I would be able to directly recommend Debian to new users, rather than Linux Mint, due to getting the firmware right. Sidenote, I too personally struggled with getting the drivers right for almost a month when I started using Debian way back in 2020. Coming back, technical overview was followed by Debian trivia by [Anupa](https://nm.debian.org/person/anupa/). Initially I was confident about the quiz, though the questions Anupa brought were harder than I had anticipated and taught quite a few things about Debian. This was followed by [Akshay](https://asd.learnlearn.in/)'s "Discussion - What Debian lacks/need to improve" session. Akshay, being an Arch user, had some insights to highlight where Debian can improve. He had collected statements and walked us through the viewpoints. Meanwhile, Abraham also gave a quick overview of a draft re-design for debian.org.in  website to ease user onboarding, which he mentioned we lack. Then we took the group photo and moved onto the SuperTuxKart gaming party, which was fun as always. We did record the party, but I have no ETA when this would be available. 

I'm yet to upgrade servers under my management to bookworm, as I usually wait it out a few days after a release so that teeny-tiny bugs are reported and iron-ed out and for cooling down of download mirror loads. Maintaining a Matrix instance through synapse has made me cautious about not being on the bleeding edge of new releases. Recently, also got a new perspective that with the now omni-present mix-match usage of third party repositories, waiting a little more bit for them to be up to speed with new stable would remove much of the troubles post updates. 

Good then, and see you in DebUtsav Kochi and/or DebConf Kochi :)

<div style="text-align: center">

![Group photo](/img/debian-12-release-party-group-photo.png)
<i> Goup photo. <a href="/img/debian-12-release-party-group-photo.png">Click to enlarge</a> </i>
</div>
