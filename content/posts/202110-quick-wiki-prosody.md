---
title: "Quick-wiki: Prosody"
date: 2021-10-03T10:52:11+05:30
tags: ["quick-wiki", "prosody", "xmpp", "server", "system-adminstration", "100DaysToOflload"]
---
_Last update: 01/2025, Prosody v0.12.5-1~bpo12+1 from Debian._

This is a WIP [quick-wiki](https://www.urbandictionary.com/define.php?term=quick-wiki) for quick reference for [Prosody](https://prosody.im/), an [XMPP](https://xmpp.org/about/technology-overview/) server software and XMPP in general.

I do had an XMPP account from my diasp.in account, but things being up down sometimes and my urge to experiment and learn how prosody works got me into this installation. It's a mess, but it's super lightweight to run. 

## Table of Content

- [RFCs](#rfcs)
- [Installation and Configurations](#installation-and-configurations)
- [Prosody modules](#prosody-modules)
- [Certs](#certs)
- [Accounts](#accounts)
- [Debugging and logs](#[debugging-and-logs)
- [Glossary](#glossary)
- [Quick links](#quick-links)


### RFCs

- [RFC 6120:](https://www.rfc-editor.org/info/rfc6120) XMPP Core.
- [RFC 6121:](https://www.rfc-editor.org/info/rfc6121) XMPP IM.

### Installation and Configurations

Most of the initial setup was done by following DO's ['How To Install Prosody on Ubuntu 18.04'](https://www.digitalocean.com/community/tutorials/how-to-install-prosody-on-ubuntu-18-04), on Debian machine with following changes:
- python3-certbot-nginx instead of certbot from Ubuntu PPA.
- prosody from Debian main.
- port 5269 for s2s communication instead of 5322.

#### Ports

- 5222/tcp: For client connections.
- 5269/tcp: For server to server connections.
- 5280/tcp: Serving files through HTTP.
- 5281/tcp: Serving files through HTTPS.

#### DNS

Atleast two DNS A records needs to be added:
- _chat.example.org_: main XMPP address.
- _groups.example.org_: for multiuser chat (MUC).

A third DNS record maybe required for uploads.

Prosody documentation mentions that by [using SRV records](https://prosody.im/doc/dns#srv_records), a user with XMPP address _user@chat.example.org_ can use DNS redirection to use _user@example.org_ address.

The following is the DNS configuration:
```dns
_xmpp-client._tcp.example.org. 18000 IN SRV 0 5 5222 chat.example.org.
_xmpp-server._tcp.example.org. 18000 IN SRV 0 5 5269 chat.example.org.
```

Though implementing shows following issue in prosody logs:
```
info    Incoming s2s stream example.com->example.org closed: This host does not serve example.org
```

### Prosody modules

In prosody, most of the features are enabled via modules(plugins).

Debian has prosody-modules package, but it misses some community and early-stage modules. Fetched the modules from directly from prosody's mercurial by:

```bash
  hg clone https://hg.prosody.im/prosody-modules/ prosody-modules
```
And adding path to `plugin_paths` variable in prosody conf.

#### To update modules

```bash
hg pull --update
```

#### Modules enabled

- [roster:](https://prosody.im/doc/modules/mod_roster) Allow users to have a roster/friend list.
- [saslauth:](https://prosody.im/doc/modules/mod_saslauth) Authentication for clients and server.
- [tls:](https://prosody.im/doc/modules/mod_tls) Add support for secure TLS on c2s/s2s connections.
- [dialback:](https://prosody.im/doc/modules/mod_dialback) s2s dialback support. Identity verification through DNS system before accepting new s2s traffic.
- [disco:](https://prosody.im/doc/modules/mod_disco) Service discovery. Essentially allows clients to discover MUCs, file transfer or other services supported by the server.
- [carbons:](https://prosody.im/doc/modules/mod_carbons) Keep multiple clients/devices in sync.
- [pep:](https://prosody.im/doc/modules/mod_pep) Enables users to publish their avatar, mood, activity, playing music and more.
- [private:](https://prosody.im/doc/modules/mod_private) Private XML storage (for room bookmarks, etc.).
- [blocklist:](https://prosody.im/doc/modules/mod_blocklist) Allow users to block other users.
- [vcard4:](https://prosody.im/doc/modules/mod_vcard4) User profiles (in PEP).
- [vcard_muc:](https://modules.prosody.im/mod_vcard_muc.html) To allow defining avatar for MUC rooms.
- [vcard_legacy:](https://prosody.im/doc/modules/mod_vcard_legacy) Conversion between legacy vCard and PEP avatar.
- [limits:](https://prosody.im/doc/modules/mod_limits) Enable bandwidth limiting for XMPP connections, both c2s and s2s.
- [uptime:](https://prosody.im/doc/modules/mod_uptime) Server uptime.
- [ping:](https://prosody.im/doc/modules/mod_ping) Replies to pings with pong. Helps client establish if they have a stable connection to server or not.
- [mam:](https://prosody.im/doc/modules/mod_mam) Store messages in archive. Also used by carbons for client sync.
- [csi_simple:](https://prosody.im/doc/modules/mod_csi_simple) Mobile optimizations.
- [admin_adhoc:](https://prosody.im/doc/modules/mod_admin_adhoc) Admin through XMPP client. Gajim, Pidgin and others supports it, dino doesn't I believe.
- [bosh:](https://prosody.im/doc/modules/mod_bosh) XMPP over HTTP, ie from browser clients. Also, helpful when client is behind firewalls.
- [http_files:](https://prosody.im/doc/modules/mod_http_files) Serve static files from a directory. Also, if chat is unencrypted, gives a public browser link like _`http://prosody.example:5280/files/`_ for files.
- [http_upload:](https://modules.prosody.im/mod_http_upload.html) Community module. For file uploading in chat. Says not to be added in module enabled, but seems to work anyhow, _To be investigated: how file upload works in other implementations without a separate domain or with it_.
- [posix:](https://prosody.im/doc/modules/mod_posix) POSIX stuff.
- [groups:](https://prosody.im/doc/modules/mod_groups) Shared roster support ie server side shared friend/contact list like spaces in matrix.
- [watchregistrations:](https://prosody.im/doc/modules/mod_watchregistrations) To get notified on new registrations. People automatically see each other in contact list.
- [muc_mam:](https://prosody.im/doc/modules/mod_muc_mam) Room archives.
- [smacks:](https://modules.prosody.im/mod_smacks.html) Stream management, allow a client to resume a disconnected session, and prevent message loss.
- [bookmarks2:](https://modules.prosody.im/mod_bookmarks2.html) This module fetches users’ bookmarks.
- [cloud_notify:](https://modules.prosody.im/mod_cloud_notify.html) It allows clients to register an “app server” which is notified about new messages while the user is offline, disconnected or the session is hibernated by mod_smacks.


[List of core modules](https://prosody.im/doc/modules) with description.

[List of prosody community modules](https://modules.prosody.im/) with description.

A reload is required for adding or removing a module from configuration.

### Certs

Wrote a [blog post for certificate management for Prosody with Nginx and Certbot](https://blog.sahilister.in/2025/01/prosody-certificate-management-with-nginx-and-certbot/). 

### Accounts

- Create a new user, a password entry prompt follows.
```bash
prosodyctl adduser user@chat.example.org
```

- Change a user password, a password entry prompt follows.
```bash
prosodyctl passwd user@chat.example.org
```
- Delete a user account.
```bash
prosodyctl deluser user@chat.example.org
```

### Debugging and logs

- See all prosody related information.
```bash
prosodyctl about
```
- Check issues with full installation. Combination of checking configuration, DNS and certs.
```bash
prosodyctl check

# for individual checks only
prosodyctl check {config|dns|certs}
``` 

### Glossary

- **BOSH:** Bidirectional-streams Over Synchronous HTTP.
- **c2s/C2S:** client to server.
- **s2s/S2S:** server to server.
- **Roster:** contact list.
- **OMEMO:** OMEMO Multi-End Message and Object Encryption. An extension to XMPP that allows encryption with the Signal Protocol.
- **MUC:** Multi-User Chat.
- **MIX:** Mediated Information eXchange. MIX is a potential successor to MUC. [Specification](https://xmpp.org/extensions/xep-0369.html).
- **JID:** Jabber ID or XMPP address.
- **XEP:** XMPP Protocol Extension. XMPP standards they're.
- **XMPP:** Extensible Messaging and Presence Protocol.
- **XSF:** XMPP Standards Foundation.

### Quick links

- [Release notes](https://prosody.im/doc/release)
- [All XEPs](https://xmpp.org/extensions/)
- [XMPP Compliance tester](https://compliance.conversations.im/)
- [XMPP monthly newsletter](https://xmpp.org/categories/newsletter/)
