---
title: "Late, Late Experience Post of DebConf21"
date: 2022-04-18T07:47:48+05:30
tags: ["debconf", "experience", "debian"]
---
DebConf21 was held online from August 24th to August 28th, 2021. As the tradition dictates, I wanted to jot down the experience for my future self and others, but it kept slipping. Now, as DebConf22, Kosovo looms (and preparations for DebConf23 here in India begins), I feel obliged to quickly write down as much as I can remember. As this year's DebConf is in-person again, in Kosovo, I won't be attending it, so no experience post this year around. 

This time our focus was to bring a separate Indic track featuring talks and workshops in Indic languages. Though a separate Indic track wasn't added, five Indic languages were added in talk submission page to be directly included in main DebConf. We did outreach for bringing people to submit talks in their native language. Finally, on the lower end, but we did manage to persuade some folks to submit talks.

As usual this time too during the conference I volunteered to do the talk meistering and talk direction. The setup was similar to [DebConf20](https://blog.sahilister.in/2020/09/experience-attending-debconf20/) and [MiniDebConf India](https://blog.sahilister.in/2021/01/experience-volunteering-for-minidebconf-india-2021/) though this year was more fun because of the inclusion of a number of Indic language talks, four out of five of which I didn't understand. 


This year, four Hindi talks were accepted (including mine), which was the biggest number ever (as far I as I know). I was elated, seeing this. Alas, three of the talks were cancelled at the last moment. Mine was the only Hindi talk in the end.

About talk; my first talk was a short talk titled: [_डेबियन में योगदान कैसे करें_ ](https://debconf21.debconf.org/talks/43-/) (How to contribute to Debian). I prepared the talk with 2 pages of pointers on notebook and taking presentation help from Praveen. Made the slides in Cryptpad and OBS, capturing 1/4th of the screen with presentation and webcam. As usual, made the video in one take. The [talk etherpad](https://salsa.debian.org/debconf-team/public/data/dc21-online/-/raw/master/etherpads/txt/43-.txt) looks funny now :) Someone mentioned (don't remember who), that I was doing all the stuff and by chance during this talk QnA, talk meister for talk, Sruthi's internet blinked. As I was already the talk director and was on call, I stepped in to give the closing notes. You can see it starting at 20.16 mins into the video. Finally, I edited the final cut of my video as well as the video team :D

I was also part of [BoF on Growing the Debian Community in India](https://debconf21.debconf.org/talks/66-growing-the-debian-community-in-india/). I did particularly like the fact folks put in their honest views, leading to prolonged discussions on the same. As the discussion went on, the 45-minute time seemed less. The BoF saw the participation of Anupa, [Sruthi](https://debconf21.debconf.org/users/srud/), [Akshay](https://asd.learnlearn.in/), [Praveen](https://social.masto.host/@praveen), [Abraham](https://abrahamraji.in/) and [Utkarsh](https://utkarsh2102.com/).

Participated in the DebConf closing ceremony and party thereafter with online with _gol gappa chat_. It was delicious as always. After the closure, we also did an 8.5 hour late night video call, discussing Debian, software and life in general.

Initial preparations for DebConf23 India have already started, scheduled for next year. Will be actively participating in the prep, albeit online. DebConf23 seems to be the first Debian event I would attend in-person. Come join us, if you want to make DebConf23 India a success.

_PS: [Ravi](https://ravidwivedi.in/) mentioned that he showed my Hindi talk to his mother, and now she knows what Debian is. A small win._
