---
title: "Services That I (Self) Host - July 2021 Edition"
date: 2021-07-17T15:40:57+05:30
tags: ["hosting", "servers", "selfhosting", "100DaysToOffload"]
---
It's been a year since I started fiddling with servers (expiration of AWS 12 months free tier on 30th June told me that). Most of the stuff just manages fine (or that's what I believe).

Presently, following are all the virtual rigs I manage for personal stuff:
- Two **AMD Compute VMs** on always free tier with 1/8th OCPU with 1 GB RAM and 2 GB swap each.
- **Azure B1S Standard** on student account with 1 GB RAM and 2 GB swap. 
- **Hetzner CPX21** with 3 vCPU, 4 GB RAM and 4 GB swap.


These rigs power the following, ever evolving list of software packages:

- **Nextcloud**, through snap. Snap, snap because LAMP stack is messy (atleast for me) and getting the native package up for Nextcloud is too. More than files sharing,  it now powers my music streaming, RSS feed reader, notes, forms and a bunch of other stuff through add-ons. Snap handles updates on its own but nowadays, it's giving storage troubles, let's see about that later (or it will go down and come back as fresh install which I dread, considering the number of additional add-on services and apps I have to reconfigure).
- **The Lounge** as an IRC bouncer and web client. It's a progressive web app (PWA) so added it on mobile for native-like experience without a dedicated app. Probably the coolest and easiest IRC setup on mobile, though I usually lurk IRC through desktop more ;). IRC was one of the biggest reason I quickly picked a Hetzner server and migrated The Lounge from AWS as the free tier credits expired on 30th.
- Temporary file sharing and pastebin through **linx-server**, using docker. Why docker? Just that I was fiddling with docker when I came across this, and hence linx-server rolled with docker.
- **Blog mirror**, with this [mirror script](https://blog.sahilister.in/2021/04/script-to-automatically-mirror-blog-on-seperate-domain/). It mirrors my blog hosted through GitLab Pages on my server, viewable at [blog.mirror.sahilister.in](https://blog.mirror.sahilister.in/).
- **Wireguard** for virtual private network (VPN).
- **Searx** as meta-search engine.

I'm working to setup the following service:
- **Prosody**, an XMPP server for experimenting and learning the complicated XMPP ecosystem.

_PS: This post was inspired from [this thread](https://bsd.network/@h3artbl33d/106541046199854963) on Mastodon (archive link [here](https://web.archive.org/web/20210708073903if_/https://bsd.network/@h3artbl33d/106541046199854963). Also, it seems most folks have reply set to some non-public setting, login through your fedi account to see the complete thread). People self host crazy amount of stuff and  email ;)_
