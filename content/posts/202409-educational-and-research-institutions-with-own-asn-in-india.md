---
title: "Educational and Research Institutions With Own ASN in India"
date: 2024-09-20T21:59:59+05:30
tags: ["internet", "asn", "network","nren", "list"]
---

Another one of the ASN list. This turned out longer than I expected (which is good). If you want to briefly understand what is an ASN, my [Personal ASNs From India](https://blog.sahilister.in/2024/07/personal-asns-from-india/) post carries an introduction to it.

Now, here're the Educational and Research Institutions with their own ASN in India, which I could find:
- [AS2697](https://bgp.tools/as/2697) Education and Research Network
- [AS9885](https://bgp.tools/as/9885) NKN Internet Gateway
- [AS23770](https://bgp.tools/as/23770) Tata Institute of Fundamental Research (used as National Centre for Biological Sciences network)
- [AS38021](https://bgp.tools/as/38021) Network of Indian Institute of Foreign Trade
- [AS38620](https://bgp.tools/as/38620) National Knowledge Network
- [AS38872](https://bgp.tools/as/38872) Indian School of Business
- [AS45340](https://bgp.tools/as/45340) B.M.S College of Engineering
- [AS55296](https://bgp.tools/as/55296) National Institute of Public Finance and Policy
- [AS55479](https://bgp.tools/as/55479) Indian Institute of Technology, Kanpur
- [AS55566](https://bgp.tools/as/55566) Inter University Centre for Astronomy and Astrophysics
- [AS55824](https://bgp.tools/as/55824) NKN Core Network
- [AS55847](https://bgp.tools/as/55847) NKN Edge Network
- [AS56056](https://bgp.tools/as/56056) AMITY-IN
- [AS58703](https://bgp.tools/as/58703) Amrita Vishwa Vidyapeetham
- [AS58758](https://bgp.tools/as/58758) Tata Institute of Fundamental Research (used as Homi Bhabha Centre for Science Education (HBCSE) network)
- [AS59163](https://bgp.tools/as/59163) GLA University
- [AS59193](https://bgp.tools/as/59193) Indian Institute of Technology, Hyderabad
- [AS131226](https://bgp.tools/as/131226) Indian Institute of Technology, Roorkee
- [AS131473](https://bgp.tools/as/131473) SRM University
- [AS132423](https://bgp.tools/as/132423) Indian Institute of Technology, Bombay
- [AS132524](https://bgp.tools/as/132524) Tata Institute of Fundamental Research (used as main campus network)
- [AS132749](https://bgp.tools/as/132749) Indraprastha Institute of Information Technology, Delhi
- [AS132780](https://bgp.tools/as/132780) Indian Institute of Technology, Delhi
- [AS132984](https://bgp.tools/as/132984) Uka Tarsadia University
- [AS132785](https://bgp.tools/as/132785) Shiv Nadar Institution of Eminence Deemed to be University
- [AS132995](https://bgp.tools/as/132995) South Asian University
- [AS133002](https://bgp.tools/as/133002) Indian Institute of Tropical Meteorology
- [AS133233](https://bgp.tools/as/133233) S.N. Bose National Centre for Basic Sciences
- [AS133273](https://bgp.tools/as/133273) Tata Institute of Social Sciences
- [AS133308](https://bgp.tools/as/133308) Indira Gandhi Centre For Atomic Research
- [AS133313](https://bgp.tools/as/133313) Saha Institute of Nuclear Physics
- [AS133552](https://bgp.tools/as/133552) B.M.S. College of Engineering
- [AS133723](https://bgp.tools/as/133723) Institute for Plasma Research
- [AS134003](https://bgp.tools/as/134003) Centre For Cellular And Molecular Platforms
- [AS134023](https://bgp.tools/as/134023) Aligarh Muslim University
- [AS134322](https://bgp.tools/as/134322) Tata Institute of Fundamental Research (used as International Centre for Theoretical Sciences (ICTS) network)
- [AS134901](https://bgp.tools/as/134901) Indian Institute of Science Education And Research
- [AS134934](https://bgp.tools/as/134934) Institute For Stem Cell Biology And Regenerative Medicine
- [AS135730](https://bgp.tools/as/135730) Datta Meghe Institute Of Medical Sciences
- [AS135734](https://bgp.tools/as/135734) Birla Institute of Technology And Science
- [AS135835](https://bgp.tools/as/135835) Sardar Vallabhbhai Patel National Police Academy
- [AS136005](https://bgp.tools/as/136005) Raman Research Institute
- [AS136304](https://bgp.tools/as/136304) Institute of Physics, Bhubaneswar
- [AS136470](https://bgp.tools/as/136470) B.M.S. College of Engineering
- [AS136702](https://bgp.tools/as/136702) Physical Research Laboratory
- [AS137136](https://bgp.tools/as/137136) Indian Agricultural Statistics Research Institute
- [AS137282](https://bgp.tools/as/137282) Kalinga Institute of Industrial Technology
- [AS137617](https://bgp.tools/as/137617) Indian Institute of Management, Ahmedabad
- [AS137956](https://bgp.tools/as/137956) Indian Institute of Technology, Ropar
- [AS138155](https://bgp.tools/as/138155) Jawaharlal Nehru University
- [AS138231](https://bgp.tools/as/138231) Indian Institute of Information Technology, Allahabad
- [AS140033](https://bgp.tools/as/140033) Indian Institute of Technology, Bhilai
- [AS140118](https://bgp.tools/as/140118) Indian Institute of Technology Banaras Hindu University
- [AS140192](https://bgp.tools/as/140192) Indian Institute of Information Technology and Management, Kerala
- [AS140200](https://bgp.tools/as/140200) Panjab University
- [AS141270](https://bgp.tools/as/141270) Indian Institute Of Technology, Indore
- [AS141340](https://bgp.tools/as/141340) Indian Institute Of Technology, Madras
- [AS141477](https://bgp.tools/as/141477) Indira Gandhi National Open University
- [AS141478](https://bgp.tools/as/141478) Director National Institute Of Technology, Calicut
- [AS141288](https://bgp.tools/as/141288) National Institute of Science Education And Research Bhubaneswar
- [AS141507](https://bgp.tools/as/141507) National Institute of Mental Health And Neurosciences
- [AS142493](https://bgp.tools/as/142493) Sri Ramachandra Institute Of Higher Education And Research
- [AS147239](https://bgp.tools/as/147239) Lal Bahadur Shastri National Academy of Administration (LBSNAA)
- [AS147258](https://bgp.tools/as/147258) Dayalbagh Educational Institute
- [AS149607](https://bgp.tools/as/149607) National Forensic Sciences University
- [AS151086](https://bgp.tools/as/151086) Amrita Vishwa Vidyapeetham
- [AS152533](https://bgp.tools/as/152533) National Institute of Technology, Karnataka


Special Mentions

- [AS132926](https://bgp.tools/as/132926) Allen Career Institute
- [AS141841](https://bgp.tools/as/141841) Indian Institute of Hardware Technology Limited


Some observations:

- Only 14 (out of 67) networks have/announce IPv6 prefixes.
- Two networks have IX presence; [AS132785](https://bgp.tools/as/132785) Shiv Nadar at Extreme IX Delhi and [AS56056](https://bgp.tools/as/56056) Amity at DE-CIX Delhi.
- TIFR as an organization has 4 ASNs ([AS23770](https://bgp.tools/as/23770), [AS58758](https://bgp.tools/as/58758), [AS132524](https://bgp.tools/as/132524) and [AS134322](https://bgp.tools/as/134322)), BMS College of Engineering has 3 ([AS45340](https://bgp.tools/as/45340), [AS133552](https://bgp.tools/as/133552) and [AS136470](https://bgp.tools/as/136470)) and Amrita Vishwa Vidyapeetham has 2 ([AS58703](https://bgp.tools/as/58703) and [AS151086](https://bgp.tools/as/151086)).
- [AS2697](https://bgp.tools/as/2697), [AS9885](https://bgp.tools/as/9885), [AS38620](https://bgp.tools/as/38620), [AS55824](https://bgp.tools/as/55824) and [AS55847](https://bgp.tools/as/55847) ie Education and Research Network (ERNET) and National Knowledge Network (NKN) form the Indian [NREN](https://en.wikipedia.org/wiki/National_research_and_education_network).
- 43 network have NKN as their upstream, while BSNL comes second as upstream for 21.

Let me know if I'm missing someone.
