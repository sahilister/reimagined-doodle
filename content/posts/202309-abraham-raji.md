---
title: "Abraham Raji"
date: 2023-09-24T14:21:13+05:30
tags: ["abraham", "life", "loss", "friend", "debian", "abraham-raji"]
---

![Abraham with Polito](/img/abraham-1.jpg)

Man, you're no longer with us, but I am touched by the number of people you have positively impacted. Almost every DebConf presentations by locals, I saw after you carried how you were instrumental in bringing them there. How you were a dear friend and brother. 

It's a weird turn of events, that you left us during one thing we deeply cared and worked towards making possible since the past 3 years, together. Who would have known, that "Sahil, I'm going back to my apartment tonight" and casual bye post that would be the last conversation we ever had.

Things were terrible after I heard the news. I had a hard time convincing myself to come see you one last time during your funeral. That was the last time I was going to get to see you, and I kept on looking at you. You, there in front of me, all calm, gave me peace. I'll carry that image all my life now. Your smile will always remain with me. Who'll meet and receive me on the door at almost every Debian event (just by sheer co-incidence?). Who'll help me speak out loud about all the Debian shortcomings (and then discuss solutions, when sober :)).

![Abraham and me during Debian discussion in DebUtsav Kochi](/img/abraham-2.jpg)

It was a testament of the amount of time we had already spent together online, that when we first met during MDC Palakkad, it didn't feel we're physically meeting for the first time. The conversations just continued. 
Now [this song](https://www.youtube.com/watch?v=rtOvBOTyX00) is associated with you now due to your speech during MiniDebConf Palakkad dinner. Hearing this keeps on reminding me of all the times we spent together chilling and talking community (which you cared deeply about). IG now we can't stop caring for the community, because your energy was contagious. 

Now, I can't directly dial your number to listen - "Hey Sahil! What's up?" from the other end, or "Tell me, tell me" on any mention of the problem. Nor would I be able to send ref of usage of [Debian packaging guide](https://wiki.abrahamraji.in/simple-packaging-tutorial.html) in the wild. You already know about that text of yours. How many people that guide has helped with getting started with packaging. Did I ever tell you, I too got my first start with packaging from there. Hell, I started looking up to you from there, even before we met or talked. Now, I missed telling you, I was probably your biggest fan whenever you had the mic in hand and started speaking. You always surprised me all the insights and idea you brought and would keep on impressing me for someone who was just my age but was way more mature. 

Reading recent toots from Raju Dev made me realize, how much I loved your writings. You wrote 
[How the Future will remember Us](https://abrahamraji.in/post/how-future-will-remember-us/),  [Doing what's right](https://abrahamraji.in/right/) and [many more](https://abrahamraji.in/post/). The level of depth in your thought was unparalleled. I loved reading those, that's why I kept pestering you to write more, which you slowly stopped. Now I fully understand why though, you were busy, really busy helping people out or just working for making things better. You were doing Debian, upstream projects, web development, designs, graphics, mentoring, evangelist while being the go-to person for almost everyone around. Everyone depended on you, because you were too kind to turn down anyone. 

![Abraham and me just chilling around. We met for the first time there](/img/abraham-3.jpg)


Man, I still get your spelling wrong  :) Did I ever tell you that? That was the reason, I used to use AR instead. 

You'll be missed and will always be part of our conversations, because you have left a profound impact on me, our friends, Debian India and everyone around. See you! the coolest man around.

In memory:
* [Farewell Abraham video](https://www.youtube.com/watch?v=6n3_sabZy_w) by Vysakh Premkumar
* Tails project dedicating [5.17.1 release](https://tails.net/news/version_5.17.1/index.en.html)
* Hopscotch team dedicating [2023.8.1](https://github.com/hoppscotch/hoppscotch/releases/tag/2023.8.1)
* Athul dedicating [0.2.5 release](https://github.com/athul/waka-readme/releases/tag/v0.2.5) of waka-readme
* [Debian project](https://www.debian.org/News/2023/20230914)



PS - Just found you even had a [Youtube channel](https://www.youtube.com/@abrahamraji3699), you one heck of a talented man. 
