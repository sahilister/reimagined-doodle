---
title: "Why Should Everyone Read \"The Story of My Experiments With Truth\" by Mahatma Gandhi?"
date: 2021-04-18T15:27:27+05:30
tags: ["life", "book", "gandhi", "100DaysToOffload"]
---
The following was a part of an email conversation I had with [Akshay](https://asd.learnlearn.in/). 

Reading the book being mentioned in [about section](https://blog.sahilister.in/about/), he asked:
> Also, why should everyone read the autobiography of Gandhi? 

This was probably the second time when I pondered upon the significance of Mahatma Gandhi's autobiography _The Story of My Experiments With Truth_ in my life. (First was on the creation of blog's about section.) 

I replied (slightly corrected for clarity):
> […] I see you were engaged in My experiment with truth back then, apparently I too came across the book in 7th or 8th standard.

> I wasn’t someone who admired MK Gandhi before that. Furthermore, I just read it because I was just getting in the habit of reading and this was one book I wanted to “just” read, but it was different, it showed how to experiment. I have learned how monotonous life is, and how we can radically change it with experimentation. So I started fasting once every week on Wednesdays, not to lose weight but to build will power (which I believe I lacked). Still continuing with that after like 8-9 years. Taking back control is what it taught me.

> Then there was the way how passive resistance and perseverance helps get things done (which can only be undertaken by having a strong will for a cause). Being stubborn sometimes does help ;)

> Overall, it changed my life approach and now that I’m thinking, I do consider it kind of sacred (or closest to one).

