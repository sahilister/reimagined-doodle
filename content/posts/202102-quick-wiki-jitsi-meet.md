---
title: "Quick-wiki: Jitsi Meet"
date: 2021-02-04T09:16:57+05:30
tags: ["quick-wiki", "jitsi", "100DaysToOffload"]
---

_Last update: 09/2021, Jitsi Meet v2.0.6293 from upstream repos._

This is a [quick-wiki](https://www.urbandictionary.com/define.php?term=quick-wiki) for quick reference for my occasional Jitsi Meet installations.

## Table of Content

- [Limit RAM usage](#limit-ram-usage)
- [Restart setup](#restart-setup)
- [Check installation version](#check-installation-version)
- [Log files](#log-files)
- [Interface changes](#interface-changes)
- [Configuration](#configuration)
- [RTMP stream and record setup](#rtmp-stream-and-record-setup)
- [Glossary](#glossary)
- [Quick links](#quick-links)


### Limit RAM usage

Both jicofo and jvb are configured to take by default up to 3 GB of RAM each. On a machine with running both by default almost ~8 GB RAM is required. Memory hog keeps on building due to not so aggressive garbage collection, but it can be instructed to use less [^1].

[^1]: https://community.jitsi.org/t/cpu-and-ram-usage-constantly-increases-on-an-idle-jitsi-server/78210/4

For jvb, add the following line in `/etc/jitsi/videobridge/config`
```bash
VIDEOBRIDGE_MAX_MEMORY=320m
```
For jicofo, add the following line in `/etc/jitsi/jicofo/config`
```bash
if [ -z "$JICOFO_MAX_MEMORY" ]; then JICOFO_MAX_MEMORY=320m; fi
```
Change the memory size as you want and do a [restart](#restart-setup) for applying new changed configs.

I host a personal instance for friends and family on 1 GB EC2, 320 MB each for jvb and jicofo is fine for 5-6 folks with videos on.


### Restart setup 

```bash
sudo /etc/init.d/prosody restart 
sudo /etc/init.d/jicofo restart
sudo /etc/init.d/jitsi-videobridge2 restart
```
or a copy-paste one liner for above:
```bash
sudo /etc/init.d/prosody restart && sudo /etc/init.d/jicofo restart && sudo /etc/init.d/jitsi-videobridge2 restart
```

### Check installation version

```bash
dpkg -l | grep jitsi
```
Returns versions of all the components installed with description.

### Log files

- **jvb** - `/var/log/jitsi/jvb.log`
- **jicofo** - `/var/log/jitsi/jicofo.log`
- **prosody** - `/var/log/prosody/prosody.log` and `/var/log/prosody/prosody.err`
- **fail2ban** - `/var/log/fail2ban.log`
- **unattended upgrades** - `/var/log/unattended-upgrades/unattended-upgrades.log`


### Interface changes

The UI can be customized by making changes in `/usr/share/jitsi-meet/interface_config.js` file.

Following are some handy variables to change:
- `APP_NAME` - change title.
- `DEFAULT_LOCAL_DISPLAY_NAME` - changes default name for self (before name is set, default is "me").
- `DEFAULT_LOGO_URL` - to change away from default Jitsi logo in meeting. Add the logo in `/usr/share/jitsi-meet/images/` directory.  
- `DEFAULT_REMOTE_DISPLAY_NAME` - change default name for everyone else (default is "Fellow Jitster").
- `DEFAULT_WELCOME_PAGE_LOGO_URL` - main page logo.
- `DISABLE_JOIN_LEAVE_NOTIFICATIONS` - set it to true to disable notification pop ups on every join/leave.
- `JITSI_WATERMARK_LINK` - change where should the logo redirect on click, in meeting screen.

Steps to change header (main) text on homepage:
- Add following lines in  `body.html` file (empty on vanilla install) found in /usr/share/jitsi-meet/body.html: 
```html
<link rel="stylesheet" href="css/custom.css">
```

(optional) if wanting to add custom JS, also add following line in `body.html`:
```html
<script src="scripts/custom.js"></script>
```

- Create a `custom.css` file in `/usr/share/jitsi-meet/css/` directory and add the following style:
```css
.welcome .header .header-text-title { 
    display: none;  

}

.welcome .header .header-container:before { 
    content:'FSCI Jitsi Meet'; 
    color: #fff; 
    font-size: 2.5rem; 
    font-weight: 500; 
    line-height: 1.18; 
    text-align: center
}
```
(optional) for custom script, create a file `custom.js` in `/usr/share/jitsi-meet/scripts` directory and add JS into it.

Interface changes don't require a server or jitsi-* reload  to take effect. Every reload fetches the latest version.


### Configuration

Configuration file ie `config.js` is being used as central place for making all changes. Interface file is being deprecated by upstream in coming releases. 

`config.js` can be found as `/etc/jitsi/meet/<DOMAIN>-config.js`. It is publicly viewable by default in vanilla installation at `https://<DOMAIN>/config.js`

Quite a bit of configurations can be changed through it. Following are some handy changes:
- Set `prejoinPageEnabled` to true. It shows an intermediate page before joining, where the user can configure their devices (camera/microphone/background/name).
- Set `startAudioMuted` to maybe `10`. Every participant after the defined value will enter with audio muted automatically.
- Set `startVideoMuted` to maybe `10`. Every participant after the defined value will enter with video off automatically.
- Set `fileRecordingsEnabled` to false to disable showing recording option in menu.
- Set `liveStreamingEnabled` to false to disable showing livestreaming option in menu.
- Add a notice/message to users like maintenance timings etc by adding text to `noticeMessage` variable. Displays as a text on top in meetings.
- For peer to peer connections (two participants) meetings, STUN server is used (through [coturn](https://github.com/coturn/coturn)). By default upstream's public STUN server at `stun:meet-jit-si-turnrelay.jitsi.net:443`, used. To use local (and reduce load for upstream), enable/uncomment your server's STUN server URL in line preceding to Jitsi's STUN server URL in `stunServers` array. To disable upstream's STUN server completely, comment it out.

_Note: Most of the options are commented, so need to be uncommented to enable._








### RTMP stream and record setup

_**Note** - For self-hosted Jitsi Meet instances, additional Jibri setup is required. [meet.jit.si](https://meet.jit.si/) has a Jibri instance running, so that can be used._

This setup allows you to record stream on server (without default Dropbox) as well as stream meeting (without default YouTube). Steps:

- Setup a RTMP server (see [this link](https://obsproject.com/forum/resources/how-to-set-up-your-own-private-rtmp-server-using-nginx.50/) for a basic RTMP server setup). 
- From Jitsi click "Start a live stream".
- Put URL in following format`rtmp://<server-ip-or-domain>/live/STREAM-KEY` in "Live stream key". 
- The same URL would be used for viewing the stream. 


### Glossary 

- **Jitsi Meet** - WebRTC JavaScript application that uses Jitsi Videobridge for video conferences.

- **Prosody** - XMPP server. Jitsi uses XMPP for signalling.
- **Jitsi Conference Focus (jicofo)** - server side focus component that manages media sessions between each participant and the videobridge.
- **Jitsi Videobridge (jvb)** -  video router. 
- **Jitsi Gateway to SIP (Jigasi)** - server-side application that allows regular SIP clients to join Jitsi Meet calls.
- **Jibri** - broadcasting and recording. It works by launching a headless Chrome instance rendered in a virtual framebuffer and capturing and encoding the output with ffmpeg.
- **STUN (Session Traversal Utilites for NAT)** - Standard set of methods for traversal of Network Address Translator (NAT) gateways in applications of realtime voice, video, messaging and other interactive communications. Public IP is discovered via STUN server and type of NAT, usually during session start. Doesn't provide a complete solution.
Originally was "Simple Traversal of User Datagram Protocol (UDP) through Network Address Translators" but updated in [RFC 5389](https://tools.ietf.org/html/rfc5389).
- **TURN (Traversal Using Relay NAT)** - The TURN Server is a VoIP media traffic NAT traversal server and gateway. It can be used as a general-purpose network traffic server and gateway, too. Resource intensive so used as last resort in Jitsi.


### Quick links

- [Self hosting guide for Debian/Ubuntu server](https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-quickstart)
- [Network diagram for setup](https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-manual#network-description)
- [Limit the number of participants](https://jitsi.github.io/handbook/docs/faq#how-to-limit-the-number-of-participants)
- [Enable authentication for new meeting creation](https://jitsi.github.io/handbook/docs/devops-guide/secure-domain)



