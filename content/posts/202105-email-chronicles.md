---
title: "Email Chronicles"
date: 2021-05-27T11:10:59+05:30
tags: ["email", "100DaysToOffload"]
---

A recent incident brought back the thought why I love independent, non Google/Outlook, custom emails. Till last year, I was exploring email providers to move away from Gmail in my quest to minimize usage of Google products. Initially, I looked at ProtonMail but lack of IMAP integration (for thunderbird) on a free account deterred me. At the same time, I was also exploring domain registrars for buying _sahilister.in_. Finally settled on _Gandi.net_ as they provide mailboxes with each domain. Hence, _sahil AT sahilister.in_ came into existence. Slowly, I switched my accounts to this email seeing the high quality and reliability of mailbox.

I now love interacting with people over email more than any other medium. The conversation is slower than instant messaging and phone. The replies are composed and well thought of. Email is simple, federated and works almost everywhere. It should be the medium of all complex discussions (looking at mailing lists).

Four incidences about email stand out in my memory. The first happened a few months back during my internship. My Product Development Manager (PDM) said, "Sahil, send me your gmail id". For context, it was for emailing me an invitation to a company's project on a platform. I thought of saying I don't have one and would probably bust his bubble of Gmail only email world, but I decided against it and simply sent my _@sahilister.in_ address. There was no reply for a while. I assume (for my own fun) that he was perplexed seeing the email address, thinking what sort of email is this and 101 other  related thoughts. Finally, the invite landed, and I intimated it in reply.

The next incidence happened a week back. I received a notification from an agency holding _@nic.in_ address. The email was sent to 87 addresses in To field. Curious as I'm with mail addresses, I opened and saw a sea of Gmail addresses. I could only find three non-Gmail addresses, two _@yahoo.in_ addresses (probably of pre-Andriod era relics) and my address. It was actually sad seeing the prominence of Gmail or rather Google's advertisement, tracking emails.  

Now a happy incidence. The first email for Debian packaging learners in Free Software Camp, 2020 was probably the most diverse email server crowd I have ever seen. There were about ten people in the thread, with email accounts on _@riseup.net_, _@disroot.org_, _@gnu.org.in_ (mailing list), _@debian.org_, _@onenetbeyond.org_, a custom mail server, two custom domain mailboxes from Gandi.net and of course two _@gmail.com_ addresses. I was so happy seeing it that I mentioned the following line in my reply to the thread:

_PS: It looks nice seeing such diversity in mail providers in the "to" and "cc" section. Mail diversity for the win \o/_

The final, recent incident happened two days ago, which also prompted this post. I was working on a collaborative presentation over [meet.fsci.in](https://meet.fsci.in) with Abhay. Abhay got a call from Aashim. He requested Aashim's presentation via email. As he started reciting his long _@gmail.com_ address, Aashim snapped and told him to text it. Then I, who was listening to the whole conversation, said "Let me introduce my shorter email address. Tell Aashim to send the presentation on _x AT sahil.rocks_" and we both started laughing. Abhay knew it was legit email. He told Aashim, "_x AT sahil.rocks_" and Aashim was like "Whattttttt!!!". It was fun. Weird flex but ok ;) It made me realize that it is not normal seeing such weird addresses in wild and made me remember all the above-mentioned instances and gave the idea of jotting them down as email chronicles.
