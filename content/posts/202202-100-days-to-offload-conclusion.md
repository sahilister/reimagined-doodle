---
title: "100DaysToOffload Conclusion"
date: 2022-02-05T10:02:16+05:30
tags: ["100DaysToOffload", "writing"]
---

I took the [#100DaysToOffload](https://100daystooffload.com/) challenge last year, thanks to [Jason](https://janusworx.com/). The whole concept of #100DaysToOffload challenge was to write 100 posts in the span of 365 days. Now counting this post (which is coming out 365+ some days ;)), I managed to write about 54 posts (starting with [this write-up](https://blog.sahilister.in/2021/01/beginners-guide-to-host-static-content-on-a-server/)), a little more than the halfway mark.

Initially, I was hesitant to take it up, saying 50 is a more reasonable number, but Jason's mentioning it's only two posts/week and seeing he did multiple 100s of posts for some years around gave the boost to go for it. It seems that January to July were quite productive months in terms of writings, 40 till then. August came, which saw no writing (probably due to work stuff coming in full swing). After August, it was slower, leading to this conclusion post.

#100DaysToOffload was a good motivator to write, but it later became a burden. In hindsight, I have more clarity and my writing is better than before, but I won't re-attempt it again. Writing is better (and easier) with no commitments attached and posting as and when something is completed. Still, I don't see myself committing less to writing, the blog will keep on pinging :)

_PS: Having # in title and in turn URL seems not easy in Hugo._
