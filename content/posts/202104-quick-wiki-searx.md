---
title: "Quick-wiki: Searx"
date: 2021-04-04T15:50:24+05:30
tags: ["searx", "search", "self-hosting", "100DaysToOffload", "quick-wiki"]
---

_Last update: 04/2021, Searx v1.0.0 from GitHub._

This is a [quick-wiki](https://www.urbandictionary.com/define.php?term=quick-wiki) for quick reference for my occasional [Searx](https://searx.github.io/searx/) installations.

## Table of Content

- [Set as default search engine](#set-as-default-search-engine)
- [Update to new release](#update-to-new-release)
- [Restart](#restart)
- [Nginx configuration](#nginx-configuration)
- [Settings file](#settings-file)
- [Interface and other changes](#interface-and-other-changes)
- [Search engines configuration](#search-engines-configuration) 
- [Features](#features)
- [Glossary](#glossary)
- [Quick links](#quick-links)


### Set as default search engine

- [**Firefox desktop**](https://support.mozilla.org/en-US/kb/add-or-remove-search-engine-firefox#w_add-a-search-engine-from-the-address-bar) **/ Tor -** visit searx instance, right click in address bar, select "Add {Search Engine}". Now under Search page in browser settings, in Default Search Engine section dropdown, choose the searx instance.
- **Firefox mobile / Tor mobile -** `Settings` -> `Search` -> `+ Add search engine` -> `Other`. Now add name and following in the search string: 

```http
https://SEARX-URL/?q=%s
```
Above is a slight variation from vanilla installation specific, to my changes. For vanilla installation use the following string in place:
```http
https://SEARX-URL/searx?q=%s
```
- **Brave -** `Settings` -> `Search engine` (from sidebar) -> `Manage search engine` -> `Add`. Now add name and following in the search string: 

```http
https://SEARX-URL/?q=%s
```
Above is a slight variation from vanilla installation, specific to my changes. For vanilla installation use the following string in place:
```http
https://SEARX-URL/searx?q=%s
```
Optionally add a custom search keyword (shortcut) too like `:s` to make search on said search engine by prepending search term with `:s`.

- For **Google Chrome, Edge or Safari**, see [this](https://support.microsoft.com/en-us/microsoft-edge/change-your-default-search-engine-f863c519-5994-a8ed-6859-00fbc123b782) and reuse the above-mentioned search string.

### Update to new release

Check for new releases and changelogs [on GitHub](https://github.com/searx/searx/releases) (usually 6 months).

Little tweaked version of [official documentation](https://searx.github.io/searx/admin/update-searx.html) customized towards my installation.

```bash
sudo -H -u searx -i
cd searx-src
git pull origin master
./manage.sh update_packages
exit
sudo -H systemctl restart uwsgi
```

### Restart

For restarting uWSGI:
```bash
sudo -H systemctl restart uwsgi
```
For restarting searx
```bash
sudo -H service uwsgi restart searx
```

### Nginx configuration

Add these and let certbot handle the rest (including https redirections). Taken from [documentation](https://searx.github.io/searx/admin/installation-nginx.html#id1).

```nginx
server {        
        server_name instance_name;

        # changed from default /searx to have search on base
        # filtron configs
        location / {
                proxy_pass         http://127.0.0.1:4004/;

                proxy_set_header   Host             $host;
                proxy_set_header   Connection       $http_connection;
                proxy_set_header   X-Real-IP        $remote_addr;
                proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
                proxy_set_header   X-Scheme         $scheme;
                # commented following proxy_set_header to remove 
                # /searx from getting appended in each search URL
                # proxy_set_header   X-Script-Name    /searx;
        }

        location /searx/static/ {
                alias /usr/local/searx/searx-src/searx/static/;
        }

        # morty configs
        location /morty {
                proxy_pass         http://127.0.0.1:3000/;

                proxy_set_header   Host             $host;
                proxy_set_header   Connection       $http_connection;
                proxy_set_header   X-Real-IP        $remote_addr;
                proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
                proxy_set_header   X-Scheme         $scheme;
        }
}
```

### Settings file

Most things are customized via `settings.yml` file. 

The lookup order for `settings.yml` is 
1. Path set in `SEARX_SETTINGS_PATH` environment variable.
2. `/etc/searx/settings.yml`
3. In searx git repo in `searx/settings.yml`

Main is in `/etc/searx/settings.yml` and full is in `/usr/local/searx/searx-src/searx`.

### Interface and other changes

To change favicon and logo, make changes in `searx/searx/static/themes/[theme-used]/img`.

Following are some handy variables to change/customize in `settings.yml`:
- `instance_name`
- `autocomplete` - duckduckgo for now
- `secret_key` - generate via openssl command mentioned below.
- `base_url`
- `image_proxy` - proxing image via server. Done via morty. Keep true.
- Under `result_proxy`, `url` to morty address and key in format `key: !!binary "OPENSSL_key"`

To generate keys:
```bash
openssl rand -base64 33
```

### Search engines configuration

To disable a specific engine, add the following in `settings.yml`: 
```yml
disabled : True
```

TODO - find how to disable certain categories on search page.

### Features

- Search "ip" to display IP.
- Use `!iv`, `!ddg`, `!wp` before search term for directly searching Invidious instances, _Duckduckgo_ or _Wikipedia_. See all supported search terms in Engines menu `searx_URL/preferences` or [here](https://searx.github.io/searx/admin/engines.html).
- Use `!images search-term` to directly search images.

### Glossary

- **Filtron** - A reverse proxy and firewall to filter requests before hitting searx written in golang. Blocks crawlers, bots and brute searches. By defaults listens on localhost:4004 and forwards filtered request to localhost:8888 where searx is running.

- **Morty** - A content sanitizer result (including image) proxy written in golang. Rewrites external requests to make them private.

- **uWSGI** - A python web server that implements [Web Server Gateway Interface (WSGI)](https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface) standard. The uWSGI server is a full-featured HTTP server that is runs searx. However, it's not as performant as nginx at serving static content, so it's pretty common to see nginx sitting in front of a uWSGI server.

### Quick links

- [Official documentation](https://searx.github.io/searx/index.html)
- [Searx Github repositery](https://github.com/searx/searx) 
- [Architecture](https://searx.github.io/searx/admin/architecture.html)

