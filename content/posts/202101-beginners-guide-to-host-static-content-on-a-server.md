---
title: "Beginners Guide to Host Static Content on a Server"
date: 2021-01-22T10:17:21+05:30
tags: ["nginx", "server", "guide", "fscamp", "100DaysToOffload"]
---

Recently, I took a session on hosting static content on a web server for [Free Software Camp 2020](https://camp.fsf.org.in/). This is somewhat a written write-up for that.

### Basic Requirements

1. A server with ssh access.
2. A domain (an IP would also work, but we'll also add TLS certificate to it so recommended).
3. A Debian based system.

### Table of Content

1. [Firing up a virtual private server](#firing-up-a-virtual-private-server)
2. [Adding DNS entry](#adding-dns-entry)
3. [Adding content](#adding-content)
4. [Writing nginx config](#writing-nginx-config)
5. [TLS certificate or making the site HTTPS](#tls-certificate-or-making-the-site-https)


### Firing up a virtual private server

As every provider has a different interface to configure and launch an instance, I'm mentioning the generic steps/requirements:
- Select a operating system for the instance. I'm going with Debian 10 Buster now. Steps would remain almost similar for any Debian based distribution with some minor packages change here and there for other distribution families.
- Select instance, storage and specs type according to will.
- Add security group rules. A security group is a set of firewall rules that control the traffic for your instance. Open/add rules for the following ports for ingress traffic (incoming traffic): 
	- Port 22 (SSH): this port is required for SSHing into the instance for remote access. As an additional measure, some providers also allow limiting access to specific IP or specific IP range also. For security reasons try limiting access to port 22 to your IP only (if you have a static one).
	- Port 80 (HTTP): the usual port for HTTP traffic.
	- Port 443 (HTTPS): for HTTPS traffic.

_Note: Egress traffic (outgoing traffic) is usually open on all ports._
- Launch and run a system update after SSHing into the server/instance.
- Keep the IP address of the instance handy.


### Adding DNS entry

Now some addition in you're DNS records:
- To map a URL/domain name to  an IP, we add an A record (for IPv4 addresses) or an AAAA (for IPv6 addresses). You can read more about A record _[here](https://support.dnsimple.com/articles/a-record/)_ and AAAA _[here](https://support.dnsimple.com/articles/aaaa-record/)_. 
- Add TTL, name (URL/domain name) and the IP address of the instance. TTL (Time-to-live) in DNS records context is a numerical value that tells how long a DNS cache should be used/served before reaching out to  the authoritative DNS server (higher up in DNS server the hierarchy) for a fresh copy of  the record. Let's assume in our case the TTL value we put in is _1800s_, domain name is _[fscamp.sahilister.in](http://fscamp.sahilister.in)_ and IP address is _52.66.87.29_ . See a visual representation of adding A record _[here](https://support.dnsimple.com/articles/manage-a-record/)_ and AAAA _[here](https://support.dnsimple.com/articles/manage-aaaa-record/)_.
- The records will take some time to propagate to the global DNS chain. You can check by typing 
```bash
ping <domain-name>
```
If you see the corresponding IP address in response, it has propagated successfully.


### Adding content

Now we have to add the content we want to serve through the server.
- Make a sub-directory in `/var/www/` directory. Why this particular directory you may ask, so apparently this changes according to needs and reasons but for simplicity’ sake we'll use it. You can read Stack Overflow thread about it _[here](https://unix.stackexchange.com/questions/47436/why-is-the-root-directory-on-a-web-server-put-by-default-in-var-www)_.
Let's assume our folder name is fscamp.
- Add a `index.html` file or other static content you want in the folder.


### Writing nginx config

We're going to use nginx. Nginx is a server software (to handle content requests) that can also be used as [reverse proxy](https://en.wikipedia.org/wiki/Reverse_proxy), [load balancer](https://en.wikipedia.org/wiki/Load_balancing_(computing)) and a bunch of [other stuff](https://www.nginx.com/resources/wiki/).
- To get it, simply do
```bash
sudo apt install nginx
```
- Nginx configs (configurations) usually reside in `/etc/nginx/` directory. We'll focus on `sites-available` and `sites-enabled` only for now. `sites-available` directory hosts our configs and `sites-enabled` usually houses a link in `sites-enabled`, also nginx reads from it.

_Note: `sites-available` and `sites-enabled` are Debian family specific directories. Others use/recommend `conf.d`._ 
- In `sites-available`, create a file, in our case `fscamp` which would house a basic nginx config like this: 
```bash
server {
	# ports nginx should listen to
	listen 80;
	listen [::]:80;

	# name and location of our site content
	root /var/www/fscamp;	

	# defining index file in root
	index index.html;

	# defining server name
	# you can add wwww.fscamp.sahilister.in in addition to point and serve same content
	server_name fscamp.sahilister.in;

	# defining a 404, when content not found
	location / {
		try_files $uri $uri/ =404;
	}
}
```
- Now it's time to link this file to `sites-enabled`. To do so type:
```bash
ln -s /etc/nginx/sites-available/<file> /etc/nginx/sites-enabled/<file>

# In our case, it would look like this
ln -s /etc/nginx/sites-available/fscamp /etc/nginx/sites-enabled/fscamp
```

- Now do a nginx restart:
```bash
sudo systemctl restart nginx
```

- If everything went according to plan, your content would be available on you're domain over HTTP.


### TLS certificate or making the site HTTPS

Now to get a TLS certificate and encrypt the connection between a user and our server, we'll use [Let's Encrypt](https://letsencrypt.org/) and [certbot](https://certbot.eff.org/). It does the heavy lifting, adds additional required nginx configs and get us 90 days TLS certificate (which has to be renewed after that).
- Install the certbot package for nginx by doing:
```bash
sudo apt install python3-certbot-nginx
```
- Run `sudo certbot` and follow along. Certbot will also handle adding required changes to nginx configs.
- Now your sites has a valid TLS certificate and can be viewed over HTTPS.

That's your basic website on your server.
