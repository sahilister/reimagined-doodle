---
title: "23 and Counting"
date: 2022-10-06T00:07:43+05:30
tags: ["life", "reflections", "birthday"]
---

23 is two years older than 21, when you're legally allowed to marry in India and one year older than 22, when I came to the realization that I old now, not old age wise but old in some mental sense to me. The age number tells that I've become an adult, but inside, it doesn't feel like it. I still don't feel like an adult. When I'm at home, mum does my packing, dad still do governmental and other stuff. Frankly, I don't feel like growing now.

As usual, life continued after 22nd birthday. Professionally, I'm still with the same organization and naturally become more comfortable and confident with my job. It has given me the freedom to not think twice on buying or eating out as I have become independent, not accountable to anybody for my spends. 
 
Once again, months and year seem to have intermingled. The year seem to be have flown by. Jotting down the months and trying to remember happening in them; couldn't come up with interesting happening in the year. Either my memory is not that great or things were really mild these past months. 

On my blog, writings have grown to focus more on my life and experiences now. Earlier, I was exploring a lot of technologies and stuff, but recently I find my writings to lean towards my feelings and thoughts. These posts usually start on Notally, my note-taking app on the phone, copied over to Etherpad with random bits and pieces of first thoughts. I usually jot down pointers I want to think/write about and then elaborate on them. 

This year also marked less time dedicated to exploring hosting new applications in server space, though have started hosting my own Mastodon server at [_masto.sahilister.in_](https://masto.sahilister.in) which has me and Ravi as the users. I also took a more active role in maintaining diasp.in with Raju Dev through co-sponsoring it, though don't have any concrete will to keep it running due to low to no participation from the community. Next,  working on pulling up a Free Software mirror hosted at [_mirrors.sahilister.in_](https://mirrors.sahilister.in). At the moment, it houses official mirrors for termux, NomadBSD and OSMC seeing more than 100k requests/day with average network usage of ~45GB/day. Work is in progress to become a Blender and Trisquel mirror, though that has already taken a long time, and I'm not seeing a completion to them in the distant future due to long back and forth with those projects. 

Coming to our operating system of choice - Debian, I missed the opportunity to attend DebConf22 Kosovo. It was a wonderful opportunity to interact with the larger Debian community with whom I have been working, having fun, for the past two years. In person MiniDebConf Palakkad, Kerala is planned for November. That should give ample time to visit Kerala and interact with the folks. On the sideline, preparations for DebConf23 Kochi are also in full swing.

A highlight for this year was contributing to OpenStreetMap (OSM). Finding random usage of OSM data ranging from online bus tracking website to live booking screen in my office has intrigued me. Even big tech players like Snap Maps and Instagram use it in their applications. Now my phone contains five different OSM apps for various navigation and contributing purposes. Going on a new road always leads to bunch local survey and POI addition, note-taking and GPS track recording for editing the map later on laptop. It has helped me be geographically aware of places near me, like knowing about the weird naming of villages near my native place which have different naming on paper and on signboards.  It has also made my conversation with my family more interesting, as I now come up with questions about nearby places, and they have to brainstorm and discuss giving me a suitable answer together. This also leads to many visits to unknown roads as well, which is always exciting. 

Lately, life gave a few setbacks (and they seem to keep coming more these days), which I'm trying to see as challenges and life lessons to be learned. This year, I want to explore the mountains, go on treks, be in nature and feel the silence. I want to be in my own and company more and want to enjoy it too. Also want to remove desire for external validation on my actions as no one has seen the world through my eyes and none desire to do accomplish the same things as I do.

Lastly, I feel blessed and grateful for having my parents, sister and other people in my life who have always been supporting me in navigating life. They have eased many of my transitions and I didn't have to face  many troubles because of them. 


_PS - Read last year's birthday post [here](https://blog.sahilister.in/2021/09/another-year-passed-22/)._ 

_PPS - Team change notified at work, so back to hustle._




