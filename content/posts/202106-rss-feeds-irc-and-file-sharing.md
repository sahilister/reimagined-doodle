---
title: "RSS Feeds, IRC and File Sharing"
date: 2021-06-02T17:45:11+05:30
tags: ["rss", "irc", "files", "file-sharing", "code", "code-sharing", "selfhosting", "100DaysToOffload"]
---

_A quick, note-taking, experience post. The thing that binds all three services is that they're all running on the same server and under various subdomains of my experimental domain, sahil.rocks._

Let's start chronologically with RSS feeds. After a discussion with Sooraj (learner from FS Camp, 2020), we decided upon having a session on FreshRSS installation for RSS feeds. I hadn't tried it myself, so said we'll have that after some days. Frankly speaking, I'm still a toddler in SQL database management (MariaDB in this case) and I knew I would need to figure out DB and let the learners know about the basics as well. Finally, FreshRSS was up and running using the [official documentation](https://freshrss.github.io/FreshRSS/en/admins/01_Index.html) and Miguel Stevens's [Installing FreshRSS on Linux article](https://notflip.be/blog/freshrss-on-linux/). Things went fine except for PHP. It makes me wonder how were folks managing PHP in LAMP stack setups, speaking specifically from a Debian perspective. Do ping, if you are/were doing it nicely. Coming back to FreshRSS, I forwarded Miguel's article to Sooraj because it wasn't much of a work warranting a full session. Personally, FreshRSS is still up and hosted. Not using it much (thunderbird FTW!). Will try to host Miniflux, that looks more of my taste.

Next came Internet Relay Chat (IRC), the de-facto communication platform for Free Software communities. I call myself a [matrix] native, usually bridging into IRC channels. The recent turmoil on Freenode piqued my interest in IRC. Earlier I was looking at ZNC as a bouncer but in a discussion, [Dhanesh](https://dhanesh.sabane.in/) suggested [The Lounge](https://thelounge.chat/). I had [The Lounge setup through Yunohost](https://blog.sahilister.in/2020/09/simple-irc-bouncer-using-the-lounge/) back in September 2020. That installation was triggered by an issue with Element back then. Similarly, an issue with Matrix triggered/quickened the installation of native, The Lounge. The Lounge, in public mode is a web IRC client while in private mode, it also works a persistent IRC bouncer and client. As it's a PWA made in JavaScript, installation was easy (and available via quite a few methods). Created a user and started exploring IRC undergrounds. Earlier, I was aware of just Freenode, Libera Chat and OFTC but researching got me into various interesting channels on TildeIRC (probably the coolest), Rizen and DalNet. I'm lurking under various nicks in about 30+ channels right now. It gives that laid back, chill hacker vibe. IRC world is big, filled with lots of jargon (of which I have to learn quite a lot). Dhanesh mentioned that we can have The Lounge setup on diasp.in if the community agrees. No objections as of now, talks are on, let's see. I'll probably write a full quick-wiki for The Lounge, it does have quite a few configurations to note.

The latest installation was for public file and code sharing through [linx-server](https://github.com/ZizzyDizzyMC/linx-server/). I was using [envs.sh](https://envs.sh/) a lot for quick files and image sharing. I explored the option of self-hosting [The Null Pointer](https://github.com/mia-0/0x0), software powering envs.sh's service, but Python and less control weren't sitting well, so the idea was dropped. Not sure how I landed on linx-server's GitHub repository (and then it's active fork). It's minimal, works on minimum resources and has an active fine-grained purging mechanism. 


![linx-server interface](/img/linx-server.png)
<i>
        <div style="text-align: center">
        linx-server's minimal interface
        </div>
</i>

Decided to get it via docker, my first public facing docker installation. Setting the upload limit to 512 MB with maximum expiry time of a day gave the right balance. Though it hit nginx's default upload limit ie a `413 Request Entity too large` error. This was sorted by increasing value of `client_max_body_size` variable to 512 MB. Uploads are not encrypted on the server, but that's fine in my case. 

I plan to keep using The Lounge (either on my server or on community's server) and linx-server. FreshRSS would be purged to be replaced with Miniflux. Though, the present installation with FreshRSS, The Lounge and linx-server are using only 382 MB of memory with minimal CPU usage. Highly recommend checking out all three services.
