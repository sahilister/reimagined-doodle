---
title: "Why I Write (Blogs)"
date: 2020-10-30T01:14:12+05:30
tags: ["blog", "writing"]
---
Blogs might be my only writings at the moment, so let's focus on why I write blogs, the idea and motivation behind it.

It's been a while I wrote something for the blog. Maybe I have lost the momentum. Earlier I usually had a topic in hand/mind for the next blog post while writing the present one. This was a real effective idea because my mind was always buzzing about the next post I would write. Not anymore. (Just to get it out, writing and making a blog post has always been hard while I sit to do it.) Enough of babbling, let's come to the main subject. I was introduced to the concept of having a personal blog by [dgplug](https://dgplug.org/)'s [Summer Training manual](https://summertraining.readthedocs.io/en/latest/blogging.html) and now I know, it's written by Jason Braganza (_if you're reading this, thanks Jason, you got me started_). You can follow Jason's work  [here](https://janusworx.com/). The blogging section in the manual goes into details on why personal blogs should be made (a highly recommended read). Back then I was exploring the idea of personal domain/website as well as moving away from Gmail. I got a domain and set out creating a minimalistic blog (even prior to the main website, which still is nowhere in sight).
 
Things that motivate me to blog are many. First my blog is my little place on the internet where I share experiences, findings, tutorials, walkthroughs or whatever comes to mind. It essentially is a journal/diary of my life (or parts of it that I'm comfortable sharing on the Internet). I went through [Kushal Das](https://toots.dgplug.org/@kushal)'s [blog](https://kushaldas.in/) [archives](https://kushaldas.in/archive.html) dating back to year 2004 and it served as a written history/journey in their life from then to now. High, lows, excitement, experiences everything could be seen reflected in their blog posts and I loved it. I also read [Pirate Praveen](https://social.masto.host/@praveen)'s ex [blog](https://web.archive.org/web/*/j4v4m4n.in) through Internet Archive Wayback Machine and absolutely loved it. That feeling of coming years later and rereading and reflecting on one's mindset or situation back then excites me.

In addition to that I generally come across and read blog posts describing how to accomplish a certain task. So I imagine maybe sometimes my experience/blog can help someone in someway or provide some value, that would be awesome. Just to add, many a time like my other works, I consult my own posts to a redo a particular problem. It essentially serves as my go-to guide more than I can remember. 

Another motivation to write it out is to get it out of the system. Writing helps me think about a certain subject while jotting it down over an extended period (usually 2-3 days, the usual duration of a post completion). It essentially makes the subject more clear while giving a structure to the idea. This blog gives me control over my content and allows me to experiment/write or do any anything about it. Everything is under my own control rather than depending on a blog provider or social media platform.
 

These are the reasons, motivations and history behind me writing blogs. (Maybe someday this will warrant an update, who knows.)

_PS: Read personal blogs from folks who inspire you. Gives an intimate window in their life and mind._
