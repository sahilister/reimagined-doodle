---
title: "Atleast Not Written by an AI"
date: 2024-07-05T21:49:01+05:30
tags: ["blog", "writing", "ai", "thoughts"]
---

I keep on going back and correcting bootload of grammatical and other errors in my posts here. I somewhat feel embarrassed how such mistakes slip through when I was proofreading. Back then it was all good and suddenly this mistake cropped up in my text, which everyone might have already noticed by now. A thought just stuck around that. Those mistakes signify that the text is written by a real human, and humans makes mistakes. :)

PS - Even LanguageTool (non-premium) couldn't identify those errors.
