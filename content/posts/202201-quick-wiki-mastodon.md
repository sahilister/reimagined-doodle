---
title: "Quick-wiki: Mastodon"
date: 2022-01-28T20:26:45+05:30
tags: ["mastodon", "server", "hosting", "100DaysToOffload"]
---

_Last update: 02/2022, Mastodon v3.4.6 from Docker Hub._

This is a [quick-wiki](https://www.urbandictionary.com/define.php?term=quick-wiki) for [Mastodon](https://joinmastodon.org/), a federated, self-hostable social media software. As the installation was done via docker-compose, it's more oriented towards it and may see fewer customizations.

## Table of Content

- [Updates](#updates)
- [Accounts](#accounts)
- [Glossary](#glossary)
- [To do](#todo)
- [Quick links](#quick-links)


### Updates

- Read changelog and instructions from GitHub [release page](https://github.com/mastodon/mastodon/releases).

- Now pull down the containers and update version numbers in docker-compose file:
```bash
## backup db
docker exec mastodon-db-1 pg_dump -Fc -U postgres postgres > name_of_the_backup.dump
cd mastodon
sed -i 's/v3.4.5/v3.4.6/g' docker-compose.yml
docker-compose pull
docker-compose up -d
``` 

### Accounts

- Create new account via admin CLI:
```bash
docker exec -it mastodon-web-1 sh
tootctl accounts create <USERNAME> --email=<EMAIL>
# a password would be generated for login
```

- Create new account via web UI by visiting <URL>/admin/invites

_Note: Accounts need manual approval (confirm CTA) via <MASTODON_URL>/admin/accounts as email setup isn't present._

- Delete an account via admin CLI:
```bash
docker exec -it mastodon-web-1 sh
tootctl accounts delete <USERNAME>
```
- Direct account deletion isn't possible via web UI. The account is first suspended and then data is deleted.

#### Database backup

```bash
docker exec mastodon-db-1 pg_dump -Fc -U postgres postgres > db.dump
```

#### Regain storage

- Steps to delete media older than seven days:
```bash
docker exec -it mastodon-web-1 sh
tootctl media remove
tootctl media remove-orphans
```

### Glossary

- **Web (through Puma)** - Short lived HTTP requests.
- **Streaming API** - Streaming API handles long-lived HTTP and WebSockets connections for real time updates.
- **Background processing (through Sidekiq)** - Many tasks are delegated to background processes for quick HTTP requests. Sidekiq handles these. Sidekiq has the following queues - default (local users tasks), push (push content to remote servers), mailers, pull (fetching content from remote servers) and scheduler (cron jobs like log cleanups and refreshing trending hashtags) - in decreasing order of preference.

### TODO

- Instance emoji's for tusky client.
- Secure mode.
- Federation with hidden services.
- Full text search.
- Video playback in tusky. Latency would be the culprit here.

### Quick links

- [Release page](https://github.com/mastodon/mastodon/releases)
- [Docker Hub release page](https://hub.docker.com/r/tootsuite/mastodon/tags)
- [Mastodon Documentation](https://docs.joinmastodon.org/)
- [Linode's installation guide](https://www.linode.com/docs/guides/install-mastodon-on-debian-10/)
- [Admin CLI documentation](https://docs.joinmastodon.org/admin/tootctl/)
