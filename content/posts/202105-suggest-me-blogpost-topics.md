---
title: "Suggest Me Blogpost Topics"
date: 2021-05-15T23:59:52+05:30
tags: ["blog", "post", "suggestion", "100DaysToOffload"]
---
During the past few months I have written quite a few blog posts. The blog home page is serving as a timekeeper now, and it makes me wonder seeing how much time passed since a post which I remember writing just recently. For me random thoughts become the topic for a new writing adventure and later full-fledged posts. There's no specific method for new topics which sometimes leaves me without inspirations to write on for days. Nowadays, some ideas are becoming too monotonous, and it's not fun penning them down. That's why  I'm writing to ask for suggestions. If you have intentionally, unintentionally or forcefully came across this post, feel free to suggest thoughts and ideas for new posts. I know I won't be able to do justice or write on all suggestions but would keep those ideas (maybe as an update to this post) to revisit them as I grow confident enough to write on them. If you're a fellow writing enthusiast, can you also tell what're your source of inspirations?

You can share your thoughts via email on _sahil AT sahilister.in_ .
Suggesting topics will guarantee that we be good buddies, promise :-]


_Update (22/05/2021):_ [_Ohio_](https://ohio.araw.xyz/) _emailed and suggested some interesting topics. Do check out [_his website_](https://ohio.araw.xyz/)_, _it's beautiful. Topics he suggested were:_

*   _what are your thoughts on cryptocurrency? are you in favor or not in favor?_
*   _Suppose you have a foreigner friend you haven't met in person. Would you invite them in your country? Why or why not? If you would invite them, what will be your primer for them during their stay in your country?_
*   _What text editor/s you use? Write about your current setup (or config) for each._
