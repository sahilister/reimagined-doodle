---
title: "How I Write Blogs - June 2024 Edition"
date: 2024-06-23T12:32:28+05:30
tags: ["blog", "writing"]
---

I wrote about [my blog writing methodology](https://blog.sahilister.in/2021/04/how-i-write-blogs-april-2021-edition/) back April 2021. My writing method has undergone a significant shift now, so thought about writing this update.

New blog topics are added to my note-taking app quite frequently now. Occasionally going through the list, I merge topics, change order to prioritize certain topics or purely drop ideas which seems not worth a write-up. Due to this, I have the liberty to work on blogs according to mood. Writing [the last one](https://blog.sahilister.in/2024/06/first-iteration-of-my-free-software-mirror/) was tiring, so I chose to work on an easy one, i.e. this blog now.

Topic decided, everything starts on etherpad now. Etherpad has this nice font and sync feature, which helps me write from any device of choice. Actual writing usually happens in the morning, right after I wake up. For most topics, I quickly jot down pointers and keep on expanding them over the course of multiple days at a leisurely pace. Though, sometime it adds too many pieces in the puzzle and takes additional time to put everything in flow. New pointer addition keeps on happening along with writing. Nowadays, pictures too dot my blog, which I rarely use to do earlier. I have come to believe on less usage of external links. These breaks the flow of readers. If someone is sufficiently motivated to learn more about something, finding useful sources isn't.

As the first draft comes into being, I run it through [LanguageTool](https://languagetool.org/) for spelling corrections (which typically are many) and fixing grammatical issues. Post that, for the first time I read the complete write-up in one go for formation of coherent storyline, moving paragraphs around for form a structure , adding explainers wherever something new or unexplained is introduced, removing elaborate sentences, making amends wherever required and moving paragraphs around for forming structure. Another round of LanguageTool follows. All set now, I try to space out my final read before publishing, which helps find additional mistakes or loopholes.

When everything is set, I do `hugo`  to generate the site and `rsync` everything to the web server. A final git sync closes the publication part.

After a day or two, I come back to read the blog on the website. This entails another round finding and fixing trivial mistakes. After this, it's set for good.

Nowadays, in addition to being on my blog, everything is syndicated on Planet FSCI and Planet Debian, which has given it more visibility. As someone who's into infrastructure and Internet as a lot, I do pay attention to logs on my server, but as a disconnected exercise to if the blog is being read or not. More hits on the blog doesn't translate to any gratification for me, at least for writers' point of view. Occasionally, people do mention my blog, which does flatter me. Four years and nearly a hundred posts later, I still wonder how I kept on writing for this long.
