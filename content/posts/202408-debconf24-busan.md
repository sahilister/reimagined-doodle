---
title: "Debconf24 Busan"
date: 2024-08-30T21:53:33+05:30
tags: ["debconf", "dc24", "life", "debian", "busan", "south-korea"]
---

DebConf24 was held in Busan, South Korea, from July 28th to August 4th 2024 and preceded by DebCamp from July 21st to July 27th. This was my second IRL DebConf (DC) and fourth one in total. I started in Debian with a DebConf, so its always an occasion when one happens.

This year again, I worked in fundraising team, working to raise funds from International sponsors. We did manage to raise good enough funding, albeit less than budgeted. Though, the local Korean team was able to connect and gather many Governmental sponsors, which was quite surprising for me.

I wasn't seriously considering attending DebConf until I discussed this with Nilesh. More or less, his efforts helped push me through the whole process. Thanks, Nilesh, for this. In March, I got my passport and started preparing documents for South Korean visa. It did require quite a lot of paper work but seeing South Korea's fresh passport visa rejection rate, I had doubts about visa acceptance. The visa finally got approved, which could be attributed to great documentation and help from DebConf visa team. This was also my first trip outside India, and this being to DebConf made many things easier. Most stuff were documented on DebConf website and wiki. Queries got immediate responses in the DebConf channels.

We took a direct flight from Delhi, reaching Seoul in the morning. With good directions from Sourab TK who had reached Seoul a few hours earlier, we quickly got Korean Won, local SIM and T Money card (transportation card) and headed towards Seoul by AREX i.e. airport metro. We spent the next two days exploring Seoul, which is huge. It probably has the highest number of skyscrapers I have ever seen. The city good modern and ancient culture blend. We explored various places in Seoul including Gyeongbokgung Palace, Statue of King Sejong, Bukchon Hanok village, N Seoul Tower and various food markets which were amazing.

<img loading="lazy" alt="A Street in Seoul" src="/img/dc24/dc24-seoul-street.jpg">
<div style="text-align: center">
    <i>A Street in Seoul</i>
</div>

Next, we headed to Busan for DebConf using KTX (Korean high speed rail). _(Fun fact - slogan for City of Busan is "Busan is Good".)_ South Korea has a good network of frequently running high speed trains. We had pre-booked tickets because, despite the frequency, trains were sold out most of the time. KTX ride was quite smooth, despite travelling at 300 Kmph at times through Korean countryside and long mountain tunnels.

<img loading="lazy" alt="View from Dorm Room" src="/img/dc24/dc24-pknu.jpg">
<div style="text-align: center">
    <i>PKNU Entrance</i>
</div>

The venue for DebConf was Pukyong National University (PKNU), Daeyeon Campus. PKNU has two campuses in the Busan and some folks ended up in wrong one too. With good help and guidance from the front desk, we got our dormitory rooms assigned. Dorms here were quite different, ie:
- Rooms had heated floors. Sometimes, it seems to snow in Busan.
- Each area was had card based access (restrictions). There was a separate (prepaid) card for laundry.
- Rooms had announcement systems right inside the room, though we couldn't decipher any announcement as all of them were in Korean.
- Each room was provided with a dedicated access point and own SSID inside the room.

<img loading="lazy" alt="View from Dorm Room" src="/img/dc24/dc24-view-from-dorm.jpg">
<div style="text-align: center">
    <i> View from Dorm Room</i>
</div>

Settling in was easy. We started meeting familiar folks after almost a year. The long conversations started again. Everyone was excited for DebConf.

Like everytime, the first day was full of action (and chaos). Meet and greet, volunteers check in, video team running around and fixing stuff and things working (and sometimes not). There were some interesting talks and sponsors stalls. After day one, things more or less settled down. I again volunteered for video team stuff and helped in camera operations and talk directions, which is always fun. As the tradition applies, saw few talks live on stream too sitting in the dorm room during the conf, which is always fun, when too tired to get ready and go out.

<img loading="lazy" alt="From Talk Director's chair" src="/img/dc24/dc24-directors-view.jpg">
<div style="text-align: center">
    <i>From talk director's chair</i>
</div>

DebConf takes care of food needs for vegan/vegetarianism folks well, of which I'm one. I got to try different food items, which was quite an experience. Tried using chopsticks again which didn't work, which I later figured that handling metal ones were more difficult. We had late night ramens and wooden chopsticks worked perfectly. One of the days, we even went out to an Indian restaurant to have some desi aloo paratha, paneer dishes, samosas and chai (milk tea). I wasn't particularly craving [desi](https://en.wikipedia.org/wiki/Desi) food but wasn't able to get something according to my taste so went there.

<img loading="lazy" alt="As usual Bits from DPL talk was packed" src="/img/dc24/dc24-attendees-for-bits-from-dpl-talk.jpg">
<div style="text-align: center">
    <i> As usual, Bits from DPL talk was packed</i>
</div>

For day trip, I went to Ulsan. San means mountains in Korean. Ulsan is a port city with many industries including Hyundai car factory, petrochemical industry, paint industry, ship building etc. We saw bamboo forest, Ulsan tower (quite a view towards Ulsan port), whale village, Ulsan Onggi Museum and the sea (which was beautiful).

<img loading="lazy" alt="The beautiful sea" src="/img/dc24/dc24-sea.jpg">
<div style="text-align: center">
    <i>The beautiful sea</i>
</div>

<br>

<img loading="lazy" alt="View from Ulsan Bridge Observatory" src="/img/dc24/dc24-view-from-ulsan-bridge-observatory.jpg">
<div style="text-align: center">
    <i> View from Ulsan Bridge Observatory</i>
</div>

Amongst the sponsors, I was most interested in our network sponsors, folks who were National research and education networks (NREN) here. We had two network sponsors, [KOREN](https://www.koren.kr/eng/index.asp) and [KREONET](https://www.kreonet.net/eng/), thanks to efforts by local team. Initially it was discussed that they'll provide 20G uplink each, so 40G in total, which was whopping but by the time the closing talk happened, we got to know we had 200G uplink to the Internet. This was a massive update to last year when we had 1G main and 100M backup link. 200G wasn't what is required, but it was massive capacity and IIRC from the talk, we peaked at around 500M in usage, but it's always fun to have astronomical amount of bandwidth for bragging rights ;)

<img loading="lazy" alt="Various mascots in attendance" src="/img/dc24/dc24-various-mascots-in-attendance.jpg">
<div style="text-align: center">
    <i>Various mascots in attendance</i>
</div>

<br>

<img loading="lazy" alt="Video and Network stats. Screengrab from closing ceremony" src="/img/dc24/dc24-stats.png">
<div style="text-align: center">
    <i>Video and Network stats. Screengrab from closing ceremony</i>
</div>

Now let's talk about things I found interesting about South Korea in general:
- Convenience stores were everywhere, one could see same brand stores less than a kilometer apart. We had even had two of them (GS25(s)), a road cross away too. These places were well stocked with almost everything, even alcohol.
- There were wide footpaths and pedestrian friendly policies.
- Public transport and intra modal transfer is convenient and easy to figure. Each metro station connects to multiple nearby buildings through underground walkways, and one never had to go out in the sun (in hot and humid weather). Also, Seoul and Busan metro networks were massive. The Same T money card worked for buses (almost hop on, tap and hop off at your destination), metros and even cabs.
- South Korea pays special attention to maintaining their historical and cultural buildings. These venues had informational brochures in Korea, English, Japanese and Chinese.
- We got a constant stream of "Public safety alerts" on our phones. Some phones even read them aloud for heatwaves and rains warnings, all in Korean.
- Trash was segregated at source everywhere.
- Public, high speed Wi-Fi was omnipresent in malls, public transport, airport etc. In metro, each coach had access points from all three telecom providers (SK Telecom, KT and LG U+) which also had almost similar voice and data plans.
- Police personals were quite helpful despite the language issue.
- Not many folks here are comfortable in English, but one can always make use of various mobile translation apps.
- Cards are accepted everywhere and there are too many of these cards ;)
- Food situation was a bit difficult for me as a vegetarian. We always have vegan/veg food in DebConf but outside, this whole concept doesn't seem to exist here.
- I couldn't find any public speedtest servers inside Korea. All my fast.com/speedtest.net servers were located either Hong Kong, Singapore, Japan and even in the United States. On the very last day, I got a speedtest servers in Seoul, inside SK Telecom.


<img loading="lazy" alt="Gyeongbokgung Palace Entrance" src="/img/dc24/dc24-gyeongbokgung-entrance.jpg">
<img loading="lazy" alt="Gyeongbokgung Palace Entrance" src="/img/dc24/dc24-gyeongbokgung-2.jpg">
<img loading="lazy" alt="Gyeongbokgung Palace Entrance" src="/img/dc24/dc24-gyeongbokgung-1.jpg">
<div style="text-align: center">
    <i>Grand Gyeongbokgung Palace, Seoul</i>
</div>

<br>
<img loading="lazy" alt="Starfield Library" src="/img/dc24/dc24-starfield-library.jpg">
<div style="text-align: center">
    <i> Starfield Library, Seoul</i>
</div>

If one has to get the whole DebConf experience, it's better to attend DebCamp as well because that's when you can sit and interact with everyone better. As DebConf starts, everyone gets busy in various talks and events and things take a pace. DebConf days literally fly. This year, attending DebConf in person was a different experience. Attending DebConf without any organizational work/stress so was better, and I was able to understand working of different Debian team and workflows better while also identified a few where I would like to join and help. A general conclusion was that almost all Debian teams needs more folks to help out. So if someone want to join, they can probably reach out to the team, and would be able to onboard new folks. Though this would require some patience. Kudos to the Korean team who were able to pull off this event under this tight timeline and thanks for all the hospitality.

<img loading="lazy" alt="DebConf24 Group Photo" src="/img/dc24/dc24-group-photo.jpg">
<div style="text-align: center">
    <i> DebConf24 Group Photo.  <a href="/img/dc24/dc24-group-photo.jpg">Click to enlarge.</a> <br> Credits - <a href="https://aigarius.com/blog/2024/08/02/debconf24-photos/">Aigars Mahinovs</a></i>
</div>

This whole experience expanded my world view. There's so much to see and explore and understand. Looking forward to DebConf25 in Brest, France.

PS - Shoutout to abbyck (aka hamCK)!






