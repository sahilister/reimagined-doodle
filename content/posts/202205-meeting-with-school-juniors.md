---
title: "Meeting With School Juniors"
date: 2022-05-08T18:35:27+05:30
tags: ["life", "school"]
---
Had a chance meeting with school juniors few days back. I was out hanging with Rajan, while we by chance met them. All of them were of the same year, a year junior to us and hanging out together.

I remembered most of them. Apparently they remembered me too. The interaction was easy going and fun. Aarush was amongst them too, one of my favorites from their batch. Felt joy, they were happy faces indeed. It was more heartfelt than even meeting my own classmates, maybe due to the fact I was seeing them after such a long time. Came to the realization that human interactions feel better and can’t be replaced with time spent on books/computer/mobile and what not.

Now I want to plan alumni meet someday where we simply meet and interact how life has been and remember the good old days when deadlines weren’t so scary.
