---
title: "25, A Quarter of a Century Later"
date: 2024-10-16T08:37:58+05:30
tags: ["reflection","birthday","25", "life"]
---

25 the number says well into adulthood. [Aviral](https://weirdlyviral.in/) pointed that I have already passed 33% mark in my life, which does hits different. 

I had to keep reminding myself about my upcoming birthday. It didn't felt like birthday month, week or the day itself.

My writings took a long hiatus starting this past year. The first post came out in May and quite a few people asked about the break. Hiatus had its own reasons, but restarting became harder each passing day afterward. Preparations for DebConf24 helped push DebConf23 (first post this year) out of the door, after which things were more or less back on track on the writing front. 

Recently, I have picked the habit of reading monthly magazines. When I was a child, I used to fancy seeing all the magazines on stationary and bookshops and thought of getting many when I'm older. Seems like that was the connection, and now I'm heavily into monthly magazines and order many each month (including Hindi ones). They're fun short reads and cover a wide spectrum of topics.

Travelling has become the new found love. I got the opportunity to visit a few new cities like Jaipur, Meerut, Seoul and Busan. My first international travel showed me how a society which cares about the people's overall wellbeing turns out to be. Going in foreign land, expanded the concept of everything for me. It showed the beauty of silence in public places. Also, re-visited Bengaluru, which felt good with its good weather and food.

It has become almost become tradition to attend a few events. Jashn-e-Rekhta, DebConf, New Delhi World Book Fair, IndiaFOSS and FoECon. It's always great talking to new and old folks, sharing and learning about ideas. It's hard for an individual to learn, grow and understand the world in a silo. Like I keep on saying about Free Software projects, it's all about the people, it's always about the people. Good and interesting people keep the project going and growing. (Side Note - it's fine if a project goes. Things are not meant to last a perpetuity. Closing and moving on is fine). Similarly, I have been trying to attend Jaipur Literature Festival since a while but failing. Hopefully, I would this time around.

Expanding my [Free Software](https://www.gnu.org/philosophy/free-sw.en.html) Mirror to India was a big highlight this year. The mirror project now has 3 nodes in India and 1 in Germany, serving almost 3-4 TB of mirror traffic daily. Increasing the number of Software mirrors in India was and still is one of my goals. Hit me up if you want to help or setup one yourself. It's not that hard now actually, projects that require more mirrors and hosting setup has already been figured out. 

One realization I would like to mention was to amplify/support people who're already doing (a better job) at it, rather than reinventing the wheel. A single person might not be able to change the world, but a bunch of people experimenting and trying to make a difference certainly would. 

Writing 25 felt harder than all previous years. It was a traditional year with much internal growth due to experiencing different perspectives and travelling. 

To infinity and beyond!
