---
title: "What's in the Name, Reimagined Doodle, sahilister"
date: 2020-12-27T13:13:23+05:30
tags: ["tell", "reimagined-doodle", "sahilister", "explain"]
---

"Reimagined Doodle", the name of my blog and "sahilister", my internet nick/handle/alias/domain name are two names I'm real proud of. Folks do occasionally ask me how these names came to be (specifically the nick, fewer folks even read my blog let alone notice its name). It isn't an interesting story per se but is nice having it on my blog itself. Maybe someday I would simply point someone directly to the link rather than explaining it myself.

So the nick "sahilister". Before having a specific internet nick, I usually used to have my real name almost everywhere, though I had to use a combination of symbols, numbers with it because almost everytime the name "Sahil" would be taken. There wasn't any consistent pattern and I choose what was available. Then came the trend of having an Instagram profile. I went ahead creating one and when it asked me for a username, I started thinking about some without a number or symbol. In a flash "sahilister" struck from my name "Sahil". This seemed perfect. One word; long, homogenous and different in itself (had real proud moments seeing that nick on my profile). Now that I have left Instagram, the nick has stuck. I have a gamer nick from my mobile gaming era. I don't know from where I got "mukebaaj" and started using that as my gaming alias everywhere. "mukebaaj" being a unique name was almost always available in every game. I still use that. It looks cool and aggressive, typical gamer nick ;) .

Now coming to "Reimagined Doodle". When I got my domain and wanted to start a blog, I didn't simply want to name it as "blog" or something. When I got about creating the GitHub repo for it (initially it was hosted on GitHub pages, GitLab pages now), the repo input field on new creation typically suggests a combination of words for the name. The suggestion I got was, "reimagined doodle". I always like to doodle while I was a schoolkid and loved this word, so without any further thinking went ahead naming the repo as "reimagined doodle" and hence the blog got it name.

_PS: Another long overdue post. Want to have this as the second last post of the year by having a year in review post next. Let's see if I could pull that off before 31st December._
