---
title: "Personal ASNs From India"
date: 2024-07-02T01:15:03+05:30
tags: ["list", "internet", "asn"]
---

_Last Updated on 27/10/2024._

Internet and it's working are interesting and complex. We need an IP address to connect to the Internet. A group of IP addresses with common routing policy is known as an Autonomous System (AS). Each AS has a globally unique Autonomous System Number (ASN) and is maintained by a single entity or individual(s). Your ISP would have an ASN. IP addresses/prefixes are advertised (announced) by an AS through Border Gateway Protocol (BGP) to its peers (ASes which it connects to) to steer traffic in its direction or back.

Take for example Google DNS service at 8.8.8.8 owned and operated by [AS15169](https://bgp.tools/as/15169) Google LLC. AS15169 through BGP announcements, lets all its peers  know that traffic for whole of 8.8.8.0/24 (including 8.8.8.8) prefix should be sent to them. See the following screenshot response of `mtr -zt 8.8.8.8` from my system. From my Internet Service Provider (ISP), [AS133982](https://bgp.tools/as/133982) Excitel Broadband, traffic travels to AS15169 to reach 8.8.8.8 (dns.google) and returns via the same path. This Inter-AS traffic makes the Internet tick.

![mtr from Excitel to Google](/img/mtr-to-google-from-excitel.png)

ASes comes in different sizes and purposes. Like [AS749](https://bgp.tools/as/749) DoD Network Information Center which holds more than 200 million+ IPv4 addresses for historical reasons or [AS23860](https://bgp.tools/as/23860) Alliance Broadband Services which has 68 thousand+ IPv4 address for purpose of providing consumer Internet.

Similarly, some individuals also run their personal ASN including a bunch of Indians. Most of these Indian ASNs are IPv6 (primary or only) networks run for hobby and educational purposes. I was interested in this data, so complied a list of active ones (visible in the global routing table) from [BGP.Tools](https://bgp.tools/):
- [AS149794](https://bgp.tools/as/149794) Daryll Swer
- [AS198383](https://bgp.tools/as/198383) Suhail Haris
- [AS199347](https://bgp.tools/as/199347) Ishan Jain
- [AS199693](https://bgp.tools/as/199693) Bihaan Sen
- [AS200169](https://bgp.tools/as/200169) Manish Pant
- [AS200730](https://bgp.tools/as/200730) Ankit Yadav
- [AS201412](https://bgp.tools/as/201412) Dhruba Dey Networks
- [AS202340](https://bgp.tools/as/202340) Sanoj Kumar
- [AS203145](https://bgp.tools/as/203145) Debdut Biswas
- [AS207941](https://bgp.tools/as/207941) Jiten Pathy
- [AS210965](https://bgp.tools/as/210965) Soundarahari Parthiban
- [AS211242](https://bgp.tools/as/211242) Srijit Banerjee
- [AS213326](https://bgp.tools/as/213326) Kalpak Mukhopadhyay Network Lab
- [AS214543](https://bgp.tools/as/214543) Karthik V
- [AS216452](https://bgp.tools/as/216452) Edvin Basil Samuval

Let me know if I'm missing someone.
