---
title: "Sync Git Repository with Upstream"
date: 2021-04-06T18:32:37+05:30
tags: ["git", "100DaysToOffload"]
---
Following are the steps for syncing a forked git repository with upstream:

- Add remote for upstream repository. Use the `git clone` URL from upstream:
```bash
git remote add upstream <upstream_repositery_link>.git
```
Here we added a remote, naming it upstream (can be varied). Remotes are set of tracked repositories. You can view all the remotes for present git repository by doing `git remote -v`. 

- Next, fetch changes from upstream and merge/sync in local repository: 
```bash
git fetch upstream
git checkout master
git merge upstream/master
```

_Note - Replace master with main if your repository has switched to naming the default branch as main._ 

`git fetch` is used to fetch remote/upstream changes. <br/>
`git checkout` is used to switch between branches. <br/>
`git merge` is used to merge changes from different remotes/branches.
 
 - Now, push these changes to your hosted git repository (origin) on _GitLab/GitHub/SourceHut/Codeberg_ which will also sync it with upstream:
 ```bash
 git push
 ```
 
 - (optional) To sync all the changes from master/main into feature branch:
 ```bash
 git checkout <branch_name>
 git merge master
 ```
 
 _PS: Work via branches to avoid merge conflicts. New branches can be created via `git checkout -b <branch_name>`. Use `git checkout master` to switch back to master branch._
