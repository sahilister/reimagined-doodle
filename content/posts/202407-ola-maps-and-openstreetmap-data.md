---
title: "Ola Maps and OpenStreetMap Data"
date: 2024-07-29T13:49:25+09:00
tags: ["openstreetmap", "osm", "ola"]
---

Recently, Ola started rolling out Ola Maps in their main mobile app, replacing Google Maps, while also offering maps as a service to other organizations. The interesting part for me was the usage of OpenStreetMap data as base map with Ola's proprietary data sources. I'll mostly about talk about map data part here.

![RTI 1](/img/ola-osm.jpg)
<div style="text-align: center">
    <i>
        Screenshot of Ola App. <br> OpenStreetMap attribution is shown after clicking the Ola Map icon.
    </i>
</div>

OpenStreetMap (OSM) for starters, is a community owned and edited map data resource which gives freedom to use map data for any purpose. This includes the condition that attribution is given back to OSM which in turn ideally would encourage other users to contribute, correct and edit, helping everyone in turn. Due to this, OSM is also regarded as Wikipedia of maps. OSM data is not just used by Ola. [Many others](https://welcome.openstreetmap.org/about-osm-community/consumers/) use it for various purposes like Wikipedia Maps, Strava Maps, Snapchat Map, bus tracking in GoIbibo/Redbus.

OSM India community has been following Ola map endeavor to use and contribute to OSM since they went public. As required by OSM for organized mapping efforts, Ola created [wiki entry](https://wiki.openstreetmap.org/wiki/Ola_-_Organised_Editing) with information regarding their editors, usage, policy and mentions following as their data usage case:
> OSM data is used for the road network, traffic signs and signals, buildings, natural features, landuse polygons and some POIs.

Creating a map product is a task in itself, an engineering hurdle creating the tech stack for collection, validation, import and serving the map and the map data part. Ola has done a good job describing the development of tech stack in their [blog post](https://tech.olakrutrim.com/navigating-india-the-journey-of-ola-maps/). Ola holds an enormous corpus of live and constantly updated GPS trace data. Their drivers, users, and delivery partners generate those, which they harness to validate, correct and add missing map data. Ola employees now regularly contribute new or missing roads (including adding dual carriageway to existing ones), fix road geometry, classification, road access type and restrictions pan India. They have been active and engaging in OSM India community channels, though community members have [raised some concerns](https://community.openstreetmap.org/t/organized-editing-by-ola-in-india/108148) on their OSM edit practices.

Ola's venture into the map industry isn't something out of the ordinary. [Grab](https://en.wikipedia.org/wiki/Grab_Holdings), a South East Asian company which has business interests in food deliveries, ride hailing and a bunch of other services too switched to their [in-house map](https://www.grab.com/sg/press/tech-product/grab-to-be-fully-powered-by-its-own-mapping-technology-by-q3-2022/) based on OpenStreetMap, followed by launching of their [map product](https://grabmaps.grab.com/). Grab too [contributed back data](https://wiki.openstreetmap.org/wiki/Grab) like Ola. Both Ola and Grab heavily rely on map for their business operations and seem to chose to go independent for it, bootstrapping the products on OSM.

In India too, a bunch of organizations contribute to OSM like [Swiggy](https://wiki.openstreetmap.org/wiki/Swiggy_-_Organised_Editing), [Stackbox](https://wiki.openstreetmap.org/wiki/Organised_Editing/Activities/Stackbox), [Amazon](https://wiki.openstreetmap.org/wiki/Organised_Editing/Activities/Amazon), [Apple](https://wiki.openstreetmap.org/wiki/Organised_Editing/Activities/Apple). [Microsoft](https://wiki.openstreetmap.org/wiki/Organised_Editing/Activities/Microsoft), [Meta/Facebook](https://wiki.openstreetmap.org/wiki/Organised_Editing/Activities/Meta) and [many others](https://wiki.openstreetmap.org/wiki/Organised_Editing/Activities). Everyone wants a better map (data), so everyone works together.

Ola could have gone their own route, bootstrapping map data from scratch, which would have been a gargantuan task when you're competing against the likes of Google Maps and Bing Maps, which have been into this since many years. Deciding to use OSM and actively giving back to make data better for everyone deserves accolades. Now I'm waiting to for their second blog post, which they mention would be on map data.

If you're an Ola map user through Ola Electric or Ola app, and find some road unmapped, you can always edit them in OSM. What I have heard from their employee, they import new OSM data weekly, which means your changes should start reflecting for you (and everyone else) by next week. If you're new, follow [Beginners' guide](https://wiki.openstreetmap.org/wiki/Beginners%27_guide) and join OSM India community [community.osm.be/resources/asia/india/](https://community.osm.be/resources/asia/india/) for any doubts and participating in various mapping events.

PS — You can see live OSM edits in India subcontinent [here](https://osmlab.github.io/show-me-the-way/#bounds=6.7,67.7,36.4,97.4).
