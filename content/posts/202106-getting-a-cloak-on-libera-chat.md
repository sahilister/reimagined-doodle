---
title: "Getting a Cloak on Libera Chat"
date: 2021-06-18T08:34:37+05:30
tags: ["irc", "libera.chat", "cloak", "100DaysToOffload"]
---

Hostnames/IPs are fetched by a simple reverse DNS lookup on joining an IRC network. IRC cloaks or "virtual hosts" are used to hide the displayed IP/hostname or to show affiliation with a certain project.  Cloaks can be as simple _nick@user/nick_ or _nick@wikimedia/nick_ inplace of _nick@123.45.67.89_. 

You can get a cloak from projects you're affiliated with like from [Wikimedia](https://meta.wikimedia.org/wiki/IRC/Cloaks#Obtaining_a_cloak), [Ubuntu](https://wiki.ubuntu.com/IRC/Cloaks) and others. They will provide project specific cloaks depending on level of involvement. If you're not affiliated or don't want one from them on libera.chat, simply join `#libera-cloak` channel and request one. The bots there will automatically provide you _@user/nick_ cloak. Bear in mind, the channel is set to `+r` mode, so you need to register with `NickServ` to join the channel. 
