---
title: "Run a Tor Bridge"
date: 2021-12-19T08:37:53+05:30
tags: ["tor", "privacy", "bridge", "community", "censorship", "100DaysToOffload"]
---

I finally got around setting up a Tor bridge node. It's an easy way to help others and you have a private internet.

Last month, the Tor Project sent out a call for running a Tor bridges on their [socials](https://web.archive.org/web/20211219031519/https://mastodon.social/@torproject/107300752194094294). I had discussions with [Ravi](https://ravidwivedi.in/) about this and concluded that it shouldn't be hard getting a bridge up. The only real requirement is an ample amount of bandwidth (and an updated, secure system). Most European and US based providers provide multiple Tbs of bandwidth with plans already.

Yesterday, finally, I got around setting it up. The step-by-step [documentation](https://community.torproject.org/relay/setup/bridge/debian-ubuntu/) was easy to follow. It took around three hours for the bridge to show up on relay search to confirm things are working as expected. After it, as each bridge is assigned to a [bridge distribution mechanism](https://bridges.torproject.org/info) - HTTPs, Moat, Email or Reserved; which usually takes upto a day, so I had to wait for my bridge to see some action.
Last night, the bridge was assigned to Moat - bridges, which show up for selection in Tor Browser settings. The bridge started getting connections before I woke up today. From the `syslogs`:

```
Dec 19 01:42:36 workspace Tor[583530]: Heartbeat: Tor's uptime is 12:00 hours, with 66 circuits open. I've sent 10.04 GB and received 10.03 GB. I've received 532 connections on IPv4 and 38 on IPv6. I've made 4978 connections with IPv4 and 1378 with IPv6.
```
532+38 connections already, 10 GB both ways, already done! That was a real good sight.

Running a relay is better. If for certain reasons you can't run one and have a server rig running with some bandwidth to spare (you can specify limits as well); spend 20 mins setting a Tor bridge on it. A bridge is an added, hidden layer in Tor traffic which helps censored internet users "bridge" or "enter" the Tor network as most relay and their IPs henceforth are public info and are readily blocked by the censoring party. Bridge acts as hidden proxy to enter the Tor network. The end website don't see your bridge's IP, it's always the exit node's one, so you're safe there as well.

Most of monthly bandwidth is already remains unused, why not use it to a good cause ;)

_PS: If not a bridge, try running a [Snowflake proxy](https://snowflake.torproject.org/). It's a browser extension ([Firefox](https://addons.mozilla.org/en-US/firefox/addon/torproject-snowflake/), [chrome](https://chrome.google.com/webstore/detail/snowflake/mafpmfcccpbjnhfhjnllmmalhifmlcie)), which turns your browser into a proxy that connects Tor users in censored regions to the Tor network. Again, it's the exit node IP showing up in website logs, not yours._

_Update (12/2021): Added another bridge. This one was allocated to HTTPs so seeing less traffic. Moat bridge is proxing more than 25+ GB of data everyday._
