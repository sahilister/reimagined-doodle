---
title: "Sleep"
date: 2021-04-22T00:40:07+05:30
tags: ["life", "sleep", "dreams", "100DaysToOffload"]
---

As we close our eyes, a process of scrubbing reality from the subconscious begins. Slowly the body goes into unconscious state, and we lose touch with the surroundings. We stop engaging in the physical world. We disconnect from reality, our miseries, happiness, material and social gains.

Now we start engaging in dream dimension, a mental world. A play with real and imaginary characters playing our very own mind created storyline. It’s amazing how all this, the scene setting, the characterization, storyline happens without us leaving the room, physically. 

Even if it plays out in the mind, still the body engages in the act, feels the emotions, the actions. This play at times, is cut short by this physical action and the characters leaves their role and reality seeps in. The conscious mind becomes aware of the existence of the dream which was its own creation demonstrating the non-atomic nature of mind. Different sections become active or passive at different times.

Coming back to the play, it’s a respite on some nights or nightmares (literally!) at the other. This leaving the world and re-entering it is something akin to magic.

As we open our eyes, a process of scrubbing the imagination from the subconscious begins, making us re-aware of the surroundings, leaving less to no traces of the intrinsically choreographed play.
