---
title: "TIL - PostSecret"
date: 2021-09-24T16:42:39+05:30
tags: ["TIL", "100DaysToOffload", "life", "secret"]
---

Came across this project called [PostSecret](https://postsecret.com/) on search of interesting websites. PostSecret is a project where people send a postcard, with a secret they wanted to share, anonymously, which get publicly viewable at the website [_PostSecret.com_](https://postsecret.com/).

Here are the instructions from the website:

![Share a secret postcard](/img/postsecret-postcard.webp)
<div style="text-align: center">
    <i>
       Share a secret instructions from <a href="https://postsecret.com/">PostSecret.com</a>. <br>
       Copyright © <a href="https://postsecret.com/">PostSecret.com</a>. Under fair dealing. 
    </i>
</div>

<br>

The experiment which eventually turned into this website show's how interesting an unknown secret can be; it's peaceful at times, sad at others or just too damn funny. Also check out the [Half a million secrets - TED talk](https://www.ted.com/talks/frank_warren_half_a_million_secrets) by Frank Warren, the person behind PostSecret, this is a good one, I must admit.



