---
title: "Experience Volunteering for Debian Release Party India"
date: 2021-10-09T20:40:09+05:30
tags: ["experience", "debian", "debianindia", "release-party", "bullseye", "100DaysToOffload"]
---

_A much delayed experience post of Debian Release Party that happened on 14th August._

Debian has a stable release every two to three years. So whenever a release happens, after the long freeze, it's an occasion for release parties. This release was codenamed bullseye, the 11th Debian stable release and happened on 14th of August 2021.

This time also, it was Anupa who initiated the conversation of having a Debian release party India. Initially, I believe [Abraham Raji](https://abrahamraji.in/) was the only other one who joined in and they started with planning, publicity and events. A virtual release party hosted on BigBlueButton was planned. Anupa pinged me for getting a placeholder, redirect URL for going on publicity material. This piqued my interest as this was the perfect opportunity to implement a nice [redirect hack](https://gitlab.com/emacs-apac/r) used by folks at Emacs APAC. I made the [following redirect](https://gitlab.com/fsci/redirect). Call for events and event posters were send on mailing lists, irc/matrix/xmpp/telegram channels and a bunch of other places. Abraham propsed a talk, "Introduction to Debian bullseye", Anupa a quiz on Debian and I, SuperTuxKart gaming session after the event. I got the anchoring of the event. 

On 14th, 6 pm IST, we started seeing folks join the BBB room. More than 30 folks joined in. I wasn't expecting these many people to turn up, which made me happy and bit nervous. Just before the event, we planned to wear [MDCO India](https://blog.sahilister.in/2021/01/experience-volunteering-for-minidebconf-india-2021/) Ts but Anupa was already in her saree attire. On that date, everyone was minutely following the Debian micro-news for updates pouring in from the release team. By that time, final testing of release candidates were in full swing. We started the event with some quick introductions.  Anupa then started with the Debian quiz which made the mood light and fun, it indeed served as a nice icebreaker. Next was Abraham's talk, which put in light on the technical changes that were landing in bullseye (which was made in IIRC org-mode, looked nice and minimal). Though the actual release was still some hours out, as the release party tradition dictates, we brought cakes next. Sruthi and Anupa made tasty looking, Debian themed cakes (we only virtually saw them eating, but I had dahi gol-gappa chat ready to eat then ;)).

<br>
<div style="text-align: center">

![Image of cake made by Sruthi.](/img/debian-11-release-party-group-photo-cake-2.jpg)

<i> Cake made by Sruthi. </i>

<br>

![Image of cake made by Anupa](/img/debian-11-release-party-group-photo-cake-1.jpg)

<i> Cake made by Anupa. </i>

</div>

<br>

A community interaction time followed. Things around Debian and Debian India were discussed. I then officially thanked everyone for joining and invited folks for the SuperTuxKart session. Most stayed back, but initially we weren't able to join a common game, then someone suggested to join an existing server which worked fantastically, and it started the hour and half long racing on tracks. [Subin](https://subinsb.com/) won the initial races. He was probably the most experienced racer amongst us. A screenshare was done for folks who weren't playing (though lower frame rate probably didn't make the screenshare anything much watchable). All in all we started at 6 pm and I dropped IIRC at 9.30 pm from the gaming party. The gaming party, did turn out as one of the highlights.  

Now, I would like to thank Abraham Raji for the all the nice posters he made (we always bug him for all our events graphic needs :D), Anupa for getting everything together (as always :)) and [Abhas](https://abhas.io/) for the event BBB infra. It was fun little get together, got to meet folks from the community and talk Debian. Let's see when the next event be organized, maybe the next MDCO India even :D

<br>
<div style="text-align: center">

![Group photo of release party, a screenshot of BBB room with bunch of camera's on with people smiling](/img/debian-11-release-party-group-photo.png)

<i>Group photo of release party. <a href="/img/debian-11-release-party-group-photo.png">Click to enlarge</a> </i>
</div
