---
title: "First Iteration of My Free Software Mirror"
date: 2024-06-19T09:05:20+05:30
tags: ["mirror", "linux", "server", "free-software", "opensource"]
---

As I'm gearing towards setting up a [Free Software](https://www.gnu.org/philosophy/free-sw.en.html) download mirror in India, it occurred to me that I haven't chronicled the work and motivation behind setting up the [original mirror](https://mirrors.de.sahilister.net) in the first place. Also, seems like it would be good to document stuff here for observing the progression, as the mirror is going multi-country now. Right now, my existing mirror i.e., [mirrors.de.sahilister.net](https://mirrors.de.sahilister.net) (was [mirrors.sahilister.in](https://mirrors.sahilister.in)), is hosted in Germany and serves traffic for Termux, NomadBSD, Blender, BlendOS and GIMP. For a while in between, it hosted OSMC project mirror as well.

To explain what is a Free Software download mirror thing is first, I'll quote myself from [work blog](https://blog.hopbox.net/sahil/hopbox-public-gnu-software-mirror) -
> As most Free Software doesn’t have commercial backing and require heavy downloads, the concept of software download mirrors helps take the traffic load off of the primary server, leading to geographical redundancy, higher availability and faster download in general.

So whenever someone wants to download a particular (mirrored) software and click download, upstream redirects the download to one of the mirror server which is geographical (or in other parameters) nearby to the user, leading to faster downloads and load sharing amongst all mirrors.

Since the time I got into Linux and servers, I always wanted to help the community somehow, and mirroring seemed to be the most obvious thing. India seems to be a country which has traditionally seen less number of public download mirrors. IITB, TiFR, and some of the public institutions used to host them for popular Linux and Free Softwares, but they seem to be diminishing these days.

In the last months of 2021, I started using Termux and saw that it had only a few mirrors (back then). I tried getting a high capacity, high bandwidth node in budget but it was hard in India in 2021-22. So after much deliberation, I decided to go where it's available and chose a German hosting provider with the thought of adding India node when conditions are favorable (thankfully that happened, and [India node is live](https://mirrors.in.sahilister.net) too now.). Termux required only 29 GB of storage, so went ahead and started mirroring it. I raised [this issue](https://web.archive.org/web/20240609183755/https://github.com/termux/termux-packages/issues/8535) in Termux's GitHub repository in January 2022. [This blog post](https://blog.sahilister.in/2022/01/termux.sahilister.in-a-termux-mirror-is-live-now/) chronicles the start of the mirror.

Termux has high request counts from a mirror point of view. Each Termux client, usually checks every mirror in selected group for availability before randomly selecting one for download (only other case is when client has explicitly selected a single mirror using `termux-repo-change`). The mirror started getting thousands of requests daily due to this but only a small percentage would actually get my mirror in selection, so download traffic was lower. Similar thing happened with OSMC too (which I started mirroring later).

With this start, I started exploring various project that would be benefit from additional mirrors. Public information from Academic Computer Club in Umeå's [mirror](https://mirror.accum.se/mirror/) and Freedif's [mirror stats](https://mirror.freedif.org/Stats/) helped to figure out storage and bandwidth requirements for potential projects. Fun fact, Academic Computer Club in Umeå (which is one of the prominent Debian, Ubuntu etc.) mirror, now has [200 Gbits/s uplink](https://mirror.accum.se/about/index.html) to the internet through SUNET.

Later, I migrated to a different provider for better speeds and added LibreSpeed test on the mirror server. Those were fun times. Between OSMC, Termux and LibreSpeed, I was getting almost 1.2 millions hits/day on the server at its peak, crossing for the first time a TB/day traffic number.

Next came Blender, which took the longest time to set up of around 9–10 months. Blender had a push-trigger requirement for rsync from upstream that took quite some back and forth. It now contributes the most amount of traffic on the mirror. On release days, mirror does more than 3 TB/day and normal days, it hovers around 2 TB/day. Gimp project is the latest addition.

At one time, the mirror traffic touched 4.97 TB/day traffic number. That's when I decided on dropping LibreSpeed server to solely focus on mirroring for now, keeping the bandwidth allotment for serving downloads only.

The mirror projects selection grew organically. I used to reach out many projects discussing the need of for additional mirrors. Some projects outright denied mirroring request as Germany already has a good academic mirrors boosting 20-25 Gbits/s speeds from FTP era, which seems fair. Finding the niche was essential to only add softwares, which would truly benefit from additional capacity. There were months when nothing much would happen with the mirror, rsync would continue to update the mirror while nginx would keep on serving the traffic. Nowadays, the mirror pushes around 70 TB/month. I occasionally check logs, vnstat, add new security stuff here and there and pay the bills. It now saturates the Gigabit link sometimes and goes beyond that, peaking around 1.42 Gbits/s (the hosting provider seems to be upping their game). The plan is to upgrade the link to better speeds.

![vnstat yearly](/img/1fs/fs1-vnstat-yearly.png)
<div style="text-align: center">
<i> Yearly traffic stats (through `vnstat -y`) </i>
</div>

On the way, learned quite a few things like -
- IPv6 and exposing rsync module due to OSMC requirement.
- Implementing user with restricted access to only trigger rsync, basically make rsync pull trigger based due to Blender.
- Iterating over right client response size for LibreSpeed test project.
- Mistakenly identifying torrent traffic for BlendOS as DDoS and blocking it for quite a few months. BlendOS added loads of hits for torrent traffic, making my mirror also serve as web seed. Web seeds in conjunction with normal seeds surely is a good combination for serving downloads as it combines best of both world, general availability of web seed/mirror and benefit of traditional seeds to maximize download speeds at user end.
- Handling abusive traffic (a lot of it to be frank). The approach is more of a whack a mole right now, which I want to improve and automate.
- Most of the traffic on non-Linux/BSD operating systems serving mirrors (like mine) is for people on Windows and Mac asking for EXEs and DMGs. Mostly because package repositories carry software distribution load for Linux/BSD OSs and partly because the number of Windows/Mac users are quite high compared to other OSs.
- Load balancing through DNS and HTTP redirection  (which I would implement in my India mirror now) to better maximize available resources.


![GeoIP Map of Clients from Yesterday Access Logs](/img/1fs/geoip-map-through-ipinfo.png)

<div style="text-align: center">
    <i>
        GeoIP Map of Clients from Yesterday's Access Logs. <a href="/img/1fs/geoip-map-through-ipinfo.png">Click to enlarge</a> <br>
        Generated from <a href="https://ipinfo.io/tools/map">IPinfo.io</a>
    </i>
</div>

In hindsight, the statistics look amazing, hundreds of TBs of traffic served from the mirror, month after month. That does show that there's still an appetite for public mirrors in time of commercially “donated” CDNs and GitHub. The world could have done with one less mirror, but it saved some time, lessened the burden for others, while providing redundancy and traffic localization with one additional mirror. And it's fun for someone like me who's into infrastructure that powers the Internet. Now, I'll try focusing and expanding the [India mirror](https://mirrors.in.sahilister.net), which in itself started pushing almost half a TB/day. Long live Free Software and public download mirrors.




