---
title: "Free Software Mirrors in India"
date: 2024-10-21T23:59:00+05:30
tags: ["mirrors", "India", "list", "free-software"]
---

_Last Updated on 03/03/2025._

List of public mirrors in India. Location discovered basis personal knowledge, traces or GeoIP. Mirrors which aren't accessible outside their own ASN are excluded.

# North India
- Bharat Datacenter - [mirror.bharatdatacenter.com](https://mirror.bharatdatacenter.com) ([AS151704](https://bgp.tools/as/151704))
- CSE Department, IIT Kanpur - [mirror.cse.iitk.ac.in ](http://mirror.cse.iitk.ac.in) ([AS55479](https://bgp.tools/as/55479))
- Cyfuture  - [cyfuture.dl.sourceforge.net](https://cyfuture.dl.sourceforge.net) ([AS55470](https://bgp.tools/as/55470))
- Extreme IX - [repo.extreme-ix.org](https://repo.extreme-ix.org) | [repos.del.extreme-ix.org](https://repos.del.extreme-ix.org) ([AS135814](https://bgp.tools/as/135814))
- Hopbox - [mirrors.hopbox.net](https://mirrors.hopbox.net) ([AS10029](https://bgp.tools/as/10029) | [Statistics](https://mirrors.hopbox.net/goaccess.html))
- IIT Delhi - [mirrors.iitd.ac.in](https://mirrors.iitd.ac.in) ([AS132780](https://bgp.tools/as/132780))
- Institute of Engineering and Technology Lucknow - [mirror.ietlucknow.ac.in](https://mirror.ietlucknow.ac.in) ([AS55824](https://bgp.tools/as/55824))
- Naman Garg - [in-mirror.garudalinux.org](https://in-mirror.garudalinux.org) ([AS133661](https://bgp.tools/as/133661))
- NKN - [debianmirror.nkn.in](http://debianmirror.nkn.in) ([AS4758](https://bgp.tools/as/4758))
- Nxtgen - [mirrors.nxtgen.com](https://mirrors.nxtgen.com) ([AS132717](https://bgp.tools/as/132717))
- Saswata Sarkar - [mirrors.saswata.cc](https://mirrors.saswata.cc) ([AS132453](https://bgp.tools/as/132453) | [Statistics](https://mirrors.saswata.cc/stats/))
- Shrirang Kahale - [mirror.del.albony.in](https://mirror.del.albony.in) ([AS135814](https://bgp.tools/as/135814) | [Statistics](https://mirror.del.albony.in/stats))
- Shiv Nadar Institution of Eminence - [ubuntu-mirror.snu.edu.in](https://ubuntu-mirror.snu.edu.in) ([AS132785](https://bgp.tools/as/132785))

# East India
- NISER Bhubaneshwar - [mirror.niser.ac.in](https://mirror.niser.ac.in) ([AS141288](https://bgp.tools/as/141288))
- Shrirang Kahale - [mirror.ajl.albony.in](https://mirror.ajl.albony.in) ([AS141253](https://bgp.tools/as/141253) | [Statistics](https://mirror.ajl.albony.in/stats))

# South India
- Cogan Ng - [in.mirror.coganng.com](https://in.mirror.coganng.com) ([AS31898](https://bgp.tools/as/31898))
- CUSAT - [foss.cusat.ac.in/mirror](https://foss.cusat.ac.in/mirror) ([AS55824](https://bgp.tools/as/55824))
- Excell Media - [centos-stream.excellmedia.net](https://centos-stream.excellmedia.net) |  [excellmedia.dl.sourceforge.net](https://excellmedia.dl.sourceforge.net) ([AS17754](https://bgp.tools/as/17754))
- IIT Madras - [ftp.iitm.ac.in](https://ftp.iitm.ac.in) ([AS141340](https://bgp.tools/as/141340))
- Niranjan Fartare - [in.arch.niranjan.co](https://in.arch.niranjan.co/) ([AS141340](https://bgp.tools/as/140543) | [Statistics](https://in.arch.niranjan.co/stats))
- NIT Calicut - [mirror.nitc.ac.in](https://mirror.nitc.ac.in) ([AS55824](https://bgp.tools/as/55824) | [Statistics](https://mirror.nitc.ac.in/statistics.html))
- NKN - [mirrors-1.nkn.in](https://mirrors-1.nkn.in) ([AS148003](https://bgp.tools/as/148003))
- Planet Unix - [mirror.planetunix.net](https://mirror.planetunix.net) | [ariel.in.ext.planetunix.net](https://ariel.in.ext.planetunix.net) ([AS14061](https://bgp.tools/as/14061))
- Shrirang Kahale - [mirror.hyd.albony.in](https://mirror.hyd.albony.in) ([AS24560](https://bgp.tools/as/24560) | [Statistics](https://mirror.hyd.albony.in/stats))

# West India
- Abhijith PA - [mirrors.abhijithpa.in](https://mirrors.abhijithpa.in/) ([AS141995](https://bgp.tools/as/141995) | [Statistics](https://mirrors.abhijithpa.in/goaccess.html))
- Abhinav Krishna C K - [mirrors.abhy.me](https://mirrors.abhy.me) ([AS31898](https://bgp.tools/as/31898) | [Statistics](https://mirrors.abhy.me/stats/)) 
- Arun Mathai - [mirrors.arunmathaisk.in](https://mirrors.arunmathaisk.in) ([AS141995](https://bgp.tools/as/141995) | [Statistics](https://mirrors.arunmathaisk.in/goaccess.html))
- Avinash Duduskar - [mirror.4v1.in](https://mirror.4v1.in) ([AS24560](https://bgp.tools/as/24560))
- Balvinder Singh Rawat - [mirror.ubuntu.bsr.one](https://mirror.ubuntu.bsr.one) ([AS31898](https://bgp.tools/as/31898))
- ICTS - [cran.icts.res.in](https://cran.icts.res.in) ([AS134322](https://bgp.tools/as/134322)) 
- Navaneeth - [mirror.fossindia.ovh](https://mirror.fossindia.ovh/) ([AS141995](https://bgp.tools/as/141995))
- Nilesh Patra - [mirrors.nileshpatra.info](https://mirrors.nileshpatra.info) ([AS31898](https://bgp.tools/as/31898) | [Statistics](https://mirrors.nileshpatra.info/goaccess.html))
- PicoNets-WebWerks - [mirrors.piconets.webwerks.in](https://mirrors.piconets.webwerks.in) | [webwerks.dl.sourceforge.net](https://webwerks.dl.sourceforge.net/) ([AS133296](https://bgp.tools/as/133296))
- Ravi Dwivedi - [mirrors.ravidwivedi.in](https://mirrors.ravidwivedi.in) ([AS141995](https://bgp.tools/as/141995) | [Statistics](https://mirrors.ravidwivedi.in/goaccess.html))
- Sahil Dhiman - [mirrors.in.sahilister.net](https://mirrors.in.sahilister.net) ([AS141995](https://bgp.tools/as/141995) | [Statistics](https://mirrors.in.sahilister.net/stats)), [2.mirrors.in.sahilister.net](https://2.mirrors.in.sahilister.net) ([AS141995](https://bgp.tools/as/141995) | [Statistics](https://2.mirrors.in.sahilister.net/stats))
- Shrirang Kahale - [mirror.bom.albony.in](https://mirror.bom.albony.in) ([AS24560](https://bgp.tools/as/24560) | [Statistics](https://mirror.bom.albony.in/stats)),  [mirror.bom2.albony.in](https://mirror.bom2.albony.in) ([AS135814](https://bgp.tools/as/135814) | [Statistics](https://mirror.bom2.albony.in/stats)), [mirror.nag.albony.in](https://mirror.nag.albony.in) ([AS24560](https://bgp.tools/as/24560) | [Statistics](https://mirror.nag.albony.in/stats))
- Starburst Services - [almalinux.in.ssimn.org](https://almalinux.in.ssimn.org) | [elrepo.in.ssimn.org](https://elrepo.in.ssimn.org) | [epel.in.ssimn.org](https://epel.in.ssimn.org) | [mariadb.in.ssimn.org](https://mariadb.in.ssimn.org) ([AS141995](https://bgp.tools/as/141995))
- Utkarsh Gupta - [mirrors.utkarsh2102.org](https://mirrors.utkarsh2102.org) ([AS31898](https://bgp.tools/as/31898) | [Statistics](https://mirrors.utkarsh2102.org/goaccess.html))

# CDN (or behind one)
- Alibaba Cloud - [mirrors.aliyun.com](https://mirrors.aliyun.com) ([AS21859](https://bgp.tools/as/21859))
- AlmaLinux OS Foundation - [blr.aws.repo.almalinux.org](https://blr.aws.repo.almalinux.org/) ([AS16509](https://bgp.tools/as/16509))
- Amazon Cloudfront - [cdn-aws.deb.debian.org](https://cdn-aws.deb.debian.org) ([AS16509](https://bgp.tools/as/16509))
- Cicku - [in.mirrors.cicku.me](https://in.mirrors.cicku.me) ([AS13335](https://bgp.tools/as/13335))
- CIQ - [rocky-linux-asia-south1.production.gcp.mirrors.ctrliq.cloud](https://rocky-linux-asia-south1.production.gcp.mirrors.ctrliq.cloud) | [rocky-linux-asia-south2.production.gcp.mirrors.ctrliq.cloud](https://rocky-linux-asia-south2.production.gcp.mirrors.ctrliq.cloud) _(GeoIP doubtful. Could be behind a CDN or single node)_ ([AS396982](https://bgp.tools/as/396982)) 
- Cloudflare - [cloudflare.cdn.openbsd.org](https://cloudflare.cdn.openbsd.org/pub/OpenBSD/) | [kali.download](https://kali.download) ([AS13335](https://bgp.tools/as/13335))
- Edgecast - [mirror.edgecast.com](https://mirror.edgecast.com) ([AS15133](https://bgp.tools/as/15133))
- Fastly - [cdn.openbsd.org](https://cdn.openbsd.org/pub/OpenBSD/) | [deb.debian.org](https://deb.debian.org) | [dlcdn.apache.org](https://dlcdn.apache.org/) | [dl-cdn.alpinelinux.org](https://dl-cdn.alpinelinux.org) | [fastly.linuxmint.io](https://fastly.linuxmint.io) | [images-cdn.endlessm.com](https://images-cdn.endlessm.com/) _(sponsored?)_ | [repo-fastly.voidlinux.org](https://repo-fastly.voidlinux.org/) ([AS54113](https://bgp.tools/as/54113))
- Niranjan Fartare - [termux.niranjan.co](https://termux.niranjan.co) ([AS13335](https://bgp.tools/as/13335))
- Sahil Kokamkar - [mirror.sahil.world](https://mirror.sahil.world) ([AS13335](https://bgp.tools/as/13335))
- Tencent - [mirrors.cloud.tencent.com](https://mirrors.cloud.tencent.com/) ([AS21859](https://bgp.tools/as/21859))

Many thanks to [Shrirang](https://shrirangkahale.com/) and [Saswata](https://c.im/@saswatasarkar13) for tips and corrections. Let me know if I'm missing someone or something is amiss.
