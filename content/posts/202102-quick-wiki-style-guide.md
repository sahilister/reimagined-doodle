---
title: "Quick-wiki: Style Guide"
date: 2021-02-17T16:10:48+05:30
tags: ["quick-wiki", "style-guide", "100DaysToOffload"]
---

Faced some confusion about styling so wrote a good practices style guide for my usage. Much inspiration was taken from the University of Oxford [style guide](https://www.ox.ac.uk/sites/files/oxford/media_wysiwyg/University%20of%20Oxford%20Style%20Guide.pdf) as well as my early day posts.


When in doubt, consult the style guide :D

## Table of Content
 
- [Headings](#headings)
- [Table of Content](#headings) <!-- adding #headings because ToC redirects to ##ToC instead of ###ToC -->
- [Comments](#comments)
- [Code](#code)
- [Quote lines](#quote-lines)
- [Citation](#citation)
- [Images and credits](#images-and-credits)
- [Abbreviations](#abbreviations)
- [Hindi typing](#hindi-typing)
- [Cultural creations, websites and emails](#cultural-creations-websites-and-emails)
- [Numbers, time and dates](#numbers-time-and-dates)
- [Miscellaneous](#miscellaneous)
- [Quick links](#quick-links)

### Headings

Second level (##) heading for ToC or other introductions. Other headings will have third level (###). 

Another level can be done with fourth level (####) heading though it essentially is a word which is bold.


### Table of Content

For sequential steps use numbers in bullets (ordered list), dash for others (unordered list).

Full stops in bullets only for long sentence.


### Comments

Comments can be added to markdown file which won't be displayed on the page. Format:

```html
<!-- ...comments       -->
```

### Code

Code blocks should be done like this (no space in end backticks):
```bash {lineos=table}
 ```<lang-code>
 // ... code block
 ` ``
```

Supported [language codes](https://gohugo.io/content-management/syntax-highlighting/#list-of-chroma-highlighting-languages). Highlight specific line and add line numberings format[^1]:
```bash
 ```lang-code {linenos=table,hl_lines=[8, "11-13"],linenostart=199}
// ... code
//
 ` ``
```
[^1]: [Highlight options](https://gohugo.io/content-management/syntax-highlighting/#highlight-shortcode)

### Quote line

Quoted sentences should be double quoted and in italics. Before starting sentence, it should have a long dash. 

Ex -
I followed up with – “_The next time you hear me say you’re stubborn. Just say – no, I’m just persistent._”[^2] 

[^2]: Text and style addition inspirated from [A Learning a Day's article](https://alearningaday.blog/2021/05/31/stubborn-and-persistent/) .

### Citation

Format for books, website or author citation:
```bash
> add the citation text here.
multi-lines can also be done.

> — <cite>Source[^1]</cite>

[^1]: ["title"](link). _Source_
```

Use [*ZoteroBib*](https://zbib.org/) for generating citations reference information. Remove access date.


### Images and credits

For images, add alt text:
```bash
![Alt text for screen readers](URL)
```
Adding images with lazy loading:
```html
<img loading="lazy" alt="DebConf23 Group Photo" src="/img/dc23/dc23-group-photo-small.jpg">
```

Adding click to enlarge button with cation:
```html
![Alt text](/img/image.jpg)

<div style="text-align: center">
    <i>
        Caption. <a href="/img/image.jpg">Click to enlarge</a>
    </i>
</div>
```

Format for credits:
```html
<div style="text-align: center"> <i>
	 <a href="image_source">image_caption</a> by
	 <a href="creater_link">creator_name</a> under
         <a href="license_link"> license_name </a> </i>
</div>
```

Articles on attribution best practices by [Creative Commons](https://wiki.creativecommons.org/wiki/Best_practices_for_attribution) and [New Media Rights](https://www.newmediarights.org/guide/how_to/creative_commons/best_practices_creative_commons_attributions).


### Abbreviations

No space or full stops after it.

- Time: am, pm
- Short forms: NASA, BBC
- Names: R K Narayan
- etc, ie or eg


### Hindi typing

Presently, I'm using/prefering (origial) [inscript keyboard layout](https://en.wikipedia.org/wiki/InScript_keyboard) for both mobile and laptop typing using ibus and ibus-m17n packages.

![Inscript keyboard layout](/img/inscript-layout.jpg)

<i>
         <div style="text-align: center">
	 <a href="http://ildc.in/images/inscript-kb/Devnagari-Inscript-Layout.jpg">Inscript layout</a>. Copyright ©
	 <a href="https://tdil.meity.gov.in/Default.aspx">TDIL</a> under fair dealing. 
	 <a href="/img/inscript-layout.jpg">Click to enlarge</a>
	 </div>
</i>

Inscript keyboard physical [image](https://upload.wikimedia.org/wikipedia/commons/9/99/Devanagari_INSCRIPT_Keyboard.JPG) on _Wikipedia_.


### Cultural creations, websites and emails

Italics and capitalize for books/album/films/website names.

- _Malgudi Days_
- _Pink Panther_
- _Wikipedia_

If a chapter/song/article/titles from a large creation is called, single quotes without italics is used.

Emails - _mail@example.com_


### Numbers, time and dates

Spell numbers upto ten, figures for numbers above ten.

Use figures and symbols for percentages, measurements and currency.

- 12am (12 hour clock)
- 12:00 (24 hours)
- 12 April
- 1914-15
- BC, AD
- 7 billion or 7bn is fine but consistent across text
- Contact - 09876 554321


### Miscellaneous

Notes, side notes and updates should be italics. 
Updates and edit notices should follow the format: 
- _Update (03/2021): Text_
- _Edit 1: What was edited and why._
- _Edit 2: ANother edit._

Full stops outside brackets with an exception where the sentence starts and end inside brackets.

Menu options to be followed should be in format:

`Application` -> `Install` -> `Communication`


Checklist and multilevel checklist can be done with following syntax:
```bash
- [ ] First
- [X] Second
    - [X] Sub-second
- [ ] Third
```

### Quick links

- [Supported language codes for code highlighting](https://gohugo.io/content-management/syntax-highlighting/#list-of-chroma-highlighting-languages)
