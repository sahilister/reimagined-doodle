---
title: "Mobile Made More Private"
date: 2021-05-15T12:02:47+05:30
tags: ["privacy", "android", "mobile", "100DaysToOffload"]
---

As someone who is privacy concerned and also can't risk a bricked phone (or a new phone), I'm using the following measures to make my (dumb) smartphone silent towards it's "supposed" corporate overloads ;)

### Removal of app(lications) without root

I did manage to remove most of the bloatware through Chris Titus Tech's this [YouTube video](https://www.youtube.com/watch?v=k9ErL9L6KIw) (or it's [invidious link](https://yewtu.be/watch?v=k9ErL9L6KIw)). In this he showed uninstalling hard to delete root/locked apps through [ADB](https://developer.android.com/studio/command-line/adb). A written cheatsheet for it can be found [here](https://christitus.com/debloat-android/).

A fair warning and comment from him:
> It is dangerous to remove system apps that you don't see in the app drawer. It can lead to you doing a full system recovery.

> On Non-Rooted phones some applications are installed as root and error with "[DELETE_FAILED_INTERNAL_ERROR]". Type this to bypass:

> `adb shell pm uninstall --user 0 <appname>`


### Netguard

[Netguard](https://f-droid.org/en/packages/eu.faircode.netguard/) is an app that block internet access for other apps via a local VPN setup. It's internet access log showed me whenever an app tried phoning home. Following are some log screenshots:

![Netguard log showing attempted internet access by Phone app](/img/netguard-phone.jpeg)

<i>
        <div style="text-align: center">
        Netguard log showing attempted internet access by Phone app.
        </div>
</i>
<br>
<br>

![Netguard log showing attempted internet access by Camera app](/img/netguard-camera.jpeg)

<i>
        <div style="text-align: center">
        Netguard log showing attempted internet access by Camera app.
        </div>
</i>
<br>
<br>

![Netguard log showing attempted internet access by (remaining) Google apps](/img/netguard-google.jpeg)
<i>
        <div style="text-align: center">
        Netguard log showing attempted internet access by (remaining) Google apps.
        </div>
</i>
<br>
<br>

I use Netguard to block all traffic and making internet access for apps opt-in. This was done by disabling access to all apps and selectively enabling access to required apps only. Got added benefit of no in-app ads. 


### App stores

Preferred way is doing most stuff on a laptop or in mobile browser. For app installation F-Droid seems to have all useful utilities but for other times when apps are only available via Google Play Store, I use [Aurora Store](https://f-droid.org/en/packages/com.aurora.store/). Aurora Store is an alternate, privacy respecting client to Play Store which can work without any sort of Google account or almost no GApps. It allows me to install and update apps. An additional benefit of Aurora Store is that it shows [Exodus Privacy](https://exodus-privacy.eu.org/en/page/what/) reports in app information, showing embedded trackers (analytics, ad systems, etc) in a given app.

### Miscellaneous

Access Dots, is an app that shows a dot in the top right corner of the screen whenever an app is accessing camera, microphone or GPS. Shows me all accesses. It's only available on Play Store.

Another area which get overlooked much is app permissions. A camera app doesn't require access to contacts, a calculator doesn't need internet access and what not. Just changing the defaults did helped a lot.


<br>

An end note, this brings the topic of [privacy and/or security](https://blog.sahilister.in/2020/08/privacy-and-security/) back in the picture. Doing most of these would make the device less secure as whatever one's stance on privacy, Google does take active mitigations against security threats.
