---
title: "Updates: No Cloudflare Call for Blog Resource"
date: 2021-02-27T01:39:02+05:30
tags: ["100DaysToOffload", "updates"]
---
I'm pleased to announce that the blog is now 100% Cloudflare free and loads completely over GitLab Pages.

Now let me explain why it had Cloudflare in the first place. The content here is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license and [this](/img/licensebutton.png) license badge in the footer is used to prominently display it. This image file was loaded directly from [i.creativecommons.org](https://i.creativecommons.org) which is a redirect for [licensebuttons.net](https://licensebuttons.net/); static files for which are served through Cloudflare. I use [True Sight](https://addons.mozilla.org/en-US/firefox/addon/detect-cloudflare-plus/), a Firefox extension which shows all the CDNs called when a website loads. Seeing Cloudflare requests every time my blog loads wasn't a pretty site for me (considering Cloudflare's track record with internet centralization, aggressive stance against Tor users etc). The image file could be loaded locally but Hugo's image lookup order always failed me until now. Finally putting the image in `/static/img` directory worked (`/content/img` also works) and I hit deploy ( via `gcom` and `gp`. How? Read [here](/2020/07/alias-command/) and [here](https://github.com/sahilister/dotfiles/blob/dc0e4cb29ea4c4fdb2c6db8b16728d98de1b6c11/.aliases#L32)), removing the dependency on Cloudflare.

Now removing Cloudflare as CDN did pushed blog's first load time beyond 1s. I knew that on blog load, a favicon lookup was making a request for non-existent favicon file which in turn was adding more than [350ms overhead](https://gitlab.com/sahilister/reimagined-doodle/uploads/37677e54a9abdb8aecf2f1e4932d5805/210225-1010-14.png). Just removing the call from head section in `index.html` didn't work as browsers by default make that request. Searching the internet lead me to this one liner code that essentially disables it[^1]:

[^1]: Taken from the top comment in the top comment of this Stack Overflow [thread](https://stackoverflow.com/questions/1321878/how-to-prevent-favicon-ico-requests).

```html
<link rel="icon" type="image/png" href="data:image/png;base64,iVBORw0KGgo=">
```
This helped bring the first loads times to [1s or less](https://gitlab.com/sahilister/reimagined-doodle/uploads/3d0006fedae1bace245a8ac695976463/210225-1731-27.png) once again in most cases and complete blog load with just 3 requests.

_PS: If you have any suitable, minimal favicon(s) suggestions for this blog, feel free to hit me up on email from the [about section](/about/)._
