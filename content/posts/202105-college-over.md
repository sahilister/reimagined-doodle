---
title: "College Over"
date: 2021-05-30T01:19:39+05:30
tags: ["college", "over", "100DaysToOffload"]
---

And finally the day came. Today (or rather yesterday), I gave my final presentation and viva. BTech. (or BE officially) is almost done. My university will probably start handing out degrees in a few months, officially tagging me as a "Graduate". It isn't an emotional moment in any way. There would be no farewell ceremony (due to COVID) and prolonged isolation due to the pandemic starting last year, my 3rd year in college did away will all the things that could have made it emotional in any way. To be frank, I wasn't attached to my college anyhow. It was "school" as we used to call it. We even had uniforms albeit college t-shirt and blue or black jeans on most days. During my time there, my batch changed three or maybe four times (even more if the pandemic period is included). (Nothing specific, as we had multiple specializations and batches going on each semester, unknowingly/unintentional my batch/batch mates kept getting changed.) The after effect of which was that I had many acquaintances but fewer friends. After first year, my primary circle were not even my classmates. They were Abhay's classmates and that became my circle, my lunch buddies and at times my go-to people.  

As usual, I was engaged in various extracurriculars, major being in college chapter of Toastmasters International club and organizing a Model United Nation conference. Toastmasters gave me ample opportunities of going out and interacting with people from other colleges, community and industry clubs. It was always fun socializing with fellow Toastmasters.

It's fun remembering now that my first day in college was also my first time seeing my college/university. I never bothered even to look it up on the internet.

A close friend was saying today that now I'm free (at least for a while) which I straight away refused because I almost never free. Writing, servers, FSCI, Debian, SSB practice with friends, books and internet keeps me engaged, and  I absolutely love doing each and every of those activities. It's just that for a life, earning is important, so job (seeking at present) comes into picture. Working, experimenting and learnings on servers, internet and the infrastructure powering it during the past 8-9 months has shown me that system administration and cloud is my thing. Now I'm actively looking into internship/job opportunities in this field. Also, SSB is lined up for July. That would be another adventure. More excited about travelling (to Gujarat) than the actual thing.

Hence, the adventure called college is over (at least for now) but life continues.
_To infinity and beyond!_
