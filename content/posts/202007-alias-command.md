---
title: "Alias command"
date: 2020-07-08T02:26:10+05:30
tags: ["shell", "commands"]
---


Writing long commands is boring. Making commands smaller or chaining of commands helps. This can be achieved using `alias` command. [Alias](https://en.wikipedia.org/wiki/Alias_(command)) command is mainly used for abbreviating a system command, or for adding default arguments to a regularly used command.
  
For example, instead of writing:

```bash
git add .  
git commit -m "COMMIT MESSAGE"
git push
```  
we can write following to achieve the same goals.
```bash
gc "COMMIT MESSAGE"  
gp  
```

In the example, git commands were chained. Git doesn't allow chaining using `git alias` command but we can simply overcome that by `alias` command as shown above. Do visit  [git-scm.com](https://git-scm.com/book/en/v2/Git-Basics-Git-Aliases) to evaluate what's best for your use case.

## Usage
### For GNU/Linux (bash)

- Add `.bash_aliases` file in `~` `/home/USER_NAME` directory:

```bash
$ touch ~/.bash_aliases
```

- Edit the file to add:

```bash
alias gc='git add . && git commit -m '
alias gp='git push'
```
_Note - no spaces between the alias target and alias input commands and single quotes only._

- In terminal, write:

```bash
$ source ~/.bash_aliases
```
- Restart terminal.


### For Windows (Git Bash)

- Run git bash in administrator mode.

- Navigate to `C:\Program Files\Git\etc\profile.d` .

```bash
 cd ../../Program\ Files/Git/etc/profile.d/
```

- Open `aliases.sh` using `nano`:

```bash
nano aliases.sh
```

- Add following in the end:

```bash
# CUSTOM ALIASES

alias gc='git add . && git commit -m'
alias gp='git push'
```
_Note - no spaces between the alias target and alias input commands and single quotes only._

- Save and exit `nano`:

```bash
CTRL+O 
# PRESS ENTER
CTRL+X 
```
- Restart terminal.

## Use Cases

Some useful aliases can be:

```bash
alias update='sudo apt update && sudo apt upgrade'
alias rm='rm -i'
alias gc='git add . && git commit - '
alias gp='git push'
alias gr='git restore --staged'
```

My bash aliases can be found [here](https://git.fosscommunity.in/sahilister/dotfiles).







