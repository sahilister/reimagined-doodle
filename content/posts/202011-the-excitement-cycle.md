---
title: "The Excitement Cycle"
date: 2020-12-01T01:36:37+05:30
tags: ["personal", "excitement"]
---
I have noticed a predictable pattern in my excitement/motivation levels when I join a new community, project or an organization. This pattern closely resembles [Gartner](https://en.wikipedia.org/wiki/Gartner)'s [Hype Cycle](https://en.wikipedia.org/wiki/Hype_cycle) [chart](https://en.wikipedia.org/wiki/Hype_cycle#/media/File:Hype-Cycle-General.png). 

Initial phase is all about discovery and aww factor towards the community and folks behind the community. Steep upwards curve in excitement level is felt as more findings about the community come to light. A peak excitement level is reached where nothing seems better than this wonderful community I discovered. This excitement reaches an infliction point where cracks seem to appear in the excitement levels. Weaknesses, issues, infighting, not so good part of the community comes forth as the move towards the core community is made and this leads to steep decline in the excitement levels. This reaches a low or in terms of Gartner's Hype Cycle, trough of disillusionment comes where question of continuing with the community start arising. Initial day excitement are to be nowhere to be found. With time this phase passes with general understanding/enlightenment comes about the initial motivation behind joining the community. The excitement slopes starts moving upwards and excitement and motivation continues with gradual upwards rediscovery of the joy of being part of the community.

_PS: This was long overdue post. I believe I got this realization in mid October and wanted to document it here but kept slacking with no blog posts at all in November. Finally, it's here._

_PSS: Recently I discovered a writing prompt bot, [Promptodon](https://botsin.space/@promptodon) on Mastodon which posts almost everyday with a new prompt. Seeing a new prompt from it and thought of writing on them excites me. Plan to take some and write on them. Let's see how it goes._
