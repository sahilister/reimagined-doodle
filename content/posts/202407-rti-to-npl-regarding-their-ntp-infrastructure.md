---
title: "RTI to NPL Regarding Their NTP Infrastructure"
date: 2024-07-03T21:45:41+05:30
tags: ["RTI", "NPL", "NTP", "NTP-pool"]
---

I became interested in Network Time Protocol (NTP) last year after learning how fundamental this protocol is to the functioning of the global Internet. NTP helps synchronize clocks on devices over the Internet, which is essential for secure browsing, timestamping, keeping everyone in sync or just checking what time it is. Computers usually have a hardware real-time clock (RTC) but that deviates over time, so an occasional sync over NTP is required to keep the time accurate. Many network and IoT devices don't have hardware RTC so have even more reliance on NTP.

Accurate time keeping starts with reference clocks like atomic clocks, GPS etc. Multiple government standard agencies host these reference clocks, which are regarded as Stratum 0. Stratum 1 servers are known as primary servers, and directly connect to Stratum 0 clocks for time.  Stratum 1 servers then distribute time to Stratum 2 and further down the hierarchy. Computers typically connects to one or more Stratum 1/2/3... servers to get their time.

Someone has to host these public Stratum 1,2,3... NTP servers. That's what [NTP pool](https://www.ntppool.org/en/), a global effort by volunteers does. They provide NTP servers for the public to use. As of today, there are 4700+ servers in the pool which are free to use for anyone.

Now let's come to the reason for writing this post. [Indian Computer Emergency Response Team (CERT-In)](https://www.cert-in.org.in/) in April 2022 released [a set of cybersecurity directions](https://web.archive.org/web/20240629154228/https://www.cert-in.org.in/PDF/CERT-In_Directions_70B_28.04.2022.pdf) which set the alarm bells ringing. Internet Society (and almost everyone else) [wrote about it](https://www.internetsociety.org/resources/doc/2022/internet-impact-brief-india-cert-in-cybersecurity-directions-2022/).

And then there was this specific section about NTP:
> All service providers, intermediaries, data centres, body corporate and Government organisations shall connect to the Network Time Protocol (NTP) Server of National Informatics Centre (NIC) or National Physical Laboratory (NPL) or with NTP servers traceable to these NTP servers, for synchronisation of all their ICT systems clocks. Entities having ICT infrastructure spanning multiple geographies may also use accurate and standard time source other than NPL and NIC, however it is to be ensured that their time source shall not deviate from NPL and NIC.

CSIR-National Physical Laboratory (NPL) is the official timekeeper for India and hosts the only public Stratum 1 clock in India, according to [NTP pool website](https://support.ntp.org/Servers/PublicTimeServer001686). So I was naturally curious to know what kind of infrastructure they're running for NTP. India has a Right to Information (RTI) Act which, like the Freedom of Information Act (FOIA) in the United States, gives citizens rights to request information from governmental entities, to which they have to respond in under 30 days.  So last year, I filed two sets of RTI (one after the first reply came) inquiring about NPL's public NTP server setup.

The first RTI had some generic questions:
![RTI 1](/img/npl-rti/npl-rti-1.png)
<div style="text-align: center">
    <i>
        First RTI. <a href="/img/npl-rti/npl-rti-1.png">Click to enlarge</a> <br>
    </i>
</div>

This gave a vague idea about the setup, so I sat down and came with some specific questions in the next RTI.


![RTI 2](/img/npl-rti/npl-rti-2.png)
<div style="text-align: center">
    <i>
        Second RTI. <a href="/img/npl-rti/npl-rti-2.png">Click to enlarge</a> <br>
    </i>
</div>

Feel free to make your conclusions from it now. Bear in mind these were filled last year so things might have changed. Do let me know if you have more information about it.

_Update (07/07/2024): Found an [article from Medianama](https://www.medianama.com/2022/12/223-specs-ntp-nic-npl-time-servers-clocks-rti-2/) about Indian government time servers with information sourced through RTI._
