---
title: "A Late, Late Debconf23 Post"
date: 2024-05-27T23:30:37+05:30
tags: ["debconf", "debian", "debconf23", "experience", "kochi"]
---

After much procrastination, I have gotten around to complete my DebConf23 (DC23), Kochi blog post. I lost the original etherpad which was started before DebConf23, for jotting down things. Now, I have started afresh with whatever I can remember, months after the actual conference ended. So things might be as accurate as my memory.

[DebConf23](https://debconf23.debconf.org/), the 24th annual Debian Conference, happened in Infopark, Kochi, India from 10th September to 17th September 2023. It was preceded by DebCamp from 3rd September to 9th September 2023.

First formal bid to host DebConf in India was made during DebConf18 in Hsinchu, Taiwan by Raju Dev, which didn't came our way. In next DebConf, DebConf19 in Curitiba, Brazil, another bid was made by him with help and support from Sruthi, Utkarsh and the whole team.This time, India got the [opportunity to host DebConf22](https://lists.debian.org/debconf-announce/2020/02/msg00000.html), which eventually became [DebConf23 for the reasons](https://lists.debian.org/debconf-announce/2021/05/msg00000.html) you all know.

I initially met the local team on the sidelines of DebConf20, which was also my first DebConf. Having recently switched to Debian, DC20 introduced me to how things work in Debian. Video team's call for volunteers email pulled me in. Things stuck, and I kept hanging out and helping the local Indian DC team with various stuff. We did manage to organize multiple events leading to DebConf23 including [MiniDebConf India 2021 Online](https://blog.sahilister.in/2021/01/experience-volunteering-for-minidebconf-india-2021/), [MiniDebConf Palakkad 2022](https://blog.sahilister.in/2023/01/minidebconf-palakkad-2022/), [MiniDebConf Tamil Nadu 2023](https://blog.sahilister.in/2023/02/minidebconf-tamil-nadu-2023/) and [DebUtsav Kochi 2023](https://blog.sahilister.in/2023/06/debutsav-kochi-2023/), which gave us quite a bit of experience and workout. Many local organizers from these conferences later joined various DebConf teams during the conference to help out.

For DebConf23, originally I was part of publicity team because that was my usual thing. After a team redistribution exercise, Sruthi and Praveen moved me to sponsorship team, as anyhow we didn't had to do much publicity and sponsorship was one of those things I could get involved remotely. Sponsorship team had to take care of raising funds by reaching out to sponsors, managing invoices and fulfillment. Praveen joined as well in sponsorship team. We also had international sponsorship team, Anisa, Daniel and various Debian Trusted Organizations (TO)s which took care of reaching out to international organizations, and we took care of reaching out to Indian organizations for sponsorship. It was really proud moment when my present employer, Unmukti (makers of [hopbox](https://hopbox.net)) came aboard as Bronze sponsor. Though fundraising seem to be hit hard from tech industry slowdown and layoffs. Many of our yesteryear sponsors couldn't sponsor.

We had biweekly local team meetings, which were turned to weekly as we neared the event. This was done in addition to biweekly global team meeting.

<img loading="lazy" alt="Pathu" src="/img/dc23/dc23-pathu.jpg">
<div style="text-align: center">
    <i> Pathu, DebConf23 mascot</i>
</div>

 To describe the conference venue, it happened in InfoPark, Kochi with the main conference hall being Athulya Hall and food, accommodation and two smaller halls in Four Point Hotel, right outside Infopark. We got Athulya Hall as part of venue sponsorship from Infopark. The distance between both of them was around 300 meters. Halls were named [Anamudi](https://en.wikipedia.org/wiki/Anamudi), [Kuthiran](https://en.wikipedia.org/wiki/Kuthiran) and [Ponmudi](https://en.wikipedia.org/wiki/Ponmudi) based on hills and mountain areas in host state of Kerala. Other than Annamudi hall which was the main hall, I couldn't remember the names of the hall, I still can't. Four Points was big and expensive, and we had, as expected, cost overruns. Due to how DebConf function, an Indian university wasn't suitable to host a conference of this scale.

<img loading="lazy" alt="Infinity Pool at Night" src="/img/dc23/dc23-infinity-pool-at-night.jpg">
<div style="text-align: center">
    <i> Four Point's Infinity Pool at Night </i>
</div>

I landed in Kochi on the first day of DebCamp on 3rd September. As usual, met Abraham first, and the better part of the next hour was spent on meet and greet. It was my first IRL DebConf so met many old friends and new folks. I got a room to myself. Abraham lived nearby and hadn't taken the accommodation, so I asked him to join. He finally joined from second day onwards. All through the conference, room 928 became in-famous for various reasons, and I had various roommates for company. In DebCamp days, we would get up to have breakfast and go back to sleep and get active only past lunch for hacking and helping in the hack lab for the day, followed by fun late night discussions and parties.

<img loading="lazy" alt="Nilesh, Chirag and Apple at DC23" src="/img/dc23/dc23-nilesh-chirag-and-apple-at-dc23.jpg">
<div style="text-align: center">
    <i> Nilesh, Chirag and Apple at DC23 </i>
</div>

The team even managed to get a press conference arranged as well, and we got an opportunity to go to Press Club, Ernakulam. Sruthi and Jonathan gave the speech and answered questions from journalists. The event was [covered by media](https://www.apnnews.com/debconf-2023-indias-first-debian-conference-to-be-held-at-infopark/) as well due to this.

<img loading="lazy" alt="Ernakulam Press Club" src="/img/dc23/dc23-ernakulam-press-club.jpg">
<div style="text-align: center">
    <i> Ernakulam Press Club </i>
</div>

During the conference, every night the team use to have 9 PM meetings for retrospection and planning for next day, which was always dotted with new problems. Every day, we used to hijack Silent Hacklab for the meeting and gently ask the only people there at the time to give us space.

DebConf, it itself is a well oiled machine. Network was brought up from scratch. Video team built the recording, audio mixing, live-streaming, editing and transcoding infrastructure on site. A gaming rig served as router and gateway. We got internet uplinks, a 1 Gbps sponsored leased line from Kerala Vision and a paid backup 100 Mbps connection from a different provider. IPv6 was added through HE's Tunnelbroker. Overall the network worked fine as additionally we had hotel Wi-Fi, so the conference network wasn't stretched much. I must highlight, DebConf is my only conference where almost everything and every piece of software in developed in-house, for the conference and modified according to need on the fly. Even event recording cameras, audio check, direction, recording and editing is all done on in-house software by volunteer-attendees (in some cases remote ones as well), all trained on the sideline of the conference. The core recording and mixing equipment is owned by Debian and travels to each venue. The rest is sourced locally.

<img loading="lazy" alt="Gaming Rig which served as DC23 gateway router" src="/img/dc23/dc23-video-team-router-gateway-setup.jpg">
<div style="text-align: center">
    <i> Gaming Rig which served as DC23 gateway router </i>
</div>


It was fun seeing how almost all the things were coordinated over text on Internet Relay Chat (IRC). If a talk/event was missing a talkmeister or a director or a camera person, a quick text on #debconf channel would be enough for someone to volunteer. Video team had a dedicated support channel for each conference venue for any issues and were quick to respond and fix stuff.

<img loading="lazy" alt="Network information. Screengrab from closing ceremony" src="/img/dc23/dc23-network-screengrab.png">
<div style="text-align: center">
    <i> Network information. Screengrab from closing ceremony </i>
</div>

It rained for the initial days, which gave us a cool weather. Swag team had decided to hand out umbrellas in swag kit which turned out to be quite useful. The swag kit was praised for quality and selection - many thanks to Anupa, Sruthi and others. It was fun wearing different color T-shirts, all designed by Abraham. Red for volunteers, light green for Video team, green for core-team i.e. staff and yellow for conference attendees.

<img loading="lazy" alt="With highvoltage" src="/img/dc23/dc23-me-with-jonathon.jpg">
<div style="text-align: center">
    <i> With highvoltage </i>
</div>

We were already acclimatized by the time DebConf really started as we had been talking, hacking and hanging out since last 7 days. Rush really started with the start of DebConf. More people joined on the first and second day of the conference. As has been the tradition, an [opening talk](https://debconf23.debconf.org/talks/18-opening-ceremony/) was prepared by the Sruthi and local team (which I highly recommend watching to get more insights of the process). DebConf day 1 also saw job fair,  where Canonical and FOSSEE, IIT Bombay had stalls for community interactions, which judging by the crowd itself turned out to be quite a hit.

For me, association with DebConf (and Debian) started due to volunteering with video team, so anyhow I was going to continue doing that this conference as well. I usually volunteer for talks/events which anyhow I'm interested in. Handling the camera, talkmeister-ing and direction are fun activities, though I didn't do sound this time around. Sound seemed difficult, and I didn't want to spoil someone's stream and recording. Talk attendance varied a lot, like in Bits from DPL talk, the hall was full but for some there were barely enough people to handle the volunteering tasks, but that's what usually happens. DebConf is more of a place to come together and collaborate, so talk attendance is an afterthought sometimes.

<img loading="lazy" alt="Audience in highvoltage's Bits from DPL talk" src="/img/dc23/dc23-bits-from-dpl-audience.jpg">
<div style="text-align: center">
    <i> Audience in highvoltage's Bits from DPL talk </i>
</div>

I didn't submit any talk proposals this time around, as just being in the orga team was too much work already, and I knew, the talk preparation would get delayed to the last moment and I would have to rush through it.

<img loading="lazy" alt="Enrico's talk" src="/img/dc23/dc23-enricos-talk.jpg">
<div style="text-align: center">
    <i> Enrico's talk </i>
</div>

From Day 2 onward, more sponsor stalls were introduced in the hallway area. Hopbox by Unmukti , MostlyHarmless and Deeproot (joint stall) and FOSEE. MostlyHarmless stall had nice mechanical keyboards and other fun gadgets. Whenever I got the time, I would go and start typing racing to enjoy the nice, clicky keyboards.

As the DebConf tradition dictates, we had a Cheese and Wine party. Everyone brought in cheese and other delicacies from their region. Then there was yummy Sadya. Sadya is a traditional vegetarian Malayalis lunch  served over banana leaves. There were loads of different dishes served, the names of most I couldn't pronounce or recollect properly, but everything was super delicious.

Day 4 was day trip and I chose to go to Athirappilly Waterfalls and Jungle safari. Pictures would describe the beauty better than words. The journey was bit long though.

<img loading="lazy" alt="Athirappilly Falls" src="/img/dc23/dc23-athirappilly-falls.jpg">
<div style="text-align: center">
    <i> Athirappilly Falls </i>
</div>
<br>

<img loading="lazy" alt="Pathu" src="/img/dc23/dc23-day-trip-1.jpg">

<img loading="lazy" alt="Pathu" src="/img/dc23/dc23-day-trip-2.jpg">

<img loading="lazy" alt="Tea Gardens" src="/img/dc23/dc23-tea-gardens.jpg">
<div style="text-align: center">
    <i> Tea Gardens </i>
</div>

Late that day, we heard the news of Abraham gone missing. [We lost Abraham.](https://blog.sahilister.in/2023/09/abraham-raji/) He had worked really hard all through the years for Debian and making this conference possible. Talks were cancelled for the next day and Jonathan addressed everyone. We went to Abraham's home the next day to meet his family. Team had arranged buses to Abraham's place. It was an unfortunate moment that I only got an opportunity to visit his place after he was gone.

Days went by slowly after that. The last day was marked by a small conference dinner. Some of the people had already left. All through the day and next, we kept saying goodbye to friends, with whom we spent almost a fortnight together.

<img loading="lazy" alt="Athirappilly Falls" src="/img/dc23/dc23-everyone-seeing-up-group-photo.jpg">
<div style="text-align: center">
    <i> Group photo with all DebConf T-shirts chronologically </i>
</div>

This was 2nd trip to Kochi. Vistara Airway's UK886 has become the default flight now. I have almost learned how to travel in and around Kochi by Metro, Water Metro, Airport Shuttle and auto. Things are quite accessible in Kochi but metro is a bit expensive compared to Delhi. I left Kochi on 19th. My flight was due to leave around 8 PM, so I had the whole day to myself. A direct option would have taken less than 1 hour, but as I had time and chose to take the long way to the airport. First took an auto rickshaw to Kakkanad Water Metro station. Then sailed in the water metro to Vyttila Water Metro station. Vyttila serves as intermobility hub which connects water metro, metro, bus at once place. I switched to Metro here at Vyttila Metro station till Aluva Metro station. Here, I had lunch and then boarded the Airport feeder bus to reach Kochi Airport. All in all, I did auto rickshaw > water metro > metro > feeder bus to reach Airport. I was fun and scenic. I must say, public transport and intermodal integration is quite good and once can transition seamlessly from one mode to next.

<img loading="lazy" alt="Kochi Water Metro" src="/img/dc23/dc23-kochi-water-metro.jpg">
<div style="text-align: center">
    <i> Kochi Water Metro </i>
</div>
<br>

<img loading="lazy" alt="Scenes from Kochi Water Metro" src="/img/dc23/dc23-kochi-water-metro-scenery.jpg">

<img loading="lazy" alt="Scenes from Kochi Water Metro" src="/img/dc23/dc23-scenes-from-water-metro.jpg">
<div style="text-align: center">
    <i> Scenes from Kochi Water Metro </i>
</div>


DebConf23 served its purpose of getting existing Debian people together, as well as getting new people interested and contributing to Debian. People who came are still contributing to Debian, and that's amazing.

<img loading="lazy" alt="Streaming video stats" src="/img/dc23/dc23-streaming-video-screengrab.png">
<div style="text-align: center">
    <i> Streaming video stats. Screengrab from closing ceremony </i>
</div>

The conference wasn't without its fair share of troubles. There were multiple money transfer woes, and being in India didn't help. Many thanks to multiple organizations who were proactive in helping out. On top of this, there was conference visa uncertainty and other issues which troubled visa team a lot.

Kudos to everyone who made this possible. Surely, I'm going to miss the name, so thank you for it, you know how much you have done to make this event possible.

Now, DebConf24 is scheduled for Busan, South Korea, and work is already in full swing. As usual, I'm helping with the fundraising part and plan to attend too. Let's see if I can make it or not.

<img loading="lazy" alt="DebConf23 Group Photo" src="/img/dc23/dc23-group-photo-small.jpg">
<div style="text-align: center">
    <i> DebConf23 Group Photo.  <a href="/img/dc23/dc23-group-photo-small.jpg">Click to enlarge.</a> <br> Credits - <a href="https://aigarius.com/blog/2023/09/17/debconf23-photos/">Aigars Mahinovs</a></i>
</div>

In the end, we kept on saying, no DebConf at this scale would come back to India for the next 10 or 20 years. It's too much trouble to be frank. It was probably the peak that we might not reach again. I would be happy to be proven wrong though :)

