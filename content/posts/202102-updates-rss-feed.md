---
title: "Updates: Blog RSS Feed"
date: 2021-02-10T22:33:18+05:30
tags: ["feed", "rss", "updates", "100DaysToOffload"]
---

Finally, after almost 7 months of its existence, the blog now has a Really Simple Syndication (RSS) feed. The feed URL is [https://blog.sahilister.in/index.xml](https://blog.sahilister.in/index.xml)

Hugo (on which this blog is) by default provides RSS feeds. I had to disable mine when the hugo package in Debian 10 stable (buster) had issues loading with the RSS template file. Removing the file solved it for the time being back then. That issue was sorted in the new versions but never made into stable. Now as I run Debian testing (which closely tracks the upstream release), I simply re-added the RSS template to enable the feed.

Open web for the win \o/
