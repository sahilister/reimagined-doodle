---
title: "Aapka Bunty and Some Thoughts"
date: 2022-03-19T12:32:38+05:30
tags: ["book", "thoughts"]
---
Stories are windows in someone's life. The author may find it suitable to open and close it at a juncture, leaving us in a mix of emotions. We may wish it closes at different point or the window is open a little longer to peer what happened with the protagonist, but as the story window open at random it may well close as that.

_आपका बंटी_    _(Aapka Bunty)_ by Mannu Bhandari was about a small boy and his relations with divorced parents. He used to live with his mother (who later remarried, just like his father). The surge of emotions; fear, anger, confusion made him disconnected, stubborn and disagreeable. The complexity of circumstances made me want the mother-son could come back to former connected self, but alas, the emotional turmoil broke them apart. Things may look simple from outside, but internally, feelings gets' harder to make sense or connect. Our feelings for someone's or something remains and are usually hard to change.

_Aapka Bunty_ stirred and brought back memories of my childhood. School, family, friends were fine but mentally, all sorts of thoughts, fear, at times anger, new realizations about my self and world around kept my brain buzzing. Most times, I wouldn't share those with anyone (which may have sorted many of them) but most times trusted another soul was hard to come by. The story hit hard and brought back memories of those time almost 13-14 years later now. After reading it, I also realized that how privileged to have guidance, love and care from both parents. It's even more significant as I transition to an in-office workplace at my first job in Gurgaon. Father guiding me about the world around and mother taking care of my household needs; complementing each other to ease the move.

An interesting aspect I find is the sea of different emotions portrayed/described in Hindi literature, which I could not find in English or translated literature. Maybe English too has its depth, but I could connect to Hindi completely.

As usual, as the end of the novel was drawing near; an excitement of end and a dread of closing of the window coincided. The window in Bunty's life closed, leaving with thought's how he coped the now physical distance from his mother and incoming hostel life. Surely this would have adverse effects on his teenage and adult life later, or perhaps he came to peace with his life.  
