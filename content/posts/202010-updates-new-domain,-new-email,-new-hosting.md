---
title: "Updates: New Domain, New Email, New Hosting"
date: 2020-10-20T01:34:00+05:30
tags: ["updates", "domain", "email", "hosting"]
---
It's been a while I wrote a blog. Initial plan was to get a new domain, switch hosting and start an RSS feed and write an update blog post on it. I kept postponing the RSS feed work and that let to postpoing the update blog (which was destined to be the next blog post, leading to this long gap). RSS feed would come in due course.

Coming back to the updates, as you can see the blog has moved from _blog.sahilister.tech_ to _blog.sahilister.in_ . Had got the _.tech_ domain from GitHub Students Pack and eventually decided to buy .in domain as birthday present to self. Was wondering to move email providers away from Gmail and ended up getting domain from [Gandi](https://www.gandi.net) which provides custom emails with each new domain bought there, so new email address too (you can get my latest mail from about section).

In the past the blog was hosted on GitHub pages which has been moved to its new home at GitLab pages [here](https://gitlab.com/sahilister/reimagined-doodle) for obvious reasons.

That's all for the updates for now, see you until next time. :)
