---
title: "termux.sahilister.in, a Termux Mirror Is Live Now"
date: 2022-01-12T17:05:56+05:30
tags: ["termux", "mirror", "server", "100DaysToOffload"]
---
[_termux.sahilister.in_](https://termux.sahilister.in/), a [termux](https://termux.com/) mirror is live now.

To use it, `termux-change-repo` (part `termux-tools` package) can be used to modify sources. Doing `apt edit-sources` and using the following lines, would achieve the same purpose:

```bash
# main
deb https://termux.sahilister.in/apt/termux-main stable main

# root
deb https://termux.sahilister.in/apt/termux-root root stable

# X11
deb https://termux.sahilister.in/apt/termux-x11 x11 main
```

The mirror is hosted on Contabo, in Germany, and syncs with upstream every 6 hours.

Since the time I got in servers and hosting, I wanted to help the community by hosting a free software mirror, but most required too much storage or already had an abundant number of mirrors. Recently, I got to know about termux and saw the [less number of its mirrors](https://github.com/termux/termux-packages/wiki/Mirrors). Thought that this might be the right candidate for a mirror, I started setting it up. A [Reddit reply](https://www.reddit.com/r/termux/comments/nizn7x/comment/gz5dtvs/) from termux dev team member and ['A New Termux Mirror'](https://www.librehat.com/a-new-termux-mirror/) post by [Librehat](https://www.librehat.com/), another mirror operator, made the setup a breeze. Finally, I opened an issue on [termux-packages repo](https://github.com/termux/termux-packages/issues) to get it added in official mirrors wiki and package.  

Update - The termux mirror has been moved domain. It now can be accessed at http://mirrors.sahilister.in/termux.  

