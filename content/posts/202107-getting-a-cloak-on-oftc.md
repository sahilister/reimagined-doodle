---
title: "Getting a Cloak on OFTC"
date: 2021-07-03T08:26:03+05:30
tags: ["irc", "oftc", "cloak", "100DaysToOffload"]
---
I'm exploring IRC a bit and kind of like it as well. It's simple, fast and minimal in terms of resources usage. Recently, I registered my nick, sahilister on OFTC. Feel free to ping ;)

Getting a user cloak on OFTC is not as quick [as on Libera Chat](https://blog.sahilister.in/2021/06/getting-a-cloak-on-libera-chat/), but it's easy. They provide `<uuid>.user.oftc.net` and `<nick>.user.oftc.net` . The canonical source of information for user cloak on OFTC can be found [here](https://www.oftc.net/UserCloaks/).

Steps to get a user cloak boils down to following:

- Register account on IRC.
- Login and verify account on [_services.oftc.net_](https://services.oftc.net/) by solving a reCAPTCHA.
- (cloak option 1) for quick, non-personalized `<uuid>.user.oftc.net` cloak, simply text following on IRC:
```bash
/msg nickserv SET CLOAK ON
```
- (cloak option 2) personalized `<nick>.user.oftc.net` can be availed by emailing registered nick to _cloakrequest AT oftc.net_ . This isn't an automated process, so would take time according to staff availability.


_PS - You can find an incomplete list of IRC networks and if user cloaks are available on them under "Known Networks" on [this page on BNC4Free](https://bnc4free.com/?page_id=63)._
