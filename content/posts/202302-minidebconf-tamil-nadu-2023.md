---
title: "MiniDebConf Tamil Nadu 2023"
date: 2023-02-05T11:39:27+05:30
tags: ["debian", "minidebconf", "experience", "tamil-nadu", "mdc", "villupuram"]
---
The first MiniDebConf of year 2023 - MiniDebConf Tamil Nadu 2023 or MDC TN23 in short, happened on 28th and 29th of January 2023 in University College of Engineering Viluppuram in Tamil Nadu. It was done with the local GNU/Linux user group - Villupuram GLUG. 

Officially speaking, MDC TN23 was announced at the closing ceremony for [MDC Palakkad 2022](https://blog.sahilister.in/2023/01/minidebconf-palakkad-2022/) in November of 2022 by VGLUG. I got to know from Praveen that the conversation around this started during VGLUG's Software Freedom Day celebration in September, which Praveen and Sruthi attended. Post MDC Palakkad, Praveen invited all the interested folks to join in organizing MDC TN23 with VGLUG team. The prepartions started and VGLUG team, being organized and experienced in event management, did most of the work with inputs from Debian India folks. There were some gaps in communications and disagreements like on budgeting but after many meetings, we came to an agreeable enough budget (and got the requested budget approved from DPL/Debian).

As usual I asked Ravi if he planned to attend, to which he said yes. The journey started at New Delhi airport. The flight was to take off at 7.40 AM and I had planned to reach the airport by 5 AM, due to long security and check-in times. I was seriously doubting myself getting up at 4 AM, getting in the taxi by 4:30 and reaching airport by 5 AM, but I did indeed made it. I met Ravi at the airport, and we flew down to Chennai airport. We then took the Chennai Metro and met Nilesh with whom we took a train to Villupuram where a cab was waiting to take us to our accommodation. There we met rest of the Debian India team and other speakers.

The opening ceremony took place in a marriage hall and guest of honor was the Member of Parliament (MP) from Villupuram, Dr D Ravikumar, which was a first for us followed by the events at University College of Engineering. This time, I had only submitted a DebConf23 introductory talk and took the time to enjoy the conference rather than worrying about my talk and stuff. I had planned to set up my mail server and update _poddery.com_'s Synapse server update with team, but didn't got the time to do any. The number of interesting conversations, meeting friends and fellow community members, and interesting events kept me engaged. Though I did complete my [MDC Palakkad](https://blog.sahilister.in/2023/01/minidebconf-palakkad-2022/) blog post and setting up Onion URL for Debian Fasttrack. 

![Some of the members of DebConf23 team](/img/mdc-tn-debconf23-team-photo.jpg)
<div style="text-align: center">
    <i>
    Some of the members of DebConf23 team
    </i>
</div>

As usual, we had DebConf23 meetings on the sideline. This time, international DebConf team including Nattie, Gwolf and Tzafir too joined for the conf, as the team planned to do a DebConf23 venue visit in Kochi post conf. Also meet them for the first time due to this. 

During the visit, I tried a variety of Tamil foods and snacks, many names I can't even pronounce properly. I had a hearty serving of filter coffee and even started to enjoy Rasam. However, I didn't like the taste of ghee masala dosa. I also visited Ratna Cafe and Sangeetha Cafe in Chennai, and my desire to try local food was fully satisfied during this visit.

My Debian Developer, non uploading AM process was already completed by 15th, so had hopped that the DD process would have completed by the time of the conf, but that didn't happen. The FD or DAM approval step was still pending. Now the next conf would become my first conf post becoming a DD :D.


The event was also covered by New Indian Express, though they got many of the details wrong. Here's the [link](https://www.newindianexpress.com/states/tamil-nadu/2023/jan/31/techies-converge-at-the-debconf-in-villupuram-2542780.html) to the news article.

![MiniDebConf Tamil Nadu 2023 group photo](/img/mdc-tn-group-photo.jpg)
<div style="text-align: center">
    <i>
    MiniDebConf Tamil Nadu 2023 group photo. <a href="/img/mdc-tn-group-photo.jpg">Click to enlarge</a>
    </i>
</div>

Hoping that MiniDebConf Tamil Nadu would becomes an annual thing, as we get to meet, learn, chill and re-visit Tamil Nadu every year. 



















