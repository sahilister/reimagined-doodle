---
title: "Second Tor Relay on Same Node"
date: 2021-12-29T12:54:58+05:30
tags: ["tor", "privacy", "hosting", "server", "100DaysToOffload"]
---
Tor project says[^1]:

> _Note: You can only run two Tor relays per public IPv4 address._

[^1]: ["Tor Project | Relay Requirements"](https://community.torproject.org/relay/relays-requirements/#public-ipv4-address)

If the bandwidth, CPU and memory usage allow, hosting a second Tor relay on the same machine is possible to maximize resource utilization.

Here's how I hosted a secondary Tor relay on a Debian machine, which was already running a middle (soon to be guard) relay. Debian systems already are capable of running multiple relays via `tor_instance_create` package which is already part of Debian/Ubuntu Tor package, the manual page of which can be found [here](https://manpages.debian.org/testing/tor/tor-instance-create.8.en.html).

Here's how to setup the node:
```bash
sudo /usr/sbin/tor-instance-create <relay-name>
sudo vim /etc/tor/instances/<relay-name>/torrc
```
Comment out +SocksPort auto line.
Add the [details for the second relay](https://community.torproject.org/relay/setup/guard/debian-ubuntu/#4-configuration-file) on a different port and nickname. 

Next, we need to add both relays fingerprint in their `torrc`, so clients don't make a circuit via both at once. We avoid this by adding the `MyFamily` variable. To get the fingerprint of second relay, start the relay, wait for the bootstrapping process by monitoring the `syslog` (found in `/var/log/syslog`). Do the following to get the fingerprint:
```bash
# start the relay
sudo systemctl start tor@<relay-name>

# wait for fingerprint generation and then
sudo systemctl stop tor@<relay-name>
```
Next, add the following line in both relay's `torrc` files:
```bash
MyFamily <relay-1-fingerprint>, <relay-2-fingerprint>
```

Restart Tor for it to take effect:
```bash
sudo systemctl restart tor
```
It will restart both the relays at once.

Monitor `syslog` for progress. Verify everything is fine by visiting [_metrics.torproject.org_](https://metrics.torproject.org/rs.html#search) (after three hours) to check if "Effective Family Members" and other settings are as intended.

And that's how we hammer our server for the benefit of the society ;)


