---
title: "Humans and Home"
date: 2022-03-23T08:11:00+05:30
tags: ["human", "home", "thoughts"]
---

I always wonder that humans erect walls to keep themselves safe when they're most vulnerable, ie in their sleep. Walls or something akin gives a feeling of protection. It protects them from natural elements (weather, heat, rain etc.), seems to be a basic human instinct to want to be safe.

Eventually, it seems as possession keeps on mounting (as compared to our hunter-gather roots), those walls gave protection to possessions as well. A reassurance of safekeeping and ownership that everything between these walls is "mine". And we started calling the area between the walls as our place or home.



