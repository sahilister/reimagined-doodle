---
title: "How I Write Blogs - April 2021 Edition"
date: 2021-04-27T02:45:09+05:30
tags: ["blog", "writing", "100DaysToOffload"]
---

Topics for the next blog post usually strike while the previous one is in work. A usual cycle from conceptualization, first draft, edits, corrections to posting on blog takes around 2-4 days for me. 

I usually prefer writing my initial drafts in a spiral notebook using Montek Megatop (my sole choice for pen since the last 6 years). Writing in running (and sometimes beautiful) handwriting and direct, easy flow of words onto paper are two factors encouraging this workflow. This draft is usually complete in 3-5 [pomodoro](https://en.wikipedia.org/wiki/Pomodoro_Technique) sprints (using [this](https://sahilister.github.io/Clock/pomodoro/)) spread over 1-2 days. It is then typed onto my laptop in blog's git repository using vim or kate. Hugo live server is kept running while typing for adding links, references and to see actual rendering of text. A minimal amount of editing is done during this phase. The process of typing (typing is fun at times) and seeing new text appearing on (local) blog gives a feeling of satisfaction and progress.

![Initial handwritten draft for 'What Is My Legacy?'](/img/first-draft.jpg)
<i>
         <div style="text-align: center">
            Initial handwritten draft for
            <a href="https://blog.sahilister.in/2021/04/what-is-my-legacy/">'What Is My Legacy?'</a>
        </div>
</i>

After this, each paragraph is copied into [LanguageTool](https://languagetool.org/) online grammar and spell checker. A fair number of spelling errors get rectified here. Now's the time for concentrated reading and making changes. Paragraphs are moved, edited or removed to make a coherent story. Efforts to reduce the number of words is also made to the blog post crisp and on point.
Another round of grammar and spell check is done. After a final read, the post is pushed to GitLab followed by posting a link on mastodon. This doesn't mean the post is final. I keep on revisiting and finding issues and correcting them; sometimes months past posting it.

![Blog writing on laptop](/img/blog-writing-in-progress.png)
<i>
         <div style="text-align: center">
            Writing in progress using Hugo local server and kate. 
            <a href="/img/blog-writing-in-progress.png">Click to enlarge</a>
        </div>
</i>

This is mostly the workflow of used to create new blog posts though there are some exceptions. Posts like ['What Is My Legacy?'](https://blog.sahilister.in/2021/04/what-is-my-legacy/) or ['What Is Life?'](https://blog.sahilister.in/2021/03/what-is-life/) were conceptualized, written and posted in matters of hours. All the experience posts usually take a week each. Tutorial write-up like this [one](https://blog.sahilister.in/2021/01/beginners-guide-to-host-static-content-on-a-server/) or this [one](https://blog.sahilister.in/2021/04/script-to-automatically-mirror-blog-on-seperate-domain/) are usually written while trying each step as follow along. Sometimes I directly start writing on laptop when I don't feel like writing by hand or am hitting a writer-block.

Just like ['My Setup'](https://blog.sahilister.in/2020/08/my-setup-august-2020-edition/) post, I would try writing an update in the foreseeable future if some major change comes in this workflow.

_PS: There's quite a bit of different categories/types of blog posts here on Reimagined Doodle now like [quick-wikis](https://blog.sahilister.in/2021/04/quick-wiki-searx/), [random thoughts](https://blog.sahilister.in/2021/04/sleep/), [experience posts](https://blog.sahilister.in/2021/04/experience-attending-free-software-camp-2020/), [tutorials write-ups](https://blog.sahilister.in/2021/01/beginners-guide-to-host-static-content-on-a-server/), [updates](https://blog.sahilister.in/2021/02/updates-no-cloudflare-call-for-blog-resource/), [learning and notes](https://blog.sahilister.in/2021/04/sync-git-repository-with-upstream/) and [researched articles](https://blog.sahilister.in/2021/03/internet-a-network-of-networks/). Nice variety :D_
