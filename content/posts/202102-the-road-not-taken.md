---
title: "The Road Not Taken"
date: 2021-02-23T20:11:52+05:30
tags: ["100DaysToOffload", "life"]
---

['The Road Not Taken'](https://en.wikisource.org/wiki/The_Road_Not_Taken) by Robert Frost is one interesting poem.

First came across this poem in secondary school. Back then it was just another poem in English textbook; read and moved on. It returned in my sophomore year when I joined [Toastmasters](https://www.toastmasters.org/about/all-about-toastmasters) and had to give my icebreaker speech.

_Side note - Icebreaker is the first speech a new joinee or new Toastmaster gives. It's a speech to introduce and share something about oneself and break the ice._

Not sure where and why I remembered the poem then. I felt, ‘The Road Not Taken’ does define a part of my life journey. Being part of the group and joining completely different organizations which excited me was always my thing. Most times, I would introduce my friends to those organizations but seldom in my past life they joined. Most times, alone I would go and enlist myself to new organizations without knowing anyone there. The experience never disappointed, never! Initially things would be a bit awkward but eventually knowing folks there, the culture and just a curious mind helped. Wherever I went, folks were helpful and cheerful (humans generally care for others and seek company). And that was my divergent road, that made who I’m today.

Coming back to the icebreaker, the last stanza from the poem was used in full to conclude my icebreaker speech (which in my opinion was a dope idea). Though I forgot the stanza and had to consult my notes. 

The poem gives the vibes that every decision we make, even smaller ones, tend to affect us in the long run. It's like [tree data structure](https://upload.wikimedia.org/wikipedia/commons/5/5f/Tree_%28computer_science%29.svg), one decision leads to more decisions to make and the cycle repeats. If only one decision/variable differed in the past, life might not have turned like what it is now. Like if I hadn't paid interest to English language and literature, I wouldn't be into writing (writing in particular not blogging). If I hadn't been into computers, I wouldn't have joined [dgplug](https://dgplug.org/)'s [summer training](https://foss.training/) (which was due to my interest in GNU/Linux (bolstered by the pandemic) which in turn was due to frustrations with Windows, and it's privacy practices) which led to blog writing which is chronicled in '[Why I Write (Blogs)](https://blog.sahilister.in/2020/10/why-i-write-blogs/)' post. Many variables fell in place to make this blog and life happen the way it is.

Now, I'm not sure about my future (career wise). University is almost over, nothing is settled (or looks positive). In hindsight, things could have happened in a grossly (not in negative sense) different way, but I'm okay-ish with how things turned out. Writing, Free Software communities, reading and life gives me hope. Have been happy-go-lucky (I believe) uptil now, let's see how the forthcoming life turns out to be. Now I would like to conclude this blog post just like how I ended my icebreaker speech, with the last stanza of 'The Road Not Taken':

> _I shall be telling this with a sigh <br>
Somewhere ages and ages hence:	<br>
Two roads diverged in a wood, and I— <br>
I took the one less travelled by, <br>
And that has made all the difference._ <br> 
