---
title: "Navratri Hunger Cycle"
date: 2022-05-01T09:14:00+05:30
tags: ["life", "fasts", "navratri"]
---
[Navratri](https://en.wikipedia.org/wiki/Navaratri) is a bi-annual celebration in Hinduism spanning over nine (sometimes eight) days. It is usually celebrated with fasting and ends with various ceremonies. Now, fasting is our topic for this post today. I have been fasting for Navratri once a year, since quite a few years. Due to family traditions, we only do fasting till eighth day, which is called _Ashtami_. I have some observation on my hunger and thought patterns during these fasting periods.

The initial, one to three days are filled with thoughts about all the delicacies the world has to offer. Spicy, _chat-patta_ _Gol Gappas_, _Chole Samosa_, _Gulab Jamuns_ and what not. It's the period when most will power is tested on the question of why even deprave oneself from all these worldly pleasures.

Day four to six are when, the mind usually makes peace with the situation. It doesn't matter if I eat food or not, it doesn't cross my mind. Things are better, there are no hunger pangs. Even if someone eats or brings some delicacy, it really doesn't matter much.

Day seven is usually an exciting day that finally it is coming to an end. A bunch of eat this and that lists starts forming in mind.

Day eight, the day when _Chole Puri_ and _Halwa_ are made for [_Puja_](https://en.wikipedia.org/wiki/Puja_(Hinduism)) and rituals are performed. It is followed by eating of _Chole Puri_, which to be frank becomes a bit under-whelming experience. As the day goes by and various, normal food is eaten, a gloom starts coming that why even eat now. It doesn't matter. Grand lists made on seventh day just don't feel so good anymore. The cravings are all gone by this time. Food feels just like a necessity to mind, leading to completion of Navratri.

Still, I feel Navratris are good time. It gives the opportunity to strengthen mental strength over physical needs. Also, it's fun seeing the reaction of folks when they get to know that someone is keeping all the fasts.  
