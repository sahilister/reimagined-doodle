---
title: "OpenStreetMap and Snapchat"
date: 2022-08-26T06:03:19+05:30
tags: ["osm", "openstreetmap", "snapchat"]
---

A casual talk regarding OpenStreetMap (OSM) with my roommate lead to the discovery that Snapchat uses OpenStreetMap as basemap for their map functionality. It was felt great learning about this development. 

I was mapping actively since the time I moved to Gurugram with my colleague/roommate. My roommate use to see me doing edits and making roads in OSM. He was curious to know who even uses those maps, and during one of these discussions, I remembered vaguely reading somewhere about Snapchat and OpenStreetMap. Personally, I don't use Snapchat, so asked him to open the map feature to confirm. Clicking on the small info icon on the bottom, confirmed that Snapchat uses OpenStreetMap data through Mapbox (seems to be using a secondary source for POI data). That got his interest piqued as well. Snap Map (as they're called) can be viewed online here [map.snapchat.com](https://map.snapchat.com) as well.

After Snap Maps, I also discovered that the big live bookings screen at my office (an online travel aggregator), also uses OpenStreetMap data. Been  passing this screen everyday and just noticed the small OpenStreetMap credits at the bottom. Next, discovered OpenStreetMap being used in my live bus tracking link through MapTiler (though the data seemed to be old and didn't contain my POI and roads edits in my nearby areas). 
Zomato also seems to used OpenStreetMap for restaurant location screenshots on their website.

These usages of OSM data in wild gives me the motivation to add/edit even more to improve maps for everyone, including Snapchatters and Pokémon Go players everywhere. OpenStreetMap mapping has helped me know places better. The history/road types/classifications and figuring out the appropriate tags is always an experience. Seeing roads and POIs fill up empty stretches of maps, always look good. 
OpenStreetMap data has given rise to so wonderful uses which wouldn't have been possible with non-free licensed geographical data. 


PS - If you're new to OpenStreetMap and want to contribute, start with StreetComplete app. It gamifies the whole data addition part by asking questions on missing nodes in your local area. 

PSS  - My inbox is always open for chats regarding mapping and OSM.
