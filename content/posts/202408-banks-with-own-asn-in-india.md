---
title: "Banks With Own ASN in India"
date: 2024-08-07T07:48:17+05:30
tags: ["bank", "asn", "network", "internet","list"]
---

Most banks are behind CDNs and DDoS mitigation providers nowadays, though they still hold their own IP space. Was interested in this, so compiled a list from [BGP.Tools](https://bgp.tools/) and [Hurricane Electric BGP Toolkit](https://bgp.he.net/).

- [AS17436](https://bgp.tools/as/17436) ICICIBANK Ltd, Banking, Mumbai
- [AS24055](https://bgp.tools/as/24055) Deutsche Bank AG-India Internet AS
- [AS38029](https://bgp.tools/as/38029) Citibank N.A. - ISP Peering, Chennai, India
- [AS38468](https://bgp.tools/as/38468) ASN for Yes Bank
- [AS45644](https://bgp.tools/as/45644) SBI-EMS-NET-IN
- [AS59194](https://bgp.tools/as/59194) Central Bank of India
- [AS131283](https://bgp.tools/as/131283) HDFC Bank House
- [AS132440](https://bgp.tools/as/132440) Unity Small Finance Bank Pvt. Ltd.
- [AS132946](https://bgp.tools/as/132946) Ujjivan Small Finance Bank Ltd
- [AS132989](https://bgp.tools/as/132989) Sangli Urban Co-operative Bank Ltd
- [AS133640](https://bgp.tools/as/133640) Indian Overseas Bank
- [AS133657](https://bgp.tools/as/133657) IndusInd Bank Ltd
- [AS134909](https://bgp.tools/as/134909) The South Indian Bank Ltd
- [AS135086](https://bgp.tools/as/135086) Axis Bank Limited
- [AS135745](https://bgp.tools/as/135745) UCO Bank
- [AS135819](https://bgp.tools/as/135819) Chaitanya Godavari Grameena Bank
- [AS136252](https://bgp.tools/as/136252) The Bank of Baroda Limited
- [AS136324](https://bgp.tools/as/136324) IDBI Bank Ltd
- [AS136622](https://bgp.tools/as/136622) Equitas Small Finance Bank Ltd
- [AS136680](https://bgp.tools/as/136680) Bank of Maharashtra
- [AS136707](https://bgp.tools/as/136707) The Kalupur Commercial Co-operative Bank Limited
- [AS137104](https://bgp.tools/as/137104) India Post Payments Bank Limited
- [AS137108](https://bgp.tools/as/137108) Bank of India
- [AS137130](https://bgp.tools/as/137130) Punjab National Bank
- [AS137662](https://bgp.tools/as/137662) IDFC Bank Ltd
- [AS137670](https://bgp.tools/as/137670) Canara Bank
- [AS138318](https://bgp.tools/as/138318) The Visakhapatnam Cooperative Bank Ltd
- [AS140156](https://bgp.tools/as/140156) Karnataka Gramin Bank
- [AS141222](https://bgp.tools/as/141222) Telangana State Coorperative Apex Bank Ltd
- [AS141561](https://bgp.tools/as/141561) Punjab and Sind Bank
- [AS146870](https://bgp.tools/as/146870) TJSB Sahakari Bank Ltd
- [AS149202](https://bgp.tools/as/149202) The Jammu And Kashmir Bank Pvt Ltd
- [AS149528](https://bgp.tools/as/149528) Indian Bank
- [AS149603](https://bgp.tools/as/149603) Suryoday Small Finance Bank Limited
- [AS150029](https://bgp.tools/as/150029) The Karnataka Bank Ltd
- [AS151172](https://bgp.tools/as/151172) CSB Bank Ltd
- [AS151692](https://bgp.tools/as/151692) Small Industries Development Bank of India


Other noteable mentions:

- [AS141857](https://bgp.tools/as/141857) National Bank for Agriculture and Rural Development
- [AS151773](https://bgp.tools/as/151773) Reserve Bank Information Technology Pvt Ltd


Let me know if I'm missing someone. Many thanks to [Saswata Sarkar](https://c.im/@saswatasarkar13) for helping with the list.

